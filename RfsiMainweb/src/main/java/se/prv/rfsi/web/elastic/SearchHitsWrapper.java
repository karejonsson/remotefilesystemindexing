package se.prv.rfsi.web.elastic;

import java.util.List;

public class SearchHitsWrapper {
	
	private double score;
	private String text;
	private String key;
	private List<String> hits;
	private long id;

	public SearchHitsWrapper(double score, String text, String key, List<String> hits, String id) {
		this.score = score;
		this.text = text; 
		this.key = key;
		this.hits = hits;
		this.id = Long.parseLong(id);
	}

	public List<String> getHits() {
		return hits;
	}

	public long getId() {
		return id;
	}
	
	public double getScore() {
		return score;
	}

}
