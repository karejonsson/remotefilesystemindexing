package se.prv.rfsi.web.elastic;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;

import se.prv.rfsi.idx.reuse.ElasticSearchOperations;

public class SearchResponseWrapper {
	
	private SearchResponse sr = null;
	
	public SearchResponseWrapper(SearchResponse sr) {
		this.sr = sr; 
	}
	
	public List<SearchHitsWrapper> getHits(String fieldname) {
		List<SearchHitsWrapper> out = new ArrayList<SearchHitsWrapper>();
		SearchHits hits = sr.getHits();
		for (SearchHit hit : hits) {
			Map<String, Object> m = hit.getSourceAsMap();
			double score = hit.getScore();
			String text = m.get(fieldname).toString();
			String id = m.get(ElasticSearchOperations.idFieldname).toString();
			Map<String, HighlightField> hfs = hit.getHighlightFields();
			for(String key : hfs.keySet()) {
				HighlightField hf = hfs.get(key);
				Text[] frags = hf.fragments();
				out.add(new SearchHitsWrapper(score, text, key, getBareTextarray(frags), id));
			}
		}
		return out;
	}
	
	private static List<String> getBareTextarray(Text[] in) {
		List<String> out = new ArrayList<String>();
		for(Text t : in) {
			out.add(t.toString());
		}
		return out;
	}

}
