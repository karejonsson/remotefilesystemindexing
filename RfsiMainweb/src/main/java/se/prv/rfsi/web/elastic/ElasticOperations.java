package se.prv.rfsi.web.elastic;

import java.io.IOException;
import java.net.InetAddress;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.Query;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.index.query.MatchPhraseQueryBuilder;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.elasticsearch.index.query.SimpleQueryStringBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import se.prv.rfsi.idx.reuse.ElasticSearchOperations;
import se.prv.rfsi.idx.reuse.PropertyNames;
import se.prv.rfsi.internal.InstallationProperties;
import se.prv.rfsi.web.deploy.SetupSSHTunnels;

public class ElasticOperations {

    public final static String clusternameProperty = "cluster.name";
	
	public static String indexName = null; 
	static {
		try {
			indexName = InstallationProperties.getString(PropertyNames.es_indexName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
    private static Client getClient() throws Exception {
    	String clustername = InstallationProperties.getString(PropertyNames.es_clustername);
    	Settings settings = Settings.builder()
        .put(clusternameProperty, clustername)
        .build();
    	
    	String publishHost = InstallationProperties.getString(PropertyNames.es_publishHost);
    	int networkPort = InstallationProperties.getInt(PropertyNames.es_networkPort, 9200);

    	@SuppressWarnings("resource")
		TransportClient client = new PreBuiltTransportClient(settings)
    	        .addTransportAddress(new TransportAddress(InetAddress.getByName(publishHost), networkPort));

    	return client;
    }
    
    private static String[] fields = new String[] { ElasticSearchOperations.bodytextFieldname, ElasticSearchOperations.bodytextSynonymFieldname };
    
	private static SearchResponse search(Client client, String freetext, QueryBuilder query, String documentTypeSpecified, String fieldname) throws IOException {
		HighlightBuilder highlightBuilder = new HighlightBuilder()
		        .preTags("<b>")
		        .postTags("</b>")
		        .field(fieldname);
		//AggregationBuilder ab = AggregationBuilders.terms("aggregation").field(ESFunctions.urlFieldname);
		SearchSourceBuilder source = new SearchSourceBuilder().query(query).highlighter(highlightBuilder);
		System.out.println("ElasticOperations: documentTypeSpecified "+documentTypeSpecified);
		System.out.println("ElasticOperations: Slutgiltig källa "+source);
		SearchRequestBuilder srb = client.prepareSearch(indexName).setTypes(documentTypeSpecified).setSource(source)/*.addAggregation(ab)*/.setExplain(true);
		System.out.println("ElasticOperations: Slutgiltig förfrågan "+srb);		
		return srb.execute().actionGet();
	}
	
	private static SearchResponse searchRawQuery(Client client, String freetext, Operator operator, String documentType, String fieldname, Integer slop) throws IOException {
		QueryBuilder query = null;
		if(slop != null) {
			MatchPhraseQueryBuilder mpqb = new MatchPhraseQueryBuilder(fieldname, freetext);
			query = mpqb.slop(slop);
		}
		else {
			MultiMatchQueryBuilder mmqb = new MultiMatchQueryBuilder(freetext, fieldname);//fields);
			query = mmqb.operator(operator); 
		}
		/*
		MultiMatchQueryBuilder query = QueryBuilders.multiMatchQuery(freetext, fields);
		MatchPhraseQueryBuilder mpqr = new MatchPhraseQueryBuilder(fieldname, freetext);
		if(slop != null) {
			query = query.slop(slop);
		}
		*/
		//query.operator(operator);
		return search(client, freetext, query, documentType, fieldname);
	}
	
	private static SearchResponse searchSimpleQuery(Client client, String freetext, Operator operator, String documentType, String fieldname) throws IOException {
		SimpleQueryStringBuilder query = new SimpleQueryStringBuilder(freetext);
		query.defaultOperator(operator);
		return search(client, freetext, query, documentType, fieldname);
	}
	
	private static SearchResponse searchAdvancedQuery(Client client, String freetext, Operator operator, String documentType, String fieldname, Integer slop) throws IOException, ParseException {
		QueryParser qp = new QueryParser(fieldname, new StandardAnalyzer());
		Query q = qp.parse(freetext);
		QueryStringQueryBuilder query = QueryBuilders.queryStringQuery(q.toString());
		if(slop != null) {
			query = query.phraseSlop(slop);
		}
		query.defaultOperator(operator);
		return search(client, freetext, query, documentType, fieldname);
	}
	
	private static Client client = null;
	
	public static void startElasticClientIfNecessary() {
		if(client == null) {
			SetupSSHTunnels.startElasticClientTunnelsIfNecessary();
			try {
				client = getClient();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void closeElasticClientIfPossible() {
		if(client == null) {
			try {
				client.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			SetupSSHTunnels.closeElasticClientTunnelsIfPossible();
		}
	}
	
	public static SearchResponseWrapper searchRawQuery(String freetext, Operator operator, String documentType, String fieldname, Integer slop) {
		startElasticClientIfNecessary();
		SearchResponse out = null;
		try {
			out = searchRawQuery(client, freetext, operator, documentType, fieldname, slop);
		} 
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		return new SearchResponseWrapper(out);
	}

	public static SearchResponseWrapper searchSimpleQuery(String freetext, Operator operator, String documentType, String fieldname) {
		startElasticClientIfNecessary();
		SearchResponse out = null;
		try {
			out = searchSimpleQuery(client, freetext, operator, documentType, fieldname);
		} 
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		return new SearchResponseWrapper(out);
	}

	public static SearchResponseWrapper searchAdvancedQuery(String freetext, Operator operator, String documentType, String fieldname, Integer slop) throws IOException, ParseException {
		startElasticClientIfNecessary();
		SearchResponse out = searchAdvancedQuery(client, freetext, operator, documentType, fieldname, slop);
		return new SearchResponseWrapper(out);
	}

}
