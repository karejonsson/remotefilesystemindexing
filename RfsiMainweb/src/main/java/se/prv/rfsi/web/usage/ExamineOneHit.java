package se.prv.rfsi.web.usage;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;

import com.vaadin.server.DownloadStream;
import com.vaadin.server.StreamResource;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.VerticalLayout;

import se.prv.rfsi.dbc.DocumentDB;
import se.prv.rfsi.domain.Document;
import se.prv.rfsi.domain.Indexable;
import se.prv.rfsi.domain.Indexrow;
import se.prv.rfsi.internal.InstallationProperties;
import se.prv.rfsi.web.deploy.PropertyNames;

public class ExamineOneHit extends Panel {
	
	private Runnable onClose = null;
	private long documentId;
	private String textOfHit;
	private String baseUrl = null;
	private VerticalLayout vl = new VerticalLayout();
	private Document doc = null;
	private List<Indexable> indexables = null;
	private Grid<Indexable> grid = null;
	
	public ExamineOneHit(long documentId, String textOfHit, Runnable onClose) {
		this.documentId = documentId;
		this.textOfHit = textOfHit;
		this.onClose = onClose;
		try {
			baseUrl = InstallationProperties.getString(PropertyNames.retrieveBaseOfUrl);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//vl.addComponent(new Label("Hej "+documentId));
		Button back = new Button("Åter");
		back.addListener(event -> { close(); });
		vl.addComponent(back);
		doc = DocumentDB.getDocumentFromId(documentId);
		indexables = doc.getReferringIndexables();
		init();
		setContent(vl);
	}
	
	private void init() {
		//TextArea hit = new TextArea("Träffande text");
		//hit.setValue(textOfHit);
		//hit.setWidth("100%");
		Label hit = new Label(textOfHit, ContentMode.HTML);
		hit.setWidth("100%");
		vl.addComponent(hit);
		
		vl.addComponent(new Label("Filens förekomster på servern"));
		grid = new Grid<>();
		
		grid.addColumn(Indexable::getName).setCaption("Filnamn");
		grid.addColumn(Indexable::getSize).setCaption("Storlek");
        grid.addComponentColumn(row -> {
        	return new Label(row.getIndexrow().getSkeletondate().toString()); 
        }).setCaption("Publicerad");
		grid.addColumn(Indexable::getActualized).setCaption("Inläst");
        grid.addComponentColumn(row -> {
        	return new Label(row.getIndexrow().getImportsize().toString()); 
        }).setCaption("Storlek källfil");
        grid.addComponentColumn(row -> {
        	return getLink(row.getIndexrow()); 
        }).setCaption("Källfil");
        
		grid.setItems(indexables);
		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.setSizeFull();

		vl.addComponent(grid);
		TextArea indexedText = new TextArea("Indexerad brödtext");
		indexedText.setValue(new String(doc.getData(), StandardCharsets.UTF_8));
		indexedText.setWidth("100%");
		vl.addComponent(indexedText);
	}

	private void close() {
		if(onClose != null) {
			onClose.run();
		}
	}
	
	public Label getLink(Indexrow ixr) {
		return new Label(
				"<a href=\""+baseUrl+ixr.getHref()+"\" target=\"_blank\">"+ixr.getName()+"</a> "+
				"<a href=\""+getFolderLink(baseUrl+ixr.getHref())+"\" target=\"_blank\">katalog</a>"
						,ContentMode.HTML);
	}
	
	public static String getFolderLink(String url) {
		int idx = url.lastIndexOf("/");
		return url.substring(0, idx);
	}
	
	public class LinkToSourceFile extends Link {
		
		private Indexrow ixr = null;
		
		public LinkToSourceFile(Indexrow ixr) {
			this.ixr = ixr;
			setCaption(ixr.getHref());;
			setDescription("Hämta källfilen");
			setTargetName("_blank");
		}
		@Override
		public void attach() {
			super.attach(); // Must call.

			StreamResource.StreamSource source = new StreamResource.StreamSource() {
				public InputStream getStream() {
					URL url = null;
					String urlS = baseUrl+ixr.getHref();
					try {
						url = new URL(urlS);
					} catch (MalformedURLException e) {
						Notification.show("URLen "+urlS+" har internt fel.", Type.ERROR_MESSAGE);
						e.printStackTrace();
						return null;
					}
					try {
						return url.openStream();
					} catch (IOException e) {
						Notification.show("URLen "+urlS+" kunde inte öppnas för nedladdning.", Type.ERROR_MESSAGE);
						e.printStackTrace();
						return null;
					}
				}
			};
			StreamResource resource = new StreamResource (source, ixr.getName()) {
				public DownloadStream getStream() {
					DownloadStream out = super.getStream();
					InputStream is = out.getStream();
					return out;
				}
			};
			resource.setCacheTime(0);
			setResource(resource);
		}
		
	}
}
