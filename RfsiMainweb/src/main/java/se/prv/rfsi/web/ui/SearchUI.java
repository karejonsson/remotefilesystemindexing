package se.prv.rfsi.web.ui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;

import general.reuse.vaadinreusable.ui.SimplifiedContentUI;
import se.prv.rfsi.dbcreuse.Commons;
import se.prv.rfsi.web.deploy.SetupSSHTunnels;
import se.prv.rfsi.web.usage.MainSearchPanel;

@SpringBootApplication
@SpringUI(path="/")
public class SearchUI extends SimplifiedContentUI {
	
	@Override
	public void setupGeneralMenubar() {
	}

	@Override
	public void setupParticularMenubar() {
	}

	@Override
	protected void init(VaadinRequest request) {
	}

    public static void main(String[] args) {
    	SetupSSHTunnels.startJDBCTunnelIfNecessary();
        SpringApplication.run(SearchUI.class, args);
    }
    
    public SearchUI() {
    	setup(new MainSearchPanel());
    }
    
}
