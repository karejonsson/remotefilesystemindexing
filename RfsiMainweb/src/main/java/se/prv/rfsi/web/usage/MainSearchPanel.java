package se.prv.rfsi.web.usage;

import java.util.List;

import org.apache.lucene.queryparser.classic.ParseException;
import org.elasticsearch.index.query.Operator;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import com.vaadin.data.HasValue.ValueChangeEvent;
import com.vaadin.data.HasValue.ValueChangeListener;
import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import se.prv.rfsi.idx.reuse.ElasticSearchOperations;
import se.prv.rfsi.idx.reuse.PropertyNames;
import se.prv.rfsi.internal.InstallationProperties;
import se.prv.rfsi.web.elastic.ElasticOperations;
import se.prv.rfsi.web.elastic.SearchHitsWrapper;
import se.prv.rfsi.web.elastic.SearchResponseWrapper;
import se.prv.rfsi.web.ui.SearchUI;

public class MainSearchPanel extends Panel {
	
	private VerticalLayout layout = new VerticalLayout();
	private TextField searchField = null;
	private ComboBox<String> operator = null;//new CheckBox("Avancerat sök");
	private ComboBox<String> synonymChoices = null;//new CheckBox("Avancerat sök");
	private ComboBox<String> slopChoices = null;//new CheckBox("Avancerat sök");
	private ComboBox<String> language = null;//new CheckBox("Avancerat sök");
	private Panel results = new Panel();
	private VerticalLayout resultsLayout = new VerticalLayout();
	
	private boolean sizeFixed = false;
	private static final String[] langItems = new String[] {"Rått", "Enkelt", "Avancerat"};
	private static final String[] operItems = new String[] {"AND", "OR"};
	private static final String[] synonymChoicesItems = new String[] {"NEJ", "JA"};
	private static final String[] slopChoicesItems = new String[] {"Inget", "4 ord", "20 ord", "100 ord"};

	public MainSearchPanel() {
		fixSize();
		operator = new ComboBox<String>("Operator");
		operator.setItems(operItems);
		operator.setValue(operItems[0]);
		operator.setTextInputAllowed(false);
		operator.setEmptySelectionAllowed(false);
		operator.setWidth("15%");
		
		synonymChoices = new ComboBox<String>("Synonymer");
		synonymChoices.setItems(synonymChoicesItems);
		synonymChoices.setValue(synonymChoicesItems[0]);
		synonymChoices.setTextInputAllowed(false);
		synonymChoices.setEmptySelectionAllowed(false);
		synonymChoices.setWidth("15%");
		
		slopChoices = new ComboBox<String>("Gynna närhet");
		slopChoices.setItems(slopChoicesItems);
		slopChoices.setValue(slopChoicesItems[0]);
		slopChoices.setTextInputAllowed(false);
		slopChoices.setEmptySelectionAllowed(false);
		slopChoices.setWidth("15%");
		
		language = new ComboBox<String>("Frågespråk");
		
		language.setItems(langItems);
		language.setTextInputAllowed(false);
		language.setValue(langItems[0]);
		language.setEmptySelectionAllowed(false);
		language.setWidth("15%");
		language.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				operator.setEnabled(language.getValue().equals(langItems[0]));
				slopChoices.setEnabled(language.getValue().equals(langItems[0]) || language.getValue().equals(langItems[2]));
 			}
		});
		
		Label hidden = new Label("X");
		hidden.setWidth("15%");
		
		searchField = new TextField();
		searchField.setWidth("70%");
		searchField.addShortcutListener(new ShortcutListener("Return shortcut", ShortcutAction.KeyCode.ENTER, null) {
		    @Override
		    public void handleAction(Object sender, Object target) {
		    	searchHappened();
		    }
		});
		layout.addComponent(searchField);
		layout.setComponentAlignment(searchField, Alignment.TOP_CENTER);
		HorizontalLayout hl = new HorizontalLayout();
		hl.addComponent(language);
		hl.addComponent(operator);
		hl.addComponent(synonymChoices);
		hl.addComponent(slopChoices);
		layout.addComponent(hl);
		layout.setComponentAlignment(hl, Alignment.TOP_CENTER);
		results.setContent(resultsLayout);
		layout.addComponent(results);
		layout.setComponentAlignment(results, Alignment.MIDDLE_CENTER);
		layout.setHeight("100%");

		setContent(layout);
	}
	
	private void fixSize() {
		if(sizeFixed) {
			return;
		}
		try {
			//setWidth(""+((int) UI.getCurrent().getPage().getBrowserWindowWidth()*0.98)+"px");
			results.setHeight(""+((int) UI.getCurrent().getPage().getBrowserWindowHeight()*0.8-40)+"px");
			//System.out.println("Gick bra!");
			sizeFixed = true;
		}
		catch(Exception e) { 
			results.setHeight("500px");
		}
	}
	
	private void searchHappened() {
		fixSize();
		//String entry = searchField.getValue();//Jsoup.clean(searchField.getValue(), Whitelist.basic());
		//System.out.println("MainSearchPanel: Säkrad version <"+entrysafe+">");
		SearchResponseWrapper resp = null;		
		
		String operatorS = operator.getValue();
		Operator oper = Operator.AND;
		if(operatorS.equals("OR")) {
			oper = Operator.OR;
		}
		
		String documentTypeChoiceYesNo = synonymChoices.getValue();
		String documentType = InstallationProperties.getString(PropertyNames.es_documentType, null);

		String fieldname = null;
		if(documentTypeChoiceYesNo.equals(synonymChoicesItems[0])) {
			fieldname = ElasticSearchOperations.bodytextFieldname;
		} 
		if(documentTypeChoiceYesNo.equals(synonymChoicesItems[1])) {
			fieldname = ElasticSearchOperations.bodytextSynonymFieldname;
		}

		String slopChoice = slopChoices.getValue();
		Integer slop = null;
		if(slopChoice.equals(slopChoicesItems[0])) {
			slop = null;
		} 
		if(slopChoice.equals(slopChoicesItems[1])) {
			slop = 4;
		}
		if(slopChoice.equals(slopChoicesItems[2])) {
			slop = 20;
		}
		if(slopChoice.equals(slopChoicesItems[3])) {
			slop = 100;
		}

		String languageS = language.getValue();
		if(languageS.equals(langItems[0])) {
			String entrysafe = Jsoup.clean(searchField.getValue(), Whitelist.basic());
			resp = ElasticOperations.searchRawQuery(entrysafe, oper, documentType, fieldname, slop);
		}
		else if(languageS.equals(langItems[1])) {
			String entry = searchField.getValue();
			resp = ElasticOperations.searchSimpleQuery(entry, oper, documentType, fieldname);
		}
		else {
				String entry = searchField.getValue();
				try {
					resp = ElasticOperations.searchAdvancedQuery(entry, oper, documentType, fieldname, slop);
				} catch (ParseException e) {
					Notification.show("Sökbegreppet ej analyserbart. Feltext:\n"+e.getMessage(), Type.ERROR_MESSAGE);
					e.printStackTrace();
					return;
				} catch (Exception e) {
					Notification.show("Inga träffar", Type.WARNING_MESSAGE);
					e.printStackTrace();
					return;
				}
		}

		if(resp == null) {
			Notification.show("Inga träffar", Type.WARNING_MESSAGE);
			return;
		}
		
		resultsLayout.removeAllComponents();
		
		for(SearchHitsWrapper hit : resp.getHits(ElasticSearchOperations.bodytextFieldname)) {
			resultsLayout.addComponent(getHit(hit.getHits(), hit.getId(), hit.getScore()));
		}
	}
	
	private Component getHit(List<String> hits, long id, double score) {
		StringBuffer sb = new StringBuffer();
		for(String hit : hits) {
			sb.append(hit+"<br>\n");
		}
		//String link = "<a href=\""+id+"\" target=\"_blank\">"+sb.toString()+"</a>";
		HorizontalLayout hl = new HorizontalLayout();
		Button chose = new Button(String.format("%1$,.2f", score));
		chose.addListener(event -> { choiceDone(id, sb.toString()); });
		hl.addComponent(chose);
		hl.addComponent(new Label(sb.toString(), ContentMode.HTML));
		return hl;
	}
	
	private void choiceDone(long id, String textOfHit) {
		((SearchUI) UI.getCurrent()).setWorkContent(new ExamineOneHit(id, textOfHit, new Runnable() {
			@Override
			public void run() {
				((SearchUI) UI.getCurrent()).setWorkContent(MainSearchPanel.this);
			}
		}));
	}
	
}
