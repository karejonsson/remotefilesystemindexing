package se.prv.rfsi.web.deploy;

import general.reuse.tunneling.TunnelForwardLEncapsulation;
import general.reuse.tunneling.TunnelForwardLParameters;
import se.prv.rfsi.idx.reuse.PropertyNames;
import se.prv.rfsi.internal.InstallationProperties;

public class SetupSSHTunnels {
	
	private static TunnelForwardLEncapsulation jdbc_tfle = null;
	
	public static void startJDBCTunnelIfNecessary() {
		
		InstallationProperties.print();
		
		TunnelForwardLParameters jdbc_tflp = new TunnelForwardLParameters();
		try {
			jdbc_tflp.setLogin_domain(InstallationProperties.getString(PropertyNames.jdcb_login_domain));
			jdbc_tflp.setLogin_password(InstallationProperties.getString(PropertyNames.jdcb_login_password));
			jdbc_tflp.setLogin_user(InstallationProperties.getString(PropertyNames.jdcb_login_user));
			jdbc_tflp.setTunnel_lport(InstallationProperties.getInt(PropertyNames.jdcb_tunnel_lport));
			jdbc_tflp.setTunnel_rport(InstallationProperties.getInt(PropertyNames.jdcb_tunnel_rport));
			jdbc_tflp.setTunnel_rhost(InstallationProperties.getString(PropertyNames.jdcb_tunnel_rhost));

			jdbc_tflp.setSsh_port(InstallationProperties.getInt(PropertyNames.ssh_port, 22));
		}
		catch(Exception e) {
			System.out.println("Tillhandahållna parametrar räcker ej för tunneln för JDBC.\n"+jdbc_tflp);
			e.printStackTrace();
			return;
		}
		
		if(!jdbc_tflp.hasAll()) {
			System.out.println("Erforderliga parametrar saknas för tunneln för JDBC.\n"+jdbc_tflp);
			return;
		}

		try {
			jdbc_tfle = new TunnelForwardLEncapsulation(jdbc_tflp);
		} catch (Exception e1) {
			System.out.println("Parametrar saknas för att öppna tunneln för JDBC.\n"+jdbc_tflp);
			e1.printStackTrace();
			return;
		}
		
		if(!jdbc_tfle.openTunnel()) {
			System.out.println("Det gick inte att öppna SSH-tunneln för JDBC.\n"+jdbc_tflp);
			return;
		}

		System.out.println("SSH-tunneln för JDBC öppnad.");

	}
	
	public static void closeJDBCTunnelIfPossible() {
		if(jdbc_tfle != null) {
			jdbc_tfle.closeTunnel();
		}
	}

	private static TunnelForwardLEncapsulation es1_tfle = null;
	private static TunnelForwardLEncapsulation es2_tfle = null;
	
	public static void startElasticClientTunnelsIfNecessary() {
		InstallationProperties.print();

		if(InstallationProperties.getBoolean(PropertyNames.es1_open, false)) {
			TunnelForwardLParameters es1_tflp = new TunnelForwardLParameters();
			try {
				es1_tflp.setLogin_domain(InstallationProperties.getString(PropertyNames.es_login_domain));
				es1_tflp.setLogin_password(InstallationProperties.getString(PropertyNames.es_login_password));
				es1_tflp.setLogin_user(InstallationProperties.getString(PropertyNames.es_login_user));
				es1_tflp.setTunnel_lport(InstallationProperties.getInt(PropertyNames.es1_tunnel_lport));
				es1_tflp.setTunnel_rport(InstallationProperties.getInt(PropertyNames.es1_tunnel_rport));
				es1_tflp.setTunnel_rhost(InstallationProperties.getString(PropertyNames.es1_tunnel_rhost));
	
				es1_tflp.setSsh_port(InstallationProperties.getInt(PropertyNames.ssh_port, 22));
			}
			catch(Exception e) {
				System.out.println("Tillhandahållna parametrar räcker ej för tunneln för ES1.\n"+es1_tflp);
				e.printStackTrace();
				return;
			}
			
			if(!es1_tflp.hasAll()) {
				System.out.println("Erforderliga parametrar saknas för tunneln för ES1.\n"+es1_tflp);
				return;
			}
			
			try {
				es1_tfle = new TunnelForwardLEncapsulation(es1_tflp);
			} catch (Exception e1) {
				System.out.println("Parametrar saknas för att öppna tunneln för ES1.\n"+es1_tflp);
				e1.printStackTrace();
				return;
			}
			
			if(!es1_tfle.openTunnel()) {
				System.out.println("Det gick inte att öppna SSH-tunneln för ES1.\n"+es1_tflp);
				return;
			}
	
			System.out.println("SSH-tunneln för ES1 öppnad.");
		}

		if(InstallationProperties.getBoolean(PropertyNames.es2_open, false)) {
			TunnelForwardLParameters es2_tflp = new TunnelForwardLParameters();
			try {
				es2_tflp.setLogin_domain(InstallationProperties.getString(PropertyNames.es_login_domain));
				es2_tflp.setLogin_password(InstallationProperties.getString(PropertyNames.es_login_password));
				es2_tflp.setLogin_user(InstallationProperties.getString(PropertyNames.es_login_user));
				es2_tflp.setTunnel_lport(InstallationProperties.getInt(PropertyNames.es2_tunnel_lport));
				es2_tflp.setTunnel_rport(InstallationProperties.getInt(PropertyNames.es2_tunnel_rport));
				es2_tflp.setTunnel_rhost(InstallationProperties.getString(PropertyNames.es2_tunnel_rhost));
	
				es2_tflp.setSsh_port(InstallationProperties.getInt(PropertyNames.ssh_port, 22));
			}
			catch(Exception e) {
				System.out.println("Tillhandahållna parametrar räcker ej för tunneln för ES2.\n"+es2_tflp);
				e.printStackTrace();
				return;
			}
			
			if(!es2_tflp.hasAll()) {
				System.out.println("Erforderliga parametrar saknas för tunneln för ES2.\n"+es2_tflp);
				return;
			}
			
			try {
				es2_tfle = new TunnelForwardLEncapsulation(es2_tflp);
			} catch (Exception e1) {
				System.out.println("Parametrar saknas för att öppna tunneln för ES2.\n"+es2_tflp);
				e1.printStackTrace();
				return;
			}
			
			if(!es2_tfle.openTunnel()) {
				System.out.println("Det gick inte att öppna SSH-tunneln för ES2.\n"+es2_tflp);
				return;
			}
	
			System.out.println("SSH-tunneln för ES2 öppnad.");
		}

	}
	
	public static void closeElasticClientTunnelsIfPossible() {
		if(es1_tfle != null) {
			es1_tfle.closeTunnel();
			es1_tfle = null;
		}
		if(es2_tfle != null) {
			es2_tfle.closeTunnel();
			es2_tfle = null;
		}
	}
	
}
