Indexera från tom databas

Processen har fyra steg. De utförs i filen remotefilesystemindexing/documentation/refresh.sh

1. Tag in det filsystemsskelett som publiceras på www.3gpp.org/ftp till lokal skiva. Ordet "skelett" avser att endast kataloger och indexfiler är nedladdade. Det görs med linux-programmet wget. Resultatet är att en skelett med kataloger och filer som heter index.html. Däremot är inte själva nyttofilerna nedlästa.
 
2. Skapa rader i tabellerna indexfile och indexrow. Varje katalog i strukturen har en index.html och raderna i indexfile representerar dessa. Varje rad i katalogen representeras av en rad i tabellen indexrow. 

3. Läs hem filerna och extrahera indexerbara texter. Här finns två fall och det är att filen är en zip respektive att det inte är det. Via extrahering erhålls filer som kan indexeras. Dvs antingen filen i sig eller det som kommer ut. För de indexerbara skapas en rad i tabellen indexable. Raden pekar på den basfil den kommer ifrån. Raderna i indexable spårar existerande nedläsningsställen. För filen beräknas en kontrollsiffra och med den sparas information i tabellen document. Där lagras indexerbar text samt källfilernas kontrollsiffra. Däremot inte filerna i sig. Denna mekanism gör att filernas text lagras unikt. Om en fil finns på flera ställen kommer detta återspeglas genom att flera rader i indexable pekar på samma rad i document.

4. Indexering av texter genom att iterera över raderna i document. 

I september 2018 kördes allt från tömd databas. Det tog cirka 3,5 dagar och gav 24357 indexfile, 1133171 indexrow, 2180000 indexable, 1140000 document. 

Om fallet med fylld databas

Processen är gjord för att köras om och att de senare körningarna skall vara smäckra. Genom att identifiera en fil på att den finns på samma plats, har samma storlek som förut samt lagts upp på samma datum antas den vara oförändrad. Det hämtade skelettet från punkt 1 tar i den meningen hem så lite som möjligt. Punkt 2 blir för all del omfattande men den är intern. Punkt 3 gör så lite det går mot servern hos 3gpp. Punkt 4 indexerar bara det nya genom att en kolumn på tabellen document säger om den är indexerad sedan tidigare. Om man vill köra om indexeringen är det en intern process.  