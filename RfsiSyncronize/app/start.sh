#!/bin/sh

THE_CLASSPATH=
for i in `ls ../target/*.jar`
do
  THE_CLASSPATH=${THE_CLASSPATH}:${i}
done
for i in `ls ../target/lib/*.jar`
do
  THE_CLASSPATH=${THE_CLASSPATH}:${i}
done
THE_CLASS=$1
shift
java -Xmx4G -cp ".:${THE_CLASSPATH}" ${THE_CLASS} "$@"
