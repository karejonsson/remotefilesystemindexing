Common
--prv.rfsi.jdbc.driverclass org.postgresql.Driver
--prv.rfsi.jdbc.domain localhost
--prv.rfsi.jdbc.port ${prv.rfsi.ssh.tunnel_lport}
--prv.rfsi.jdbc.sid indexdb
--prv.rfsi.jdbc.url jdbc:postgresql://${prv.rfsi.jdbc.domain}:${prv.rfsi.jdbc.port}/${prv.rfsi.jdbc.sid}
--prv.rfsi.jdbc.username prvuser
--prv.rfsi.jdbc.password prvpwd

--prv.rfsi.ssh.tunnel_lport 5445
--prv.rfsi.ssh.tunnel_rhost localhost
--prv.rfsi.ssh.tunnel_rport 5432
    
--prv.rfsi.ssh.login_user index
--prv.rfsi.ssh.login_domain 172.17.25.14
--prv.rfsi.ssh.ssh_port 22
--prv.rfsi.ssh.login_password pwd123

AbsorbSkeleton
--folder /home/kare/3gpp/Information/skelett/www.3gpp.org/ftp
--delimiter /ftp/

mvn exec:java -Dexec.mainClass=<klass full kvalificerad> -Dexec.args="<argument, dollar bakåtslashad>"

// Ta in skelett. Att det är från skiva är självklart.
./start.sh se.prv.rfsi.sync.skeleton.AbsorbSkeleton --folder /home/kare/3gpp/Information/skelett/www.3gpp.org/ftp --delimiter /ftp/ --prv.rfsi.jdbc.driverclass org.postgresql.Driver --prv.rfsi.jdbc.domain localhost --prv.rfsi.jdbc.port \${prv.rfsi.ssh.tunnel_lport} --prv.rfsi.jdbc.sid indexdb --prv.rfsi.jdbc.url jdbc:postgresql://\${prv.rfsi.jdbc.domain}:\${prv.rfsi.jdbc.port}/\${prv.rfsi.jdbc.sid} --prv.rfsi.jdbc.username prvuser --prv.rfsi.jdbc.password prvpwd --prv.rfsi.ssh.tunnel_lport 5445 --prv.rfsi.ssh.tunnel_rhost localhost --prv.rfsi.ssh.tunnel_rport 5432 --prv.rfsi.ssh.login_user index --prv.rfsi.ssh.login_domain 172.17.25.14 --prv.rfsi.ssh.ssh_port 22 --prv.rfsi.ssh.login_password pwd123

ImportIndexrow
--delimiter /ftp/
--startServer true
--serveFolder /home/kare/3gpp/Information/total/www.3gpp.org/ftp
--servePort 8083
--serverThreads 100
--servedUrl http://localhost:${servePort}${delimiter}
--hashAlgo SHA-256
--maxHandledSize 1000000000
--dateFormatActualized 'yyyy-MM-dd HH:mm:ss'
--dateActualized 

// Ta in från data som ligger på lokal skiva efter att skelettet absorberats
./start.sh se.prv.rfsi.sync.total.ImportEverything --dateFormatActualized 'yyyy-MM-dd HH:mm:ss' --dateActualized '2018-02-12 12:58:00' --maxHandledSize 1000000000 --hashAlgo SHA-256 --delimiter /ftp/ --startServer true --serverThreads 100 --serveFolder /home/kare/3gpp/Information/total/www.3gpp.org --servePort 8083 --servedUrl http://localhost:\${servePort}\${delimiter} --prv.rfsi.jdbc.driverclass org.postgresql.Driver --prv.rfsi.jdbc.domain localhost --prv.rfsi.jdbc.port \${prv.rfsi.ssh.tunnel_lport} --prv.rfsi.jdbc.sid indexdb --prv.rfsi.jdbc.url jdbc:postgresql://\${prv.rfsi.jdbc.domain}:\${prv.rfsi.jdbc.port}/\${prv.rfsi.jdbc.sid} --prv.rfsi.jdbc.username prvuser --prv.rfsi.jdbc.password prvpwd --prv.rfsi.ssh.tunnel_lport 5445 --prv.rfsi.ssh.tunnel_rhost localhost --prv.rfsi.ssh.tunnel_rport 5432 --prv.rfsi.ssh.login_user index --prv.rfsi.ssh.login_domain 172.17.25.14 --prv.rfsi.ssh.ssh_port 22 --prv.rfsi.ssh.login_password pwd123

// Ta in från 3gpp-s server direkt efter att skelettet absorberats
./start.sh se.prv.rfsi.sync.total.ImportEverything --dateFormatActualized 'yyyy-MM-dd HH:mm:ss' --dateActualized '2018-02-12 16:08:00' --maxHandledSize 1000000000 --hashAlgo SHA-256 --delimiter /ftp/ --servedUrl http://www.3gpp.org\${delimiter} --prv.rfsi.jdbc.driverclass org.postgresql.Driver --prv.rfsi.jdbc.domain localhost --prv.rfsi.jdbc.port \${prv.rfsi.ssh.tunnel_lport} --prv.rfsi.jdbc.sid indexdb --prv.rfsi.jdbc.url jdbc:postgresql://\${prv.rfsi.jdbc.domain}:\${prv.rfsi.jdbc.port}/\${prv.rfsi.jdbc.sid} --prv.rfsi.jdbc.username prvuser --prv.rfsi.jdbc.password prvpwd --prv.rfsi.ssh.tunnel_lport 5445 --prv.rfsi.ssh.tunnel_rhost localhost --prv.rfsi.ssh.tunnel_rport 5432 --prv.rfsi.ssh.login_user index --prv.rfsi.ssh.login_domain 172.17.25.14 --prv.rfsi.ssh.ssh_port 22 --prv.rfsi.ssh.login_password pwd123

// Sätt postfix och indexed fälten till null
./start.sh se.prv.rfsi.sync.total.ResetStateOfIndexfiles --prv.rfsi.jdbc.driverclass org.postgresql.Driver --prv.rfsi.jdbc.domain localhost --prv.rfsi.jdbc.port \${prv.rfsi.ssh.tunnel_lport} --prv.rfsi.jdbc.sid indexdb --prv.rfsi.jdbc.url jdbc:postgresql://\${prv.rfsi.jdbc.domain}:\${prv.rfsi.jdbc.port}/\${prv.rfsi.jdbc.sid} --prv.rfsi.jdbc.username prvuser --prv.rfsi.jdbc.password prvpwd --prv.rfsi.ssh.tunnel_lport 5445 --prv.rfsi.ssh.tunnel_rhost localhost --prv.rfsi.ssh.tunnel_rport 5432 --prv.rfsi.ssh.login_user index --prv.rfsi.ssh.login_domain 172.17.25.14 --prv.rfsi.ssh.ssh_port 22 --prv.rfsi.ssh.login_password pwd123

// Räkna filer som text kunnat extraheras från
./start.sh se.prv.rfsi.sync.total.AnalyseLosses --prv.rfsi.jdbc.driverclass org.postgresql.Driver --prv.rfsi.jdbc.domain localhost --prv.rfsi.jdbc.port \${prv.rfsi.ssh.tunnel_lport} --prv.rfsi.jdbc.sid indexdb --prv.rfsi.jdbc.url jdbc:postgresql://\${prv.rfsi.jdbc.domain}:\${prv.rfsi.jdbc.port}/\${prv.rfsi.jdbc.sid} --prv.rfsi.jdbc.username prvuser --prv.rfsi.jdbc.password prvpwd --prv.rfsi.ssh.tunnel_lport 5445 --prv.rfsi.ssh.tunnel_rhost localhost --prv.rfsi.ssh.tunnel_rport 5432 --prv.rfsi.ssh.login_user index --prv.rfsi.ssh.login_domain 172.17.25.14 --prv.rfsi.ssh.ssh_port 22 --prv.rfsi.ssh.login_password pwd123

