package se.prv.rfsi.sync.skeleton;

import general.reuse.properties.CommandLineProperties;
import general.reuse.timeouthandling.task.ResourceManagement;

public class AbsorbtionSkeletonResourceManagement implements ResourceManagement {

	private CommandLineProperties clp;
	private ConnectionTotal ct;

	public AbsorbtionSkeletonResourceManagement(CommandLineProperties clp) {
		this.clp = clp;
	}
	
	@Override
	public boolean isActive() {
		if(ct == null) {
			return false;
		}
		return ct.isOpen();
	}

	@Override
	public void stop() {
		if(ct != null) {
			ct.close();
			ct = null;
		}
	}

	@Override
	public void start() {
		ct = new ConnectionTotal(clp);
		if(!ct.open()) {
			System.out.println("Kunde inte öppna mot DB");
		}
	}

	@Override
	public boolean isProbablyOK() {
		// TODO Auto-generated method stub
		return false;
	}

}
