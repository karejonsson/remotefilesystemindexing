package se.prv.rfsi.sync.docimporter;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.apache.poi.hssf.extractor.OldExcelExtractor;
import org.apache.poi.xssf.extractor.XSSFExcelExtractor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class PoiSpecificImporterLegacyExcel extends TikaBasedSpecificImporterLegacy {

	public static final int importerid = 5;
	
	protected int getImporterId() {
		return importerid;
	}

	public static final String[] postfixes = new String[] {
			"xls", "xlsx", 
			};

	@Override
	public boolean handles(String postfix) {
		String pxl = postfix.toLowerCase();
		for(String px : postfixes) {
			if(pxl.equals(px)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String[] getPostfixes() {
		return postfixes;
	}

	protected String getText(byte[] contents) throws IOException {
		try {
			XSSFWorkbook xwpfdoc = new XSSFWorkbook(new ByteArrayInputStream(contents));
		    XSSFExcelExtractor ex = new XSSFExcelExtractor(xwpfdoc);
			String out = ex.getText();
			ex.close();
		    return out;
		}
		catch(Exception e) {
		}	    
		OldExcelExtractor ex = new OldExcelExtractor(new ByteArrayInputStream(contents));
		String out = ex.getText();
		ex.close();
	    return out;
	}

}
