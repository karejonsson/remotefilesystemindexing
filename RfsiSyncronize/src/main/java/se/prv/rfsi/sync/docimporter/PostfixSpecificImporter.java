package se.prv.rfsi.sync.docimporter;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import org.apache.tika.Tika;

import se.prv.rfsi.domain.Document;

public abstract class PostfixSpecificImporter {
	
	public static final Charset charset = StandardCharsets.UTF_8;
	
	public abstract boolean handles(String postfix);
	public abstract String[] getPostfixes();
	public abstract Document getDocument(byte[] contents, String checksum);

	protected String getMimetype(byte[] content) {
		return new Tika().detect(content);
	}

}
