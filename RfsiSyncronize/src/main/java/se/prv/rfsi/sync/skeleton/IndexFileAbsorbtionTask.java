package se.prv.rfsi.sync.skeleton;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import general.reuse.timeouthandling.task.TimeoutTask;
import se.prv.rfsi.dbc.IndexfileDB;
import se.prv.rfsi.domain.Indexfile;

public class IndexFileAbsorbtionTask implements TimeoutTask {
	
	private static final String indexhtmlname = "index.html";

	private File indexFile = null;
	private String delimiter = null;
	private boolean isDone = false;
	
	public IndexFileAbsorbtionTask(File indexFile, String delimiter) {
		this.indexFile = indexFile;
		this.delimiter = delimiter;
	}

	@Override
	public void runTimeconsuming(int attemptNo) throws InterruptedException {
		try {
			indexhtml(indexFile, delimiter);
			isDone = true;
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onFinishedBeQuick(int attemptNo) {
	}

	@Override
	public void onTimeoutBeQuick(int attemptNo) {
	}

	@Override
	public void onTerminallyFailed() {
	}

	@Override
	public boolean isDone() {
		return isDone;
	}
	
	private static String getRelativeFolderPath(File node, String delimiter) throws IOException {
		String completeFilesystemName = node.getCanonicalPath();
		int delimiter_startPos = completeFilesystemName.indexOf(delimiter);
		String fileSystemNameAfterDelimiter = completeFilesystemName.substring(delimiter_startPos+delimiter.length());
		String relativeFolderPath = fileSystemNameAfterDelimiter.substring(0, fileSystemNameAfterDelimiter.length()-indexhtmlname.length()-1);
		return relativeFolderPath;
	}
	
	private static void indexhtml(File node, String delimiter) throws IOException {
		String relativeFolderPath = getRelativeFolderPath(node, delimiter);
		Indexfile ixf = IndexfileDB.getIndexfileFromFolder(relativeFolderPath);
		System.out.println("IndexFileAbsorbtionTask: Folder "+relativeFolderPath+" -> "+ixf);
		
		// Struntar i att några kodrader är duplicerade här. Väljer att ha två separat
		// skrivna flöden.
		if(ixf == null) {
			ixf = new Indexfile();
			ixf.setFolder(relativeFolderPath);
			ixf.setPostfixes("");
			ixf.setProcess(false);
			ixf.setActualized(new Date());
			ixf.save();
			CreateSubsequentIndexrow.handlePhase1Skeleton(ixf,  node, delimiter);
		} 
		else {
			ixf.setActualized(new Date());
			ixf.save();
			CreateSubsequentIndexrow.handlePhase1Skeleton(ixf,  node, delimiter);
		}
	}

}
