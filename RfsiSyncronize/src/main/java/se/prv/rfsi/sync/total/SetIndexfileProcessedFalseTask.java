package se.prv.rfsi.sync.total;

import general.reuse.timeouthandling.task.TimeoutTask;
import se.prv.rfsi.domain.Indexfile;

public class SetIndexfileProcessedFalseTask implements TimeoutTask {
	
	private Indexfile ixf = null;
	private boolean isDone = false;
	
	public SetIndexfileProcessedFalseTask(Indexfile ixf) { 
		this.ixf = ixf;
	}

	@Override
	public void runTimeconsuming(int attemptNo) throws InterruptedException {		
		ixf.setProcess(false);
		ixf.save();
		isDone = true;
	}

	@Override
	public void onFinishedBeQuick(int attemptNo) {
	}

	@Override
	public void onTimeoutBeQuick(int attemptNo) {
	}

	@Override
	public void onTerminallyFailed() {
	}

	@Override
	public boolean isDone() {
		return isDone;
	}

}
