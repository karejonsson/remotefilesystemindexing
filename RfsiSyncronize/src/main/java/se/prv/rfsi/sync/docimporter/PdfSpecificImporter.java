package se.prv.rfsi.sync.docimporter;

import se.prv.rfsi.dbc.DocumentDB;
import se.prv.rfsi.domain.Document;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

public class PdfSpecificImporter extends PostfixSpecificImporter {
	
	public static final String[] postfixes = new String[] {"pdf"};
	public static final int importerid = 1;

	@Override
	public boolean handles(String postfix) {
		return postfix.toLowerCase().equals(postfix);
	}

	@Override
	public String[] getPostfixes() {
		return postfixes;
	}
	
	@Override
	public Document getDocument(byte[] content, String checksum) {
		Document doc = DocumentDB.getDocumentFromChecksum(checksum);
		if(doc != null) {
			return doc;
		}
		String mimetype = getMimetype(content);
		if(!mimetype.toLowerCase().contains(postfixes[0])) {
			System.out.println("PdfSpecificImporter: Got mimetype "+mimetype+" to importer "+postfixes[0]+". Rejects.");
			return null;
		}

		String text = getFreetext(content, mimetype);

		doc = new Document();
		doc.setChecksum(checksum);
		doc.setMimetype(mimetype);
		doc.setImporter(importerid);
		doc.save();
		if(text != null) {
			doc.setData(text.getBytes(PostfixSpecificImporter.charset));
		}

		return doc;
	}
	
	private static String getFreetext(byte[] content, String mimetype) {
		String out = null;
		try {
			PDDocument document = PDDocument.load(content);

			//Instantiate PDFTextStripper class
			PDFTextStripper pdfStripper = new PDFTextStripper();

			//Retrieving text from PDF document
			out = pdfStripper.getText(document);

			//Closing the document
			document.close();
		}
		catch(Exception e) {
			System.out.println("PdfSpecificImporter: Got mimetype "+mimetype+" to importer "+PdfSpecificImporter.class.getName()+". Failed with apache pdfbox.");
			e.printStackTrace();
			return null;
		}
		return out;
	}

}
