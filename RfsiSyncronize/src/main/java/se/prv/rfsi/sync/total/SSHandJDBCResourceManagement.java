package se.prv.rfsi.sync.total;

import org.restlet.Component;

import general.reuse.properties.CommandLineProperties;
import general.reuse.timeouthandling.task.ResourceManagement;
import se.dom.rinfo.reuse.server.ServeFolder;
import se.prv.rfsi.sync.skeleton.ConnectionTotal;
import se.prv.rfsi.sync.skeleton.PropertyNames;

public class SSHandJDBCResourceManagement implements ResourceManagement {
	
	private CommandLineProperties clp;

	public SSHandJDBCResourceManagement(CommandLineProperties clp) {
		this.clp = clp;
	}
	
	@Override
	public boolean isActive() {
		if(ct == null) {
			return false;
		}
		if(!ct.isOpen()) {
			return false;
		}
		if(clp.getBoolean(PropertyNames.startServer, false)) {
			if(server == null) {
				return false;
			}
			if(!server.isStarted()) {
				return false;
			}
		}
		return true;
	}

	@Override
	public void stop() {
		if(ct != null) {
			ct.close();
			ct = null;
		}
		if(server != null) {
			try {
				server.stop();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			server = null;
		}
	}

	ConnectionTotal ct = null;
	Component server = null;
	
	@Override
	public void start() {
		stop();
		ct = new ConnectionTotal(clp); 
		if(!ct.open()) {
			System.out.println("Kunde inte öppna mot DB");
			stop();
			return;
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			if(clp.getBoolean(PropertyNames.startServer, false)) {
				String folder = clp.getString(PropertyNames.serveFolder);
				int port = clp.getInt(PropertyNames.servePort);
				int threads = clp.getInt(PropertyNames.serverThreads);
				server = ServeFolder.serveFolderOnPort(folder, port, threads, true);
			}
		}
		catch(Exception e) {
			System.out.println("Kunde inte starta servern");
			stop();
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public boolean isProbablyOK() {
		return isActive();
	}

}
