package se.prv.rfsi.sync.total;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import general.reuse.properties.CommandLineProperties;
import general.reuse.properties.PropertiesAssemble;
import general.reuse.webmanipulation.Download;
import se.dom.rinfo.reuse.server.ServeFolder;
import se.prv.rfsi.dbc.IndexrowDB;
import se.prv.rfsi.domain.Indexrow;
import se.prv.rfsi.internal.Services;
import se.prv.rfsi.sync.rowimporter.ImportErrorCodes;
import se.prv.rfsi.sync.rowimporter.RowImporter;
import se.prv.rfsi.sync.rowimporter.RowImporterManagement;
import se.prv.rfsi.sync.skeleton.ConnectionTotal;
import se.prv.rfsi.sync.skeleton.CreateAllIndexhtml;
import se.prv.rfsi.sync.skeleton.PropertyNames;

public class ImportIndexrow {
	
	public static void main(String args[]) {
		CommandLineProperties clp = null;
		try {
			clp = PropertiesAssemble.collectCLP(args);
			clp.printEvaluated();
		}
		catch(Exception e) {
			System.out.println("Fel vid inläsningen av kommandoradsparametrarna.");
			e.printStackTrace();
			System.exit(1);						
		}
		try {
			main(clp);
		}
		catch(Exception e) {
			System.out.println("Felaktig terminering.");
			e.printStackTrace();
			System.exit(1);			
		}
		System.exit(0);			
	}

	public static void main(CommandLineProperties clp) throws Exception {
		ConnectionTotal ct = new ConnectionTotal(clp); 
		if(!ct.open()) {
			System.out.println("Kunde inte öppna mot DB");
			System.exit(-1);
		}
		
		if(clp.getBoolean(PropertyNames.startServer)) {
			String folder = clp.getString(PropertyNames.serveFolder);
			int port = clp.getInt(PropertyNames.servePort);
			ServeFolder.serveFolderOnPort(folder, port);
		}
		
		String url = clp.getString(PropertyNames.servedUrl);
		String delimiter = clp.getString(PropertyNames.delimiter);
		int idxdel = url.indexOf(delimiter);
		if(idxdel == -1) {
			System.out.println("Avgränsande sekvensen finns inte i URLen");
			System.exit(-1);
		}
		String reducedUrl = url.substring(0, idxdel);
		String usedUrl = reducedUrl+delimiter;
		String hashAlgo = clp.getString(PropertyNames.hashAlgo);
		long maxHandledSize = clp.getLong(PropertyNames.maxHandledSize);
		//String dateFormatActualized = clp.getString(PropertyNames.dateFormatActualized);
		//String dateActualizedS = clp.getString(PropertyNames.dateActualized);
		
		//SimpleDateFormat formatter = new SimpleDateFormat(dateFormatActualized);
		//Date dateFromCommandline = formatter.parse(dateActualizedS);
		
		Indexrow ixr = IndexrowDB.getIndexrowFromId(36);
		System.out.println("OUT "+ixr);
		
		ImportIndexrow iir = new ImportIndexrow(usedUrl, hashAlgo, maxHandledSize);//, dateFromCommandline);
		
		iir.handlePhase2Import(ixr);
		
		System.out.println("URL "+url+", objekt "+ixr);
		
		ct.close();
	}
	
	private String url;
	private RowImporter[] importers;
	private String hashAlgo = null;
	private long maxHandledSize;
	//private Date dateFromCommandline;
	
	public ImportIndexrow(String url, String hashAlgo, long maxHandledSize) { //, Date dateFromCommandline) {
		this.url = url;
		this.hashAlgo = hashAlgo;
		this.maxHandledSize = maxHandledSize;
		//this.dateFromCommandline = dateFromCommandline;
		this.importers = RowImporterManagement.getRowImporters(url, hashAlgo, maxHandledSize);//, dateFromCommandline);
	}
		
	public void handlePhase2Import(Indexrow ixr) {
		ixr.setFailcode(null);
		ixr.setFailmsg((String)null);
		for(RowImporter importer : importers) {
			if(importer.handles(ixr)) {
				System.out.println("ImportIndexrow: Hanterar "+ixr+" med "+importer);
				try {
					importer.importrow(ixr);
					break;
				}
				catch(Exception e) {
					if(ixr.getFailcode() == null) {
						ixr.setFailcode(ImportErrorCodes.rowctrlUnexpectedException);
						ixr.setFailmsg(e);
						ixr.save();
					}
					else {
						if(ixr.getFailcode() < ImportErrorCodes.rowctrlLimit) {
							ixr.setFailcode(ixr.getFailcode()+ImportErrorCodes.rowctrlUnexpectedException);
							ixr.save();
						}
					}
					e.printStackTrace();
				}
			}
		}
	}

}
