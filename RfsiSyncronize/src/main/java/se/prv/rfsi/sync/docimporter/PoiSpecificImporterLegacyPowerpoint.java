package se.prv.rfsi.sync.docimporter;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.apache.poi.xslf.extractor.XSLFPowerPointExtractor;
import org.apache.poi.xslf.usermodel.XMLSlideShow;

public class PoiSpecificImporterLegacyPowerpoint extends TikaBasedSpecificImporterLegacy {

	public static final int importerid = 6;
	
	protected int getImporterId() {
		return importerid;
	}

	public static final String[] postfixes = new String[] {
			"ppt", "pptx", 
			};

	@Override
	public boolean handles(String postfix) {
		String pxl = postfix.toLowerCase();
		for(String px : postfixes) {
			if(pxl.equals(px)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String[] getPostfixes() {
		return postfixes;
	}

	protected String getText(byte[] contents) throws IOException {
		XMLSlideShow xwpfdoc = new XMLSlideShow(new ByteArrayInputStream(contents));
		XSLFPowerPointExtractor ex = new XSLFPowerPointExtractor(xwpfdoc);
		String out = ex.getText();
		ex.close();
	    return out;
	}

}
