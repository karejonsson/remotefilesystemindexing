package se.prv.rfsi.sync.rowimporter;

import se.prv.rfsi.domain.Indexrow;

public abstract class RowImporter {
	
	public abstract boolean handles(Indexrow ixr);
	public abstract void importrow(Indexrow ixr);

}
