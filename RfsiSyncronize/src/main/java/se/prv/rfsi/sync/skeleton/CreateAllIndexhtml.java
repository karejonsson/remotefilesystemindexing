package se.prv.rfsi.sync.skeleton;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import general.reuse.timeouthandling.task.TaskMonitorer;
import se.prv.rfsi.dbc.IndexfileDB;
import se.prv.rfsi.domain.Indexfile;

public class CreateAllIndexhtml {

	public static void main (String args[]) throws IOException {
		CreateAllIndexhtml cai = new CreateAllIndexhtml("/ftp/");
		cai.lookupAllIndexFiles_createTasks(new File("/home/kare/3gpp/Information/skelett/www.3gpp.org/ftp"));
	}
	
	public int ctr = 0;
	
	private String delimiter = null;
	private List<TaskMonitorer> tasks = new ArrayList<TaskMonitorer>();
	
	private static final String indexhtmlname = "index.html";
	
	public CreateAllIndexhtml(String delimiter) {
		this.delimiter = delimiter;
	}
	
	public List<TaskMonitorer> getIndexFileTasks() {
		return tasks;
	}
	
	public void lookupAllIndexFiles_createTasks(File node) throws IOException{
		if(node.isDirectory()){
			String[] subNote = node.list();
			for(String filename : subNote){
				lookupAllIndexFiles_createTasks(new File(node, filename));
			}
		}
		else {
			ctr++;
			if(ctr % 1000 == 0) {
				System.out.println("CreateAllIndexhtml: Gjort "+ctr);
			}
			if(node.getName().equals(indexhtmlname)) {
				tasks.add(new TaskMonitorer(new IndexFileAbsorbtionTask(node, delimiter)));
				//System.out.println(node.getName()+" : "+node.getCanonicalPath());
				//indexhtml(node, delimiter);
			}
		} 
	}
	
	public void print() {
		System.out.println("CreateAllIndexhtml: Antal "+indexhtmlname+"-filer "+ctr);
	}

}
