package se.prv.rfsi.sync.total;

import java.util.List;

import general.reuse.properties.CommandLineProperties;
import general.reuse.properties.PropertiesAssemble;
import se.prv.rfsi.dbc.DocumentDB;
import se.prv.rfsi.dbc.IndexfileDB;
import se.prv.rfsi.domain.Document;
import se.prv.rfsi.domain.Indexfile;
import se.prv.rfsi.sync.skeleton.ConnectionTotal;

public class SetStateOfDocumentIndexationFalse {

	public static void main(String args[]) {
		CommandLineProperties clp = null;
		try {
			clp = PropertiesAssemble.collectCLP(args);
			clp.printEvaluated();
		}
		catch(Exception e) {
			System.out.println("Fel vid inläsningen av kommandoradsparametrarna.");
			e.printStackTrace();
			System.exit(1);						
		}
		try {
			main(clp);
		}
		catch(Exception e) {
			System.out.println("Felaktig terminering.");
			e.printStackTrace();
			System.exit(1);			
		}
		System.exit(0);			
	}

	public static void main(CommandLineProperties clp) throws Exception {
		ConnectionTotal ct = new ConnectionTotal(clp); 
		if(!ct.open()) {
			System.out.println("Kunde inte öppna mot DB");
			System.exit(-1);
		}
		
		List<Document> docs = DocumentDB.getAllDocuments();
		System.out.println("Antal dokument "+docs.size());
		for(Document doc : docs) {
			doc.setIndexed(false);
			doc.save();
		}
		
		ct.close();
	}

}
