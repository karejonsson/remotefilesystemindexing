package se.prv.rfsi.sync.skeleton;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import general.reuse.webmanipulation.HtmlLinkOperations;
import se.prv.rfsi.domain.Indexfile;
import se.prv.rfsi.domain.Indexrow;

public class CreateSubsequentIndexrow {
	
	public static void handlePhase1Skeleton(Indexfile ixf, File node, String delimiter) {
		String content = readAllBytesJava7(node);
		Vector<String> v = getALinks(content);
		Map<String, Indexrow> m = ixf.getIndexrowsMapped();

		for(String s : v) {
			Indexrow ixr = null;
			String postfix = getPostfix_withDot(s);
			if(postfix == null) {
				// This row has no dot delimited postfix so it is assumed to be a folder. 
				// Only real files, identified by postfix, in this phase
				continue;
			}
			String href = HtmlLinkOperations.getHrefFromLink(s);
			int delpos = href.indexOf(delimiter);
			if(delpos != -1) {
				delpos += delimiter.length()-1;
			}
			String hrefAfterDelimiter = href.substring(delpos+1);
			Long skeletonsize = null;
			try {
				skeletonsize = getLength(node.getCanonicalPath(), content, s);
			} 
			catch (IOException e1) {
				e1.printStackTrace();
				continue;
			}
			Date skeletondate = null;
			try {
				skeletondate = getFtpFilesystemDate(node.getCanonicalPath(), content, s);
			} 
			catch (IOException e1) {
				e1.printStackTrace();
				continue;
			}
			String name = getVisibleName(s);
			ixr = m.get(name);
			if(ixr == null) {
				ixr = new Indexrow();
				ixr.setIndexfile(ixf);
				ixr.setSkeletonsize(skeletonsize);
				ixr.setSkeletondate(skeletondate);
				ixr.setHref(hrefAfterDelimiter);
				ixr.setName(name);
			}
			else {
				if(skeletonsize.equals(ixr.getImportsize()) 
						&& skeletondate.equals(ixr.getSkeletondate()) 
						&& hrefAfterDelimiter.equals(ixr.getHref())) {
					// Assumes it is the same size, date and hyperreference
				}
				else {
					// Different size or date or hyperreference
					ixr.setSkeletonsize(skeletonsize);
					ixr.setSkeletondate(skeletondate);
					ixr.setHref(hrefAfterDelimiter);
				}
			}
			ixr.setActualized(new Date());
			ixr.save();
		}
	}
	
	private static String getVisibleName(String htmllink) {
		String out = HtmlLinkOperations.getVisibleTextFromLink(htmllink);
		if(out.length() > 200) {
			return out.substring(0,  200);
		}
		return out;
	}

	private static Long getLength(String path, String file, String link) {
		int idx = file.indexOf(link);
		if(idx == -1) {
			return null;
		}
		String until = file.substring(Math.max(0, idx-12), idx).trim();
		while((idx = until.indexOf(" ")) != -1) {
			until = until.substring(idx);
		}
		try {
			return Long.parseLong(until);
		}
		catch(Exception e) {
			System.out.println("Path "+path+", link "+link);
		}
		return 0l;
	}


	private static Date getFtpFilesystemDate(String path, String file, String link) {
		int idx = file.indexOf(link);
		if(idx == -1) {
			return null;
		}
		String until = file.substring(0, Math.max(0, idx-12)).trim();
		idx = until.lastIndexOf(">");
		if(idx == -1) {
			return null;
		}
		String datesequence = until.substring(idx+1).trim();
		//System.out.println("getFtpFilesystemDate <"+datesequence+">");
		return getDate(datesequence);
	}
	
	private static Date getDate(String date) {
		for(String format : formats) {
			Date out = getDate(date, format);
			if(out != null) {
				return out;
			}
		}
		return null;
	}
	
	
	private static Date getDate(String date, String format) {
		//System.out.println("getDate date= <"+date+">, format "+format);
		try {
	        SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.ENGLISH);
	        Date dateStr = formatter.parse(date);
			return dateStr;
		}
		catch(Exception e) {
			//e.printStackTrace();
		}
		return null;
	}


	private static String getPostfix_withDot(String completelink) {
		String href = HtmlLinkOperations.getHrefFromLink(completelink);
		/*
		int angle = href.lastIndexOf("<");
		if(angle == -1) {
			return null;
		}
		String withoutAngle = href.substring(0, angle);
		String ending = withoutAngle.substring(Math.max(0,  withoutAngle.length()-7));
		*/
		String ending = href.substring(Math.max(0, href.length()-7));
		if(ending.endsWith("/")) {
			return null;
		}
		int idx = ending.lastIndexOf(".");
		//System.out.println("idx "+idx+", ending <"+ending+">");
		if(idx == -1) {
			return null;
		}
		return ending.substring(idx);
	}
	
	private static String readAllBytesJava7(File file)
	{
	    String content = "";
	    try
	    {
	        content = new String ( Files.readAllBytes( Paths.get(file.getAbsolutePath()) ) );
	    }
	    catch (IOException e)
	    {
	        e.printStackTrace();
	    }
	    return content;
	}

	public static Vector<String> getALinks(String page) {
		Pattern linkPattern = Pattern.compile(HtmlLinkOperations.linkpattern, Pattern.CASE_INSENSITIVE|Pattern.DOTALL);
		Matcher pageMatcher = null;
		try {
			pageMatcher = linkPattern.matcher(page);
		}
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		Vector<String> links = new Vector<String>();
		//int ctr = 0;
		while(pageMatcher.find()){
			String link = pageMatcher.group();
		    //System.out.println("XX-> "+(++ctr)+" "+link);
		    links.add(link);
		}
		return links;
	}
	
	public static void main(String[] args) { // 1
		String content = 
				"<html><head><title>www.3gpp.org - /ftp/Information/presentations/2017_flip_brochures/2017_LTE_logo_user_guide/files/basic-html/</title></head><body><H1>www.3gpp.org - /ftp/Information/presentations/2017_flip_brochures/2017_LTE_logo_user_guide/files/basic-html/</H1><hr>\n" + 
				"\n"+
				"<pre><A HREF=\"/ftp/Information/presentations/2017_flip_brochures/2017_LTE_logo_user_guide/files/\">[To Parent Directory]</A><br><br> 6/22/2017  3:34 PM        &lt;dir&gt; <A HREF=\"/ftp/Information/presentations/2017_flip_brochures/2017_LTE_logo_user_guide/files/basic-html/images/\">images</A><br> 6/22/2017  3:34 PM         9487 <A \n"+
				"HREF=\"/ftp/Information/presentations/2017_flip_brochures/2017_LTE_logo_user_guide/files/basic-html/index.html\">index.html</A><br> 6/22/2017  3:34 PM         9487 <A \n"+
				"HREF=\"/ftp/Information/presentations/2017_flip_brochures/2017_LTE_logo_user_guide/files/basic-html/page1.html\">page1.html</A><br> 6/22/2017  3:34 PM         7753 <A \n"+
				"HREF=\"/ftp/Information/presentations/2017_flip_brochures/2017_LTE_logo_user_guide/files/basic-html/page2.html\">page2.html</A><br> 6/22/2017  3:34 PM         7956 <A \n"+
				"HREF=\"/ftp/Information/presentations/2017_flip_brochures/2017_LTE_logo_user_guide/files/basic-html/page4.html\">page4.html</A><br> 6/22/2017  3:34 PM         7424 <A \n"+
				"HREF=\"/ftp/Information/presentations/2017_flip_brochures/2017_LTE_logo_user_guide/files/basic-html/page5.html\">page5.html</A><br></pre><hr></body></html>\n";
		Vector<String> v = getALinks(content);
		String delimiter = "/ftp/";
		
		for(String s : v) {
			Indexrow ixr = null;
			String postfix = getPostfix_withDot(s);
			if(postfix != null) {
				String href = HtmlLinkOperations.getHrefFromLink(s);
				int delpos = href.indexOf(delimiter);
				if(delpos != -1) {
					delpos += delimiter.length()-1;
				}
				String hrefAfterDelimiter = href.substring(delpos+1);
				Long len = null;
				try {
					len = getLength("www.3gpp.org/ftp/tsg_ct/TSG_CT/Docs/index.html", content, s);
				} 
				catch(Exception e1) {
					e1.printStackTrace();
					continue;
				}
				Date date = null;
				try {
					date = getFtpFilesystemDate("www.3gpp.org/ftp/tsg_ct/TSG_CT/Docs/index.html", content, s);
				} 
				catch(Exception e1) {
					e1.printStackTrace();
					continue;
				}
				System.out.println("Date "+date);
				String name = getVisibleName(s);
				ixr = null;
				if(ixr == null) {
					ixr = new Indexrow();
					ixr.setIndexfile(new Indexfile());
					ixr.setSkeletonsize(len);
					ixr.setSkeletondate(date);
					ixr.setHref(hrefAfterDelimiter);
					ixr.setName(name);
				}
				else {
					if(len == ixr.getImportsize() && hrefAfterDelimiter.equals(ixr.getHref())) {
						// Assumes it is the same
					}
					else {
						// Different size
						ixr.setSkeletonsize(len);
						ixr.setSkeletondate(date);
						ixr.setHref(hrefAfterDelimiter);
					}
				}
				ixr.setActualized(new Date());
				System.out.println(ixr.toString());
			}
		}

	}
	
	private static final String[] formats = new String[] {
			"MM/dd/yyyy  hh:mm a",
			"MM/dd/yyyy  hh:mm aa",
			"MM/dd/yyyy hh:mm a",
			"MM/dd/yyyy hh:mm aa",
			"MM/dd/yyyy  HH:mm",
			"MM/dd/yyyy  HH:mm",
			"MM/dd/yyyy HH:mm",
			"MM/dd/yyyy HH:mm",
			"MM/dd/yyyy  hh:mm:ss a",
			"MM/dd/yyyy  hh:mm:ss aa",
			"MM/dd/yyyy hh:mm:ss a",
			"MM/dd/yyyy hh:mm:ss aa",
			"MM/dd/yyyy  HH:mm:ss",
			"MM/dd/yyyy  HH:mm:ss",
			"MM/dd/yyyy HH:mm:ss",
			"MM/dd/yyyy HH:mm:ss",
			"MM/dd/yyyy",
			"MM/dd/yyyy",
	};
	
	private static final String[] testformats = new String[] {
			"M/d/yyyy",
			"MM/dd/yyyy",
			"MM/dd/yyyy  hh:mm a",
			"MM/dd/yyyy  h:mm a",
			"MM/dd/yyyy  hh:mm a",
			"MM/dd/yyyy  h:m a",
			"MM/dd/yyyy  hh:mm",
			"MM/dd/yyyy  HH:mm a",
			"MM/dd/yyyy  HH:mm aa",
			"MM/dd/yyyy  HH:mm:ss a",
			"MM/dd/yyyy  HH:mm:ss aa",
			"MM/dd/yyyy  hh:mm:ss a",
			"MM/dd/yyyy  hh:mm:ss aa"
	};
	
	public static void main2(String[] args) { // 2
		for(String format : testformats) {
			System.out.println("D1 "+format+" "+getDate("4/8/2017  1:37 PM", format));
			System.out.println("D2 "+format+" "+getDate("10/13/2017  8:45 AM", format));
			System.out.println("D3 "+format+" "+getDate("1/10/2018  4:35 PM", format));
			System.out.println("D4 "+format+" "+getDate("4/8/2017", format));
			System.out.println("D5 "+format+" "+getDate("4/8/2017  1:37", format));
			System.out.println("D6 "+format+" "+getDate("1/10/2018  11:35 PM", format));
			System.out.println("D7 "+format+" "+getDate("1/10/2018  11:35 AM", format));
			System.out.println("D6 "+format+" "+getDate("1/10/2018  11:35:00 PM", format));
			System.out.println("D7 "+format+" "+getDate("1/10/2018  11:35:00 AM", format));
		}
	}
	

    public static void main3(String[] argv) { // 3

    	Locale loc = Locale.ENGLISH;
        SimpleDateFormat formatter = new SimpleDateFormat("EEEE, MMM dd, yyyy HH:mm:ss a", loc);
        String dateInString = "Friday, Jun 7, 2013 12:10:56 PM";

        try {

            Date date = formatter.parse(dateInString);
            System.out.println(date);
            System.out.println(formatter.format(date));
            System.out.println(formatter.format(new Date()));

        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
    public static void main4(String[] argv) { // 4    	
		System.out.println(getDate("4/8/2017  1:37 PM"));
		System.out.println(getDate("1/24/2017 10:23 AM"));
		System.out.println(getDate("4/8/2017  1:37 PM"));
		System.out.println(getDate("4/8/2017  11:37 PM"));
		System.out.println(getDate("4/28/2017  11:37 PM"));
		System.out.println(getDate("4/28/2017 11:37 PM"));
    }

}
