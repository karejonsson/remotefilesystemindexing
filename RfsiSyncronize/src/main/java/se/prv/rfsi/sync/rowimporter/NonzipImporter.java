package se.prv.rfsi.sync.rowimporter;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Map;

import general.reuse.webmanipulation.Download;
import se.prv.rfsi.dbc.DocumentDB;
import se.prv.rfsi.domain.Document;
import se.prv.rfsi.domain.Indexable;
import se.prv.rfsi.domain.Indexrow;
import se.prv.rfsi.sync.docimporter.DocumentManagement;

public class NonzipImporter extends RowImporter {
	
	private String url;
	private String hashAlgo;
	private long maxHandledSize;
	//private Date dateFromCommandline;
	
	public NonzipImporter(String url, String hashAlgo, long maxHandledSize) { //, Date dateFromCommandline) {
		this.url = url;
		this.hashAlgo = hashAlgo;
		this.maxHandledSize = maxHandledSize;
		//this.dateFromCommandline = dateFromCommandline;
	}
	
	public String toString() {
		return "NonzipImporter{ url "+url+", hashAlgo "+hashAlgo+", maxHandledSize "+maxHandledSize/*+", dateFromCommandline "+dateFromCommandline*/+" }";
	}

	@Override
	public boolean handles(Indexrow ixr) {
		String name = ixr.getName();
		if(name == null) {
			ixr.setFailcode(ImportErrorCodes.nonzipItHasNoName);
			ixr.setFailmsg("There is no name. nonzip discovers. "+(new Date()));
			ixr.save();
			System.out.println("NonzipImporter: Null name "+ixr);
			return false;
		}
		return !ixr.getName().toLowerCase().endsWith(".zip");
	}

	@Override
	public void importrow(Indexrow ixr) {
		/*
		Date indexrowActuality = ixr.getActualized();
		if(indexrowActuality != null) {
			if(dateFromCommandline.getTime() >= indexrowActuality.getTime()) {
				System.out.println("NonzipImporter: Nod indexrad "+ixr+" på rätt sida om datum och indexfilen är processad");
				return;
			}
		}
		*/
		Map<String, Indexable> lookup = ixr.getIndexablesMapped();
		Indexable existing = lookup.get(ixr.getName());
		if(existing == null) {
			first(ixr);
		}
		else {
			notFirst(existing);
		}
	}

	private void notFirst(Indexable ixe) {
		Indexrow ixr = ixe.getIndexrow();
		if(ixr.getActualized().getTime() < ixe.getActualized().getTime()) {
			// The indexable has been handled after the row so it is still good.
			return;
		}
		Long importsize = ixr.getImportsize();
		if(importsize != null && importsize.equals(ixr.getSkeletonsize())) {
			// It is imported before and the last analysed skeleton says same size 
			// so it is likley enough to be good still.
			return;
		}
		setSizeAndDocument(ixe, url, hashAlgo);
		ixe.setActualized(new Date());
		ixe.save();
	}

	private void first(Indexrow ixr) {
		Indexable ixe = new Indexable();
		ixe.setIndexrow(ixr);
		ixe.setName(ixr.getName());
		setSizeAndDocument(ixe, url, hashAlgo);
		ixe.setActualized(new Date());
		ixe.save();
	}
	
	public static void setSizeAndDocument(Indexable ixe, String url, String hashAlgo) {
		Indexrow ixr = ixe.getIndexrow();
		String totUrl = url+ixr.getHref();
		try {
			byte[] contents = Download.getBytes(totUrl);
			if(contents != null) {
				ixe.setSize((long) contents.length);
				MessageDigest md = MessageDigest.getInstance(hashAlgo);
				md.update(contents);
				byte[] hash = md.digest();
				String checksum = Download.bytesToCharacterHexPresentation(hash);	
				ixe.setDocument(getDocument(contents, checksum, ixr.getName()));
				ixe.setFailcode(null);
				ixe.setFailmsg((String)null);
			}
			else {
				ixe.setSize(-1l);
				ixe.setDocument(null);
				ixe.setFailcode(ImportErrorCodes.nonzipNoContentsDownloadable);
				ixe.setFailmsg("Nothing downloadable from "+totUrl+". "+(new Date()));
			}
		} 
		catch (UnsupportedEncodingException e) {
			ixe.setFailcode(ImportErrorCodes.nonzipUnsupportedEncoding);
			ixe.setFailmsg(e.toString());
			System.out.println("Non-Zip importer");
			e.printStackTrace();
		} 
		catch (MalformedURLException e) {
			ixe.setFailcode(ImportErrorCodes.nonzipMalformedURL);
			ixe.setFailmsg(e.toString());
			System.out.println("Non-Zip importer");
			e.printStackTrace();
		} 
		catch (NoSuchAlgorithmException e) {
			ixe.setFailcode(ImportErrorCodes.nonzipNoSuchAlgo);
			ixe.setFailmsg(e.toString());
			System.out.println("Non-Zip importer");
			e.printStackTrace();
		}
	}
	
	public static Document getDocument(byte[] contents, String checksum, String filename) {
		Document doc = DocumentDB.getDocumentFromChecksum(checksum);
		if(doc != null) {
			return doc;
		}
		return DocumentManagement.createDocument(contents, checksum, filename);
	}
}
