package se.prv.rfsi.sync.docimporter;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.apache.poi.POITextExtractor;
import org.apache.poi.extractor.OLE2ExtractorFactory;

import se.prv.rfsi.dbc.DocumentDB;
import se.prv.rfsi.domain.Document;

public class PoiSpecificImporterModern extends PostfixSpecificImporter {

	public static final int importerid = 2;

	public static final String[] postfixes = new String[] {
			"doc", "docx", 
			"xls", "xlsx", 
			"ppt", "pptx",
			};

	@Override
	public boolean handles(String postfix) {
		String pxl = postfix.toLowerCase();
		for(String px : postfixes) {
			if(pxl.equals(px)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String[] getPostfixes() {
		return postfixes;
	}

	@Override
	public Document getDocument(byte[] contents, String checksum) {
		Document doc = DocumentDB.getDocumentFromChecksum(checksum);
		if(doc != null) {
			return doc;
		}
		String mimetype = getMimetype(contents);

		POITextExtractor ex = null;
		String out = null;
		try {
			ex = OLE2ExtractorFactory.createExtractor(new ByteArrayInputStream(contents));
			out = ex.getText();
		} 
		catch (IOException e) {
			System.out.println("PoiSpecificImporterModern: Got mimetype "+mimetype+" to importer "+PoiSpecificImporterModern.class.getName()+". Failed with apache poi.");
			e.printStackTrace();
		}
		catch(IllegalArgumentException iae) {
			System.out.println("PoiSpecificImporterModern: Got mimetype "+mimetype+" to importer "+PoiSpecificImporterModern.class.getName()+". Failed with apache poi.");
			iae.printStackTrace();
		}
		if(ex != null) {
			try {
				ex.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		if(out == null) {
			return null;
		}
		
		doc = new Document();
		doc.setChecksum(checksum);
		doc.setMimetype(mimetype);
		doc.setImporter(importerid);
		doc.save();
		doc.setData(out.getBytes(PostfixSpecificImporter.charset));

		return doc;
	}

}
