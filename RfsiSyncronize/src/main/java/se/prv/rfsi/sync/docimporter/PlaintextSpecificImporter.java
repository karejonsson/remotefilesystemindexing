package se.prv.rfsi.sync.docimporter;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class PlaintextSpecificImporter extends TikaBasedSpecificImporterLegacy {

	public static final int importerid = 4;
	
	protected int getImporterId() {
		return importerid;
	}
	public static final String[] postfixes = new String[] {
			"txt", 
			};

	@Override
	public boolean handles(String postfix) {
		String pxl = postfix.toLowerCase();
		for(String px : postfixes) {
			if(pxl.equals(px)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String[] getPostfixes() {
		return postfixes;
	}

	protected String getText(byte[] contents) throws IOException {
		try {
			return new String(contents, StandardCharsets.UTF_8);
		}
		catch(Exception e) {}
		try {
			return new String(contents, StandardCharsets.ISO_8859_1);
		}
		catch(Exception e) {}
		try {
			return new String(contents, StandardCharsets.US_ASCII);
		}
		catch(Exception e) {}
		return null;
	}

}