package se.prv.rfsi.sync.rowimporter;

public class ImportErrorCodes {
	
	public final static int zipMalformedUrlCreatUrl = 101;
	public final static int zipIoExceptionOpenConnection = 102;
	public final static int zipIoExceptionGetInputStream = 103;
	public final static int zipIoExceptionGetFirstEntry = 104;
	public final static int zipIoExceptionGetNextEntry = 105;
	public final static int zipIoExceptionPassStream = 106;
	public final static int zipIoExceptionReadStream = 107;
	public final static int zipMessageDigestNoAlgo = 108;
	public final static int zipIoExceptionCloseEntry = 109;
	public final static int zipIoExceptionClose = 110;
	public final static int zipImporterUnexpected = 111;
	public final static int zipExceptionReadStream = 112;
	public final static int zipExceptionUnexpected = 113;
	public final static int zipMessageDigestUnexpected = 114;
	public final static int zipExceptionGetNextEntry = 115;
	public final static int zipExceptionCloseEntry = 116;
	public final static int zipExceptionClose = 117;
	
	public final static int nonzipUnsupportedEncoding = 201;
	public final static int nonzipMalformedURL = 202;
	public final static int nonzipNoSuchAlgo = 203;
	public final static int nonzipItHasNoName = 204;
	public final static int nonzipNoContentsDownloadable = 205;
	
	public final static int rowctrlLimit = 100000;
	public final static int rowctrlUnexpectedException = 101000;

}
