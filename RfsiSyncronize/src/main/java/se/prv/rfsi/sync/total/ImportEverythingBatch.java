package se.prv.rfsi.sync.total;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import general.reuse.properties.CommandLineProperties;
import general.reuse.properties.PropertiesAssemble;
import general.reuse.timeouthandling.monitortask.ProgressWebReporter;
import general.reuse.timeouthandling.monitortask.ProgressbarForJobs;
import general.reuse.timeouthandling.task.JobOfManyJobs;
import general.reuse.timeouthandling.task.ResourceManagement;
import general.reuse.timeouthandling.task.TaskMonitorer;
import general.reuse.timeouthandling.task.TimeoutTask;
import se.prv.rfsi.dbc.IndexfileDB;
import se.prv.rfsi.domain.Indexfile;
import se.prv.rfsi.domain.Indexrow;
import se.prv.rfsi.sync.docimporter.DocumentManagement;
import se.prv.rfsi.sync.skeleton.PropertyNames;

public class ImportEverythingBatch {

	public static void main(String args[]) {
		CommandLineProperties clp = null;
		try {
			clp = PropertiesAssemble.collectCLP(args);
			clp.printEvaluated();
		}
		catch(Exception e) {
			System.out.println("Fel vid inläsningen av kommandoradsparametrarna.");
			e.printStackTrace();
			System.exit(1);						
		}
		try {
			main(clp);
		}
		catch(Exception e) {
			System.out.println("Felaktig terminering.");
			e.printStackTrace();
			System.exit(1);			
		}
		System.exit(0);			
	}

	public static void main(CommandLineProperties clp) throws Exception {
		SSHandJDBCResourceManagement rm = new SSHandJDBCResourceManagement(clp);
		rm.start();
				
		String url = clp.getString(PropertyNames.servedUrl);
		String delimiter = clp.getString(PropertyNames.delimiter);
		int idxdel = url.indexOf(delimiter);
		if(idxdel == -1) {
			System.out.println("Avgränsande sekvensen finns inte i URLen");
			System.exit(-1);
		}
		String reducedUrl = url.substring(0, idxdel);
		String usedUrl = reducedUrl+delimiter;
		String hashAlgo = clp.getString(PropertyNames.hashAlgo);
		long maxHandledSize = clp.getLong(PropertyNames.maxHandledSize);
		//String dateFormatActualized = clp.getString(PropertyNames.dateFormatActualized);
		//String dateActualizedS = clp.getString(PropertyNames.dateActualized);
		
		//SimpleDateFormat formatter = new SimpleDateFormat(dateFormatActualized);
		//Date dateFromCommandline = formatter.parse(dateActualizedS);

		ImportIndexrow iir = new ImportIndexrow(usedUrl, hashAlgo, maxHandledSize);//, dateFromCommandline);
		List<String> currentPostfixes = DocumentManagement.getPostfixes();

		List<Indexfile> ixfs = IndexfileDB.getAllIndexfiles();
		List<TaskMonitorer> rowtasks = new ArrayList<TaskMonitorer>();
		List<TaskMonitorer> filetasks = new ArrayList<TaskMonitorer>();

		String onlyThoseStartingWith = clp.getString(PropertyNames.onlyThoseStartingWith, null);
		if(onlyThoseStartingWith != null && onlyThoseStartingWith.trim().length() == 0) {
			onlyThoseStartingWith = null;
		}

		long before = System.currentTimeMillis();
		System.out.println("ImportEverythingBatch.main: Antal "+ixfs.size());
		int ctr = 0;
		for(Indexfile ixf : ixfs) {
			String folderName = ixf.getFolder();
			if(onlyThoseStartingWith != null && !folderName.startsWith(onlyThoseStartingWith)) {
				// We shall only do those starting with something that this one does not start with.
				continue;
			}
			ctr++;
			Date indexfileActuality = ixf.getActualized();
			boolean processed = ixf.isProcessed();
			if(indexfileActuality != null) {
				if(processed) {
					System.out.println("ImportEverythingBatch.main: Nod för indexfil "+ixf+" redan processad.");
					continue;
				}
			}
			boolean hasSame = ixf.hasSamePostfixes(currentPostfixes);
			if(!hasSame || !processed) {
				handlePhase2Import(iir, ixf, rowtasks);
				filetasks.add(new TaskMonitorer(new IndexfileImportTask(ixf, currentPostfixes)));
			}
			else {
				System.out.println("ImportEverythingBatch.main: Nod för indexfil "+ixf+" har samma postfix samt är redan processad.");				
			}
			System.out.println("ImportEverythingBatch.main: Antal uppgifter är "+rowtasks.size()+" efter "+ctr+" basnoder"+((onlyThoseStartingWith != null) ? " som börjar med "+onlyThoseStartingWith+".": "."));
		}
		long after = System.currentTimeMillis();
		
		System.out.println("ImportEverythingBatch.main: Hittade "+rowtasks.size()+" på "+(after-before)+" millisekunder");
		
		long first = clp.getLong(PropertyNames.firstWait);
		Long[] reattempts = clp.getLongArray(PropertyNames.reattempts);		

		JobOfManyJobs worker = new JobOfManyJobs(rowtasks, first, reattempts, rm);
		before = System.currentTimeMillis();
		if(clp.getBoolean(PropertyNames.progressbar, false)) {
			ProgressbarForJobs.run("Hantera raderna", worker, false);
		}
		else {
			Integer port = clp.getInt_Immutable(PropertyNames.webprogress, null);
			if(port != null) {
				ProgressWebReporter.run(worker, port, clp.getString(PropertyNames.webtitle, "Importing rows"));
			}
			else {
				worker.run();
			}
		}
		after = System.currentTimeMillis();
		System.out.println("ImportEverythingBatch.main: Klar med "+rowtasks.size()+" rad-uppgifter på "+(after-before)+" millisekunder");
		
		worker = new JobOfManyJobs(filetasks, first, reattempts, rm);
		before = System.currentTimeMillis();
		if(clp.getBoolean(PropertyNames.progressbar, false)) {
			ProgressbarForJobs.run("Hantera filerna", worker, false);
		}
		else {
			Integer port = clp.getInt_Immutable(PropertyNames.webprogress, null);
			if(port != null) {
				ProgressWebReporter.run(worker, port, clp.getString(PropertyNames.webtitle, "Importing indexfiles"));
			}
			else {
				worker.run();
			}
		}
		after = System.currentTimeMillis();
		System.out.println("ImportEverythingBatch.main: Klar med "+filetasks.size()+" fil-uppgifter på "+(after-before)+" millisekunder");
		
		rm.stop();
	}
	
	private static void handlePhase2Import(ImportIndexrow iir, Indexfile ixf, List<TaskMonitorer> tasks) throws IOException {
		System.out.println("ImportEverythingBatch.handlePhase2Import: Fixar "+ixf);
		List<Indexrow> ixrs = ixf.getReferringIndexrows();
		for(Indexrow ixr : ixrs) {
			Date indexrowActuality = ixr.getActualized();
			if(indexrowActuality != null) {
				if(ixf.isProcessed()) {
					System.out.println("ImportEverythingBatch.handlePhase2Import: Nod indexrad "+ixr+" och indexfilen är processad");
					continue;
				}
			}
			tasks.add(new TaskMonitorer(new IndexrowImportTask(iir, ixr)));
		}
	}
	
}
