package se.prv.rfsi.sync.total;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import general.reuse.properties.CommandLineProperties;
import general.reuse.properties.PropertiesAssemble;
import general.reuse.timeouthandling.monitortask.ProgressWebReporter;
import general.reuse.timeouthandling.monitortask.ProgressbarForJobs;
import general.reuse.timeouthandling.task.JobOfManyJobs;
import general.reuse.timeouthandling.task.TaskMonitorer;
import se.dom.rinfo.reuse.server.ServeFolder;
import se.prv.rfsi.dbc.DocumentDB;
import se.prv.rfsi.dbc.IndexfileDB;
import se.prv.rfsi.domain.Document;
import se.prv.rfsi.domain.Indexfile;
import se.prv.rfsi.domain.Indexrow;
import se.prv.rfsi.sync.docimporter.DocumentManagement;
import se.prv.rfsi.sync.rowimporter.RowImporter;
import se.prv.rfsi.sync.skeleton.ConnectionTotal;
import se.prv.rfsi.sync.skeleton.PropertyNames;

public class ResetStateOfIndexfiles {

	public static void main(String args[]) {
		CommandLineProperties clp = null;
		try {
			clp = PropertiesAssemble.collectCLP(args);
			clp.printEvaluated();
		}
		catch(Exception e) {
			System.out.println("Fel vid inläsningen av kommandoradsparametrarna.");
			e.printStackTrace();
			System.exit(1);						
		}
		try {
			main(clp);
		}
		catch(Exception e) {
			System.out.println("Felaktig terminering.");
			e.printStackTrace();
			System.exit(1);			
		}
		System.exit(0);			
	}

	public static void main(CommandLineProperties clp) throws Exception {
		SSHandJDBCResourceManagement resources = new SSHandJDBCResourceManagement(clp); 
		resources.start();
		if(resources.isActive()) {
			System.out.println("Kunde inte öppna mot DB");
			System.exit(-1);
		}
		
		List<Indexfile> ixfs = IndexfileDB.getAllIndexfiles();
		System.out.println("Antal indexfiler "+ixfs.size());
		
		String onlyThoseStartingWith = clp.getString(PropertyNames.onlyThoseStartingWith, null);
		if(onlyThoseStartingWith != null && onlyThoseStartingWith.trim().length() == 0) {
			onlyThoseStartingWith = null;
		}

		List<TaskMonitorer> tasks = new ArrayList<TaskMonitorer>();

		for(Indexfile ixf : ixfs) {
			String folderName = ixf.getFolder();
			if(onlyThoseStartingWith != null && !folderName.startsWith(onlyThoseStartingWith)) {
				// We shall only do those starting with something that this one does not start with.
				continue;
			}
			tasks.add(new TaskMonitorer(new SetIndexfileProcessedFalseTask(ixf)));//, dateFromCommandline)));
		}
		
		long first = clp.getLong(PropertyNames.firstWait);
		Long[] reattempts = clp.getLongArray(PropertyNames.reattempts);
		
		JobOfManyJobs worker = new JobOfManyJobs(tasks, first, reattempts, resources);

		long before = System.currentTimeMillis();
		if(clp.getBoolean(PropertyNames.progressbar, false)) {
			ProgressbarForJobs.run("Hantera raderna", worker, false);
		}
		else {
			Integer port = clp.getInt_Immutable(PropertyNames.webprogress, null);
			if(port != null) {
				ProgressWebReporter.run(worker, port, clp.getString(PropertyNames.webtitle, "Reset index files"));
			}
			else {
				worker.run();
			}
		}
		long after = System.currentTimeMillis();
		System.out.println("Klar med "+tasks.size()+" uppgifter på "+(after-before)+" millisekunder");

		
		resources.stop();
	}
	
}
