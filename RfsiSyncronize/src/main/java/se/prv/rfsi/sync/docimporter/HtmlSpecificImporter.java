package se.prv.rfsi.sync.docimporter;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;

public class HtmlSpecificImporter extends TikaBasedSpecificImporterLegacy {

	public static final int importerid = 3;
	
	protected int getImporterId() {
		return importerid;
	}
	
	public static final String[] postfixes = new String[] {
			"htm", "html" 
			};

	@Override
	public boolean handles(String postfix) {
		String pxl = postfix.toLowerCase();
		for(String px : postfixes) {
			if(pxl.equals(px)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String[] getPostfixes() {
		return postfixes;
	}

	protected String getText(byte[] contents) throws IOException {
		BufferedReader reader = new BufferedReader(
				new InputStreamReader(new ByteArrayInputStream(contents)));
		StringBuffer txt = new StringBuffer();
		ParserDelegator parserDelegator = new ParserDelegator();
		parserDelegator.parse(reader, 
				new HTMLEditorKit.ParserCallback() {
			public void handleText(char[] text, int pos) {
				txt.append(text);
			}
		}
		, true);
		return txt.toString();
	}

}