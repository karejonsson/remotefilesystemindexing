package se.prv.rfsi.sync.skeleton;

import se.prv.rfsi.internal.InstallationProperties;

public class PropertyNames {
	
	public final static String folder = "folder";
	public final static String delimiter = "delimiter";

	public static final String jdcb_driver_class_property = InstallationProperties.jdbc_driver_class_property;
	public static final String jdcb_url_property = InstallationProperties.jdbc_url_property;
	public static final String jdcb_username_property = InstallationProperties.jdbc_username_property;
	public static final String jdcb_password_property = InstallationProperties.jdbc_password_property;

	public final static String tunnel_lport = "prv.rfsi.ssh.tunnel_lport";
	public final static String tunnel_rhost = "prv.rfsi.ssh.tunnel_rhost";
	public final static String tunnel_rport = "prv.rfsi.ssh.tunnel_rport";
    
	public final static String jdbc_open = "prv.rfsi.ssh.jdbc.open";
	public final static String login_user = "prv.rfsi.ssh.login_user";
	public final static String login_domain = "prv.rfsi.ssh.login_domain";
	public final static String ssh_port = "prv.rfsi.ssh.ssh_port";
	public final static String login_password = "prv.rfsi.ssh.login_password";
	
	public final static String startServer = "startServer";
	public final static String serveFolder = "serveFolder";
	public final static String servePort = "servePort";
	public final static String serverThreads = "serverThreads";
	
	public final static String servedUrl = "servedUrl";
	public final static String hashAlgo = "hashAlgo";
	public final static String maxHandledSize = "maxHandledSize";
	//public final static String dateFormatActualized = "dateFormatActualized";
	//public final static String dateActualized = "dateActualized";
	public final static String progressbar = "progressbar";
	public final static String webprogress = "webprogress";
	public final static String webtitle = "webtitle";
	public final static String onlyThoseStartingWith = "onlyThoseStartingWith";
	public final static String firstWait = "firstWait";
	public final static String reattempts = "reattempts";

}
