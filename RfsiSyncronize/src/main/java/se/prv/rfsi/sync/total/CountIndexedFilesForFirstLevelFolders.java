package se.prv.rfsi.sync.total;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import general.reuse.properties.CommandLineProperties;
import general.reuse.properties.PropertiesAssemble;
import general.reuse.timeouthandling.monitortask.ProgressWebReporter;
import general.reuse.timeouthandling.monitortask.ProgressbarForJobs;
import general.reuse.timeouthandling.task.JobOfManyJobs;
import general.reuse.timeouthandling.task.TaskMonitorer;
import general.reuse.timeouthandling.task.TimeoutTask;
import se.prv.rfsi.dbc.DocumentDB;
import se.prv.rfsi.dbc.IndexableDB;
import se.prv.rfsi.domain.Document;
import se.prv.rfsi.domain.Indexable;
import se.prv.rfsi.sync.skeleton.ConnectionTotal;
import se.prv.rfsi.sync.skeleton.PropertyNames;

public class CountIndexedFilesForFirstLevelFolders {

	public static void main(String args[]) {
		CommandLineProperties clp = null;
		try {
			clp = PropertiesAssemble.collectCLP(args);
			clp.printEvaluated();
		}
		catch(Exception e) {
			System.out.println("Fel vid inläsningen av kommandoradsparametrarna.");
			e.printStackTrace();
			System.exit(1);						
		}
		try {
			main(clp);
		}
		catch(Exception e) {
			System.out.println("Felaktig terminering.");
			e.printStackTrace();
			System.exit(1);			
		}
		System.exit(0);			
	}

	public static void main(CommandLineProperties clp) throws Exception {
		SSHandJDBCResourceManagement rm = new SSHandJDBCResourceManagement(clp);
		rm.start();
		
		Map<String, Long> count = new HashMap<String, Long>();
		
		List<Indexable> ixes = IndexableDB.getAllIndexables();
		List<TaskMonitorer> tasks = new ArrayList<TaskMonitorer>();

		for(final Indexable ixe : ixes) {
			tasks.add(new TaskMonitorer(new TimeoutTask() {
				
				private boolean isDone = false;

				@Override
				public void runTimeconsuming(int attemptNo) throws InterruptedException {
					String folder = ixe.getIndexrow().getIndexfile().getFolder();
					//System.out.println("Folder "+folder);
					int idxSlash = folder.indexOf("/");
					if(idxSlash != -1) {
						folder = folder.substring(0, idxSlash);
					}
					Long sum = count.get(folder);
					if(sum == null) {
						count.put(folder, 1L);
					}
					else {
						count.put(folder, sum+1L);
					}
					isDone = true;
				}

				@Override
				public void onFinishedBeQuick(int attemptNo) { }

				@Override
				public void onTimeoutBeQuick(int attemptNo) { }

				@Override
				public void onTerminallyFailed() { }

				@Override
				public boolean isDone() {
					return isDone;
				}
				
			}));
		}
		
		long first = clp.getLong(PropertyNames.firstWait);
		Long[] reattempts = clp.getLongArray(PropertyNames.reattempts);
		
		JobOfManyJobs worker = new JobOfManyJobs(tasks, first, reattempts, rm);

		long before = System.currentTimeMillis();
		if(clp.getBoolean(PropertyNames.progressbar, false)) {
			ProgressbarForJobs.run("Hantera raderna", worker, false);
		}
		else {
			Integer port = clp.getInt_Immutable(PropertyNames.webprogress, null);
			if(port != null) {
				ProgressWebReporter.run(worker, port, clp.getString(PropertyNames.webtitle, "Counting base folders"));
			}
			else {
				worker.run();
			}
		}
		long after = System.currentTimeMillis();

		for(String key : count.keySet()) {
			System.out.println("Folder "+key+" has "+count.get(key));
		}
		
		rm.stop();
		System.out.println("AnalysePresence.main: Klar med "+tasks.size()+" fil-uppgifter på "+(after-before)+" millisekunder");

	}
	
}
