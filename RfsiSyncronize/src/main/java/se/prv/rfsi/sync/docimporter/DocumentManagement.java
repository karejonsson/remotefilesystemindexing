package se.prv.rfsi.sync.docimporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import se.prv.rfsi.domain.Document;
import se.prv.rfsi.sync.rowimporter.ZipImporter;

public class DocumentManagement {
	
	public static String getPostfix(String filename) {
		int idx = filename.lastIndexOf(".");
		if(idx == -1) {
			return null;
		}
		return filename.substring(idx+1).toLowerCase();
	}
	
	private static Map<String, List<PostfixSpecificImporter>> lookup = new HashMap<String, List<PostfixSpecificImporter>>();
	
	static {
		PdfSpecificImporter pdf = new PdfSpecificImporter();
		for(String postfix : PdfSpecificImporter.postfixes) {
			List<PostfixSpecificImporter> previous = lookup.get(postfix);
			if(previous == null) {
				previous = new ArrayList<PostfixSpecificImporter>();
				lookup.put(postfix, previous);
			}
			previous.add(pdf);
		}
		PoiSpecificImporterModern poi = new PoiSpecificImporterModern();
		for(String postfix : PoiSpecificImporterModern.postfixes) {
			List<PostfixSpecificImporter> previous = lookup.get(postfix);
			if(previous == null) {
				previous = new ArrayList<PostfixSpecificImporter>();
				lookup.put(postfix, previous);
			}
			previous.add(poi);
		}
		PoiSpecificImporterLegacyWord poilword = new PoiSpecificImporterLegacyWord();
		for(String postfix : PoiSpecificImporterLegacyWord.postfixes) {
			List<PostfixSpecificImporter> previous = lookup.get(postfix);
			if(previous == null) {
				previous = new ArrayList<PostfixSpecificImporter>();
				lookup.put(postfix, previous);
			}
			previous.add(poilword);
		}
		PoiSpecificImporterLegacyExcel poilexcel = new PoiSpecificImporterLegacyExcel();
		for(String postfix : PoiSpecificImporterLegacyExcel.postfixes) {
			List<PostfixSpecificImporter> previous = lookup.get(postfix);
			if(previous == null) {
				previous = new ArrayList<PostfixSpecificImporter>();
				lookup.put(postfix, previous);
			}
			previous.add(poilexcel);
		}
		PoiSpecificImporterLegacyPowerpoint poilppt = new PoiSpecificImporterLegacyPowerpoint();
		for(String postfix : PoiSpecificImporterLegacyPowerpoint.postfixes) {
			List<PostfixSpecificImporter> previous = lookup.get(postfix);
			if(previous == null) {
				previous = new ArrayList<PostfixSpecificImporter>();
				lookup.put(postfix, previous);
			}
			previous.add(poilppt);
		}
		RTFSpecificImporter poilrtf = new RTFSpecificImporter();
		for(String postfix : RTFSpecificImporter.postfixes) {
			List<PostfixSpecificImporter> previous = lookup.get(postfix);
			if(previous == null) {
				previous = new ArrayList<PostfixSpecificImporter>();
				lookup.put(postfix, previous);
			}
			previous.add(poilrtf);
		}
		PlaintextSpecificImporter poilpt = new PlaintextSpecificImporter();
		for(String postfix : PlaintextSpecificImporter.postfixes) {
			List<PostfixSpecificImporter> previous = lookup.get(postfix);
			if(previous == null) {
				previous = new ArrayList<PostfixSpecificImporter>();
				lookup.put(postfix, previous);
			}
			previous.add(poilpt);
		}
		HtmlSpecificImporter html = new HtmlSpecificImporter();
		for(String postfix : HtmlSpecificImporter.postfixes) {
			List<PostfixSpecificImporter> previous = lookup.get(postfix);
			if(previous == null) {
				previous = new ArrayList<PostfixSpecificImporter>();
				lookup.put(postfix, previous);
			}
			previous.add(html);
		}
		//ZipImporter zip = new ZipImporter();
	}
	
	public static List<String> getPostfixes() {
		Set<String> x = lookup.keySet();
		List<String> out = new ArrayList<String>();
		for(String s : x) {
			out.add(s);
		}
		return out;
	}
	
	public static List<PostfixSpecificImporter> getImporters(String postfix) {
		return lookup.get(postfix.toLowerCase());
	}
	
	public static boolean hasImporter(String postfix) {
		List<PostfixSpecificImporter> importers = getImporters(postfix);
		if(importers == null) {
			return false;
		}
		if(importers.size() == 0) {
			return false;
		}
		return true;
	}
	
	public static Document createDocument(byte[] contents, String checksum, String filename) {
		String postfix = getPostfix(filename);
		if(postfix == null) {
			System.out.println("Inget postfix från "+filename);
			return null;
		}
		List<PostfixSpecificImporter> importers = getImporters(postfix);
		if(importers == null) {
			System.out.println("Ingen importör för "+postfix+", som kom från "+filename);
			return null;
		}
		for(PostfixSpecificImporter importer : importers) {
			Document doc = importer.getDocument(contents, checksum);
			if(doc != null) {
				return doc;
			}
		}
		return null;
	}

}
