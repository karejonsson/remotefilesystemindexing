package se.prv.rfsi.sync.skeleton;

import general.reuse.database.pool.SimpleJDBCConnectionPool;
import general.reuse.properties.CommandLineProperties;
import general.reuse.tunneling.TunnelForwardLEncapsulation;
import general.reuse.tunneling.TunnelForwardLParameters;
import se.prv.rfsi.dbcreuse.Commons;

public class ConnectionTotal {
	
	private CommandLineProperties clp = null;
	private TunnelForwardLParameters tflp = null;
	private TunnelForwardLEncapsulation tfle = null;

	public ConnectionTotal(CommandLineProperties clp) {
		this.clp = clp;
	}
	
	public boolean open() {
		if(clp.getBoolean(PropertyNames.jdbc_open, false)) {
			tflp = new TunnelForwardLParameters();
			try {
				tflp.setLogin_domain(clp.getString(PropertyNames.login_domain));
				tflp.setLogin_password(clp.getString(PropertyNames.login_password));
				tflp.setLogin_user(clp.getString(PropertyNames.login_user));
				tflp.setSsh_port(clp.getInt(PropertyNames.ssh_port, 22));
				tflp.setTunnel_lport(clp.getInt(PropertyNames.tunnel_lport));
				tflp.setTunnel_rport(clp.getInt(PropertyNames.tunnel_rport));
				tflp.setTunnel_rhost(clp.getString(PropertyNames.tunnel_rhost));
			}
			catch(Exception e) {
				System.err.println("Tillhandahållna parametrar räcker ej.");
				e.printStackTrace();
				return false;
			}
			
			if(!tflp.hasAll()) {
				System.err.println("Erforderliga parametrar saknas\n"+tflp);
				return false;
			}
			
			tfle = null;
			try {
				tfle = new TunnelForwardLEncapsulation(tflp);
			} catch (Exception e1) {
				System.err.println("Parametrar saknas för att öppna tunneln\n"+tflp);
				e1.printStackTrace();
				return false;
			}
			
			if(!tfle.openTunnel()) {
				System.err.println("Det gick inte att öppna SSH-tunneln\n"+tflp);
				return false;
			}
		}
		
		try {
			Commons.setPool(new SimpleJDBCConnectionPool(
					clp.getString(PropertyNames.jdcb_driver_class_property),
					clp.getString(PropertyNames.jdcb_url_property),
					clp.getString(PropertyNames.jdcb_username_property),
					clp.getString(PropertyNames.jdcb_password_property)));
		}
		catch(Exception e) {
			System.err.println("Det gick inte att öppna mot databasen");
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public boolean isOpen() {
		if(tfle == null) {
			return true;
		}
		return tfle.isReasonableLikleyStillOpened();
	}
	
	public boolean close() {
		Commons.destroy();
		
		if(tfle != null) {
			return tfle.closeTunnel();
		}
		return false;
	}
	
}
