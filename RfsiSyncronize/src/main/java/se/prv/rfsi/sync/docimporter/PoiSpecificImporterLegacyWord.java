package se.prv.rfsi.sync.docimporter;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

public class PoiSpecificImporterLegacyWord extends TikaBasedSpecificImporterLegacy {

	public static final int importerid = 7;
	
	protected int getImporterId() {
		return importerid;
	}

	public static final String[] postfixes = new String[] {
			"doc", "docx", 
			};

	@Override
	public boolean handles(String postfix) {
		String pxl = postfix.toLowerCase();
		for(String px : postfixes) {
			if(pxl.equals(px)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String[] getPostfixes() {
		return postfixes;
	}

	protected String getText(byte[] contents) throws IOException {
	    XWPFDocument xwpfdoc = new XWPFDocument(new ByteArrayInputStream(contents));
	    XWPFWordExtractor ex = new XWPFWordExtractor(xwpfdoc);
		String out = ex.getText();
		ex.close();
	    return out;
	}

}
