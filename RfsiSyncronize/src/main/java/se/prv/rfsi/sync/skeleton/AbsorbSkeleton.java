package se.prv.rfsi.sync.skeleton;

import java.io.File;
import java.util.List;

import general.reuse.properties.CommandLineProperties;
import general.reuse.properties.PropertiesAssemble;
import general.reuse.timeouthandling.monitortask.ProgressWebReporter;
import general.reuse.timeouthandling.monitortask.ProgressbarForJobs;
import general.reuse.timeouthandling.task.JobOfManyJobs;
import general.reuse.timeouthandling.task.TaskMonitorer;

public class AbsorbSkeleton {

	public static void main(String args[]) {
		CommandLineProperties clp = null;
		try {
			clp = PropertiesAssemble.collectCLP(args);
			clp.printEvaluated();
		}
		catch(Exception e) {
			System.out.println("Fel vid inläsningen av kommandoradsparametrarna.");
			e.printStackTrace();
			System.exit(1);						
		}
		try {
			main(clp);
		}
		catch(Exception e) {
			System.out.println("Felaktig terminering.");
			e.printStackTrace();
			System.exit(1);			
		}
		System.exit(0);			
	}

	public static void main(CommandLineProperties clp) throws Exception {

		String folder = null;
		try {
			folder = clp.getString(PropertyNames.folder);
		} catch (Exception e1) {
			e1.printStackTrace();
			return;
		}
		File f = new File(folder);
		if(!f.exists()) {
			System.err.println("Katalogen "+folder+" finns inte.");
			return;
		}
		if(!f.isDirectory()) {
			System.err.println("På filsystemet skulle "+folder+" ha varit en katalog men är det inte.");
			return;
		}
		
		AbsorbtionSkeletonResourceManagement resources = new AbsorbtionSkeletonResourceManagement(clp);
		resources.start();
		
		System.out.println("Läser in index.html-filerna");
		long before = System.currentTimeMillis();
		CreateAllIndexhtml cai = new CreateAllIndexhtml(clp.getString(PropertyNames.delimiter));
		cai.lookupAllIndexFiles_createTasks(new File(clp.getString(PropertyNames.folder)));
		cai.print();
		long after = System.currentTimeMillis();
		
		List<TaskMonitorer> indexFileTasks = cai.getIndexFileTasks(); 
		System.out.println("Fann "+indexFileTasks.size()+" index.html-filer på "+(after-before)+" millisekunder.");
		
		long firstWaitMillis = clp.getLong(PropertyNames.firstWait);
		Long[] reattempts = clp.getLongArray(PropertyNames.reattempts);
				
		JobOfManyJobs worker = new JobOfManyJobs(indexFileTasks, firstWaitMillis, reattempts, resources);
		before = System.currentTimeMillis();
		if(clp.getBoolean(PropertyNames.progressbar, false)) {
			ProgressbarForJobs.run("Hantera indexfilerna", worker, false);
		}
		else {
			Integer port = clp.getInt_Immutable(PropertyNames.webprogress, null);
			if(port != null) {
				ProgressWebReporter.run(worker, port, clp.getString(PropertyNames.webtitle, "Absorbing"));
			}
			else {
				worker.run();
			}
		}
		after = System.currentTimeMillis();
		System.out.println("Klar med "+indexFileTasks.size()+" indexfiler på "+(after-before)+" millisekunder");
		
		resources.stop();

	}
	
}
