package se.prv.rfsi.sync.total;

import java.util.Date;
import java.util.List;

import general.reuse.timeouthandling.task.TimeoutTask;
import se.prv.rfsi.domain.Indexfile;
import se.prv.rfsi.domain.Indexrow;
import se.prv.rfsi.sync.docimporter.DocumentManagement;

public class IndexrowImportTask implements TimeoutTask {
	
	private ImportIndexrow iir;
	private Indexrow ixr;
	
	public IndexrowImportTask(ImportIndexrow iir, Indexrow ixr) {
		this.iir = iir;
		this.ixr = ixr;
	}
	
	private boolean done = false;

	@Override
	public void runTimeconsuming(int attemptNo) throws InterruptedException {
		iir.handlePhase2Import(ixr);
		done = true;
	}

	@Override
	public void onFinishedBeQuick(int attemptNo) {
		ixr.setActualized(new Date());
		ixr.save();
	}

	@Override
	public void onTimeoutBeQuick(int attemptNo) {
	}

	@Override
	public void onTerminallyFailed() {
	}

	@Override
	public boolean isDone() {
		return done;
	}

}
