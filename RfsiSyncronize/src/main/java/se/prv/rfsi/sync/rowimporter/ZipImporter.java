package se.prv.rfsi.sync.rowimporter;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipInputStream;

import general.reuse.webmanipulation.Download;
import se.prv.rfsi.domain.Indexable;
import se.prv.rfsi.domain.Indexrow;
import se.prv.rfsi.internal.Services;
import se.prv.rfsi.sync.docimporter.DocumentManagement;

public class ZipImporter extends RowImporter {

	private String url;
	private String hashAlgo;
	private long maxHandledSize;
	
	public ZipImporter(String url, String hashAlgo, long maxHandledSize) { //, Date dateFromCommandline) {
		this.url = url;
		this.hashAlgo = hashAlgo;
		this.maxHandledSize = maxHandledSize;
	}
	
	public String toString() {
		return "ZipImporter{ url "+url+", hashAlgo "+hashAlgo+", maxHandledSize "+maxHandledSize/*+", dateFromCommandline "+dateFromCommandline*/+" }";
	}

	@Override
	public boolean handles(Indexrow ixr) {
		String name = ixr.getName();
		if(name == null) {
			System.out.println("ZipImporter: Null name "+ixr);
			return false;
		}
		return ixr.getName().toLowerCase().endsWith(".zip");
	}
	
	@Override
	public void importrow(Indexrow ixr) {
		/*
		Date indexrowActuality = ixr.getActualized();
		if(indexrowActuality != null) {
			if(dateFromCommandline.getTime() >= indexrowActuality.getTime()) {
				System.out.println("ZipImporter: Nod indexrad "+ixr+" på rätt sida om datum och indexfilen är processad");
				return;
			}
		}
		*/
		//System.out.println("ZipImporter: importsize "+ixr.getImportsize());
		//System.out.println("ZipImporter: skeletonsize "+ixr.getSkeletonsize());
		Long importsize = ixr.getImportsize();
		if(importsize != null && importsize.equals(ixr.getSkeletonsize())) {
			// It is imported before and the last analysed skeleton says same size 
			// so it is likley enough to be good still.
			return;
		}
		//System.out.println("ZipImporter: ---1 ");
		
		Map<String, Indexable> lookup = ixr.getIndexablesMapped();
		
		String toturl = url+ixr.getHref();
		URL download = null;
		try {
			download = new URL(toturl);
		} catch (MalformedURLException e) {
			ixr.setFailcode(ImportErrorCodes.zipMalformedUrlCreatUrl);
			ixr.setFailmsg(e);
			ixr.save();
			e.printStackTrace();
			return;
		}
		URLConnection urlc = null;
		try {
			urlc = download.openConnection();
		} catch (IOException e) {
			ixr.setFailcode(ImportErrorCodes.zipIoExceptionOpenConnection);
			ixr.setFailmsg(e);
			ixr.save();
			e.printStackTrace();
			return;
		}
		InputStream input = null;
		try {
			input = urlc.getInputStream();
		} catch (IOException e) {
			ixr.setFailcode(ImportErrorCodes.zipIoExceptionGetInputStream);
			ixr.setFailmsg(e);
			ixr.save();
			e.printStackTrace();
			return;
		}
		try {
			importFromStream(lookup, ixr, input);
			ixr.save();
		}
		catch(Exception e) {
			ixr.setFailcode(ImportErrorCodes.zipImporterUnexpected);
			ixr.setFailmsg(e);
			ixr.save();
			e.printStackTrace();
		}
	}
	
	private void importFromStream(Map<String, Indexable> lookup, Indexrow ixr, InputStream input) {
		ZipInputStream zip = new ZipInputStream(input);
		ZipEntry entry = null;
		try {
			entry = zip.getNextEntry();
		} catch (IOException e) {
			ixr.setFailcode(ImportErrorCodes.zipIoExceptionGetFirstEntry);
			ixr.setFailmsg(e);
			e.printStackTrace();
			return;
		}

		while(entry != null) {
			String filename = entry.getName();
			System.out.println("---------------------------"+filename);
			long uncompressedSize = entry.getSize();
			byte[] contents = null;
			if(uncompressedSize > maxHandledSize) {
				long length = -1;
				try {
					length = Services.passStream(zip);
				} catch (IOException e) {
					ixr.setFailcode(ImportErrorCodes.zipIoExceptionPassStream);
					ixr.setFailmsg(e);
					e.printStackTrace();
				}
				toLarge(ixr, length, filename);
			}
			else {
				try {
					contents = Services.getByteArrayFromStream(zip);
				} 
				catch(ZipException ze) {
					ixr.setFailcode(ImportErrorCodes.zipExceptionReadStream);
					ixr.setFailmsg(ze);
					ze.printStackTrace();					
				}
				catch(IOException e) {
					ixr.setFailcode(ImportErrorCodes.zipIoExceptionReadStream);
					ixr.setFailmsg(e);
					e.printStackTrace();
				}
				catch(Exception e) {
					ixr.setFailcode(ImportErrorCodes.zipExceptionUnexpected);
					ixr.setFailmsg(e);
					e.printStackTrace();					
				}
				if(filename.toLowerCase().endsWith(".zip")) {
					importFromStream(lookup, ixr, new ByteArrayInputStream(contents));
				}
				
				MessageDigest md = null;
				try {
					md = MessageDigest.getInstance(hashAlgo);
				} 
				catch(NoSuchAlgorithmException e) {
					ixr.setFailcode(ImportErrorCodes.zipMessageDigestNoAlgo);
					ixr.setFailmsg(e);
					e.printStackTrace();
				}
				catch(Exception e) {
					ixr.setFailcode(ImportErrorCodes.zipMessageDigestUnexpected);
					ixr.setFailmsg(e);
					e.printStackTrace();
				}
				md.update(contents);
				byte[] hash = md.digest();
				String checksum = Download.bytesToCharacterHexPresentation(hash);
				Indexable ixe = lookup.get(filename);
				if(ixe == null) {
					first(ixr, contents, checksum, filename);
				}
				else {
					notFirst(ixe, contents, checksum, filename);
				}
			}
			
			try {
				entry = zip.getNextEntry();
			} 
			catch(IOException e) {
				ixr.setFailcode(ImportErrorCodes.zipIoExceptionGetNextEntry);
				ixr.setFailmsg(e);
				e.printStackTrace();
			}
			catch(Exception e) {
				ixr.setFailcode(ImportErrorCodes.zipExceptionGetNextEntry);
				ixr.setFailmsg(e);
				e.printStackTrace();
			}
		}

		try {
			zip.closeEntry();
		} 
		catch(IOException e) {
			ixr.setFailcode(ImportErrorCodes.zipIoExceptionCloseEntry);
			ixr.setFailmsg(e);
			e.printStackTrace();
		}
		catch(Exception e) {
			ixr.setFailcode(ImportErrorCodes.zipExceptionCloseEntry);
			ixr.setFailmsg(e);
			e.printStackTrace();
		}
		try {
			zip.close();
		} 
		catch(IOException e) {
			ixr.setFailcode(ImportErrorCodes.zipIoExceptionClose);
			ixr.setFailmsg(e);
			e.printStackTrace();
		}
		catch(Exception e) {
			ixr.setFailcode(ImportErrorCodes.zipExceptionClose);
			ixr.setFailmsg(e);
			e.printStackTrace();
		}
		ixr.setImportsize(ixr.getSkeletonsize());
	}
	
	private void notFirst(Indexable ixe, byte[] contents, String checksum, String filename) {
		Indexrow ixr = ixe.getIndexrow();
		if(ixr.getActualized().getTime() < ixe.getActualized().getTime()) {
			// The indexable has been handled after the row so it is still good.
			return;
		}
		
		ixe.setSize(new Long(contents.length));
		ixe.setDocument(NonzipImporter.getDocument(contents, checksum, filename));
		
		ixe.setActualized(new Date());
		ixe.save();
	}

	private void first(Indexrow ixr, byte[] contents, String checksum, String filename) {
		Indexable ixe = new Indexable();
		ixe.setIndexrow(ixr);
		ixe.setName(filename);

		ixe.setSize(new Long(contents.length));
		ixe.setDocument(NonzipImporter.getDocument(contents, checksum, filename));
		
		ixe.setActualized(new Date());
		ixe.save();
	}

	private void toLarge(Indexrow ixr, long length, String filename) {
		Indexable ixe = new Indexable();
		ixe.setIndexrow(ixr);
		ixe.setName(filename);

		ixe.setSize(length);
		ixe.setDocument(null);
		
		ixe.setActualized(new Date());
		ixe.save();
		System.out.println("ZipImporter: To large file "+ixr);
	}

}
