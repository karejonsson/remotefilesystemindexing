package se.prv.rfsi.sync.docimporter;

import java.io.IOException;

import se.prv.rfsi.dbc.DocumentDB;
import se.prv.rfsi.domain.Document;

public abstract class TikaBasedSpecificImporterLegacy extends PostfixSpecificImporter {

	@Override
	public Document getDocument(byte[] contents, String checksum) {
		Document doc = DocumentDB.getDocumentFromChecksum(checksum);
		if(doc != null) {
			return doc;
		}
		String mimetype = getMimetype(contents);
		String text = null;
		try {
		    text = getText(contents);
		} catch (IOException e) {
			System.out.println("TikaBasedSpecificImporterLegacy: Got mimetype "+mimetype+" to importer "+PoiSpecificImporterModern.class.getName()+". Failed with apache poi.");
			e.printStackTrace();
		}
		
		if(text == null) {
			return null;
		}
		
		doc = new Document();
		doc.setChecksum(checksum);
		doc.setMimetype(mimetype);
		doc.setImporter(getImporterId());
		doc.save();
		if(text != null) {
			doc.setData(text.getBytes(PostfixSpecificImporter.charset));
		}
		return doc;
	}
	
	protected abstract String getText(byte[] contents) throws IOException;
	
	protected abstract int getImporterId();
	
}
