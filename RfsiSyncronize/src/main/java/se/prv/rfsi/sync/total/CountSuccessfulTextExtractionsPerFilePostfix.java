package se.prv.rfsi.sync.total;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import general.reuse.properties.CommandLineProperties;
import general.reuse.properties.PropertiesAssemble;
import se.prv.rfsi.dbc.IndexableDB;
import se.prv.rfsi.dbc.IndexfileDB;
import se.prv.rfsi.domain.Indexable;
import se.prv.rfsi.domain.Indexfile;
import se.prv.rfsi.sync.docimporter.DocumentManagement;
import se.prv.rfsi.sync.skeleton.ConnectionTotal;

public class CountSuccessfulTextExtractionsPerFilePostfix {

	public static void main(String args[]) {
		CommandLineProperties clp = null;
		try {
			clp = PropertiesAssemble.collectCLP(args);
			clp.printEvaluated();
		}
		catch(Exception e) {
			System.out.println("Fel vid inläsningen av kommandoradsparametrarna.");
			e.printStackTrace();
			System.exit(1);						
		}
		try {
			main(clp);
		}
		catch(Exception e) {
			System.out.println("Felaktig terminering.");
			e.printStackTrace();
			System.exit(1);			
		}
		System.exit(0);			
	}

	public static void main(CommandLineProperties clp) throws Exception {
		ConnectionTotal ct = new ConnectionTotal(clp); 
		if(!ct.open()) {
			System.out.println("Kunde inte öppna mot DB");
			System.exit(-1);
		}

		//List<String> currentPostfixes = DocumentManagement.getPostfixes();
		Map<String, Stats> lookup = new HashMap<String, Stats>();
		List<Indexable> ixes = IndexableDB.getAllIndexables();
		System.out.println("Antal "+ixes.size());
		for(Indexable ixe : ixes) {
			String postfix = DocumentManagement.getPostfix(ixe.getName());
			Stats s = lookup.get(postfix);
			if(s == null) {
				s = new Stats();
				s.postfix = postfix;
				lookup.put(postfix, s);
			}
			if(ixe.getDocument() == null) {
				s.bad++;
			}
			else {
				s.good++;
			}
		}
		for(String key : lookup.keySet()) {
			Stats s = lookup.get(key);
			System.out.println(s.toString());
		}
		ct.close();
	}

	public static class Stats {
		public String postfix;
		public int good = 0;
		public int bad = 0;
		public String toString() {
			return "Postfix "+postfix+", good "+good+", bad "+bad;
		}
	}
	
}
