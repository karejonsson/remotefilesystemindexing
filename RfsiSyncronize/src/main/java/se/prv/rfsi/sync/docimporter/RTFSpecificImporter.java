package se.prv.rfsi.sync.docimporter;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.rtf.RTFEditorKit;

public class RTFSpecificImporter extends TikaBasedSpecificImporterLegacy {

	public static final int importerid = 8;
	
	protected int getImporterId() {
		return importerid;
	}

	public static final String[] postfixes = new String[] {
			"rtf", 
			};

	@Override
	public boolean handles(String postfix) {
		String pxl = postfix.toLowerCase();
		for(String px : postfixes) {
			if(pxl.equals(px)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String[] getPostfixes() {
		return postfixes;
	}

	protected String getText(byte[] contents) throws IOException {
		RTFEditorKit kit = new RTFEditorKit();
		Document doc = kit.createDefaultDocument();
		try {
			kit.read(new ByteArrayInputStream(contents), doc, 0);
			return doc.getText(0, doc.getLength());
		} catch (BadLocationException e) {
			System.out.println("RTFSpecificImporter: Got bad location to importer "+RTFSpecificImporter.class.getName()+". Failed with RTFEditorKit.");
			e.printStackTrace();
		}
		return null;
	}

}