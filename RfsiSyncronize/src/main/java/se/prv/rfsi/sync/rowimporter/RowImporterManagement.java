package se.prv.rfsi.sync.rowimporter;

import java.util.Date;

public class RowImporterManagement {
	
	public static RowImporter[] getRowImporters(String url, String hashAlgo, long maxHandledSize) { //, Date dateFromCommandline) {
		return new RowImporter[] { new NonzipImporter(url, hashAlgo, maxHandledSize), new ZipImporter(url, hashAlgo, maxHandledSize) };
	}

}
