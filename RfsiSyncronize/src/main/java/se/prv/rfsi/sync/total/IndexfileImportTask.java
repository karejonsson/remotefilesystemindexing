package se.prv.rfsi.sync.total;

import java.util.Date;
import java.util.List;

import general.reuse.timeouthandling.task.TimeoutTask;
import se.prv.rfsi.domain.Indexfile;
import se.prv.rfsi.domain.Indexrow;

public class IndexfileImportTask implements TimeoutTask {
	
	private Indexfile ixf;
	private List<String> currentPostfixes;
	
	public IndexfileImportTask(Indexfile ixf, List<String> currentPostfixes) {
		this.ixf = ixf;
		this.currentPostfixes = currentPostfixes;
	}
	
	private boolean done = false;

	@Override
	public void runTimeconsuming(int attemptNo) throws InterruptedException {
		List<Indexrow> subjacents = ixf.getReferringIndexrows();
		Date indexfileActuality = ixf.getActualized();
		boolean allAreFine = true;
		for(Indexrow subjacent : subjacents) {
			Date indexrowActuality = subjacent.getActualized();
			if(indexrowActuality != null) {
				if(indexfileActuality.getTime() < indexrowActuality.getTime()) {
					continue;
				}
			}
			allAreFine = false;
		}
		if(allAreFine) {
			ixf.setActualized(new Date());
			ixf.setPostfixes(currentPostfixes);
			ixf.setProcess(true);
			ixf.save();
		}
		done = true;
	}

	@Override
	public void onFinishedBeQuick(int attemptNo) {
		ixf.setActualized(new Date());
		ixf.save();
	}

	@Override
	public void onTimeoutBeQuick(int attemptNo) {
	}

	@Override
	public void onTerminallyFailed() {
	}

	@Override
	public boolean isDone() {
		return done;
	}

}
