package se.prv.rfsi.unzip;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipInputStream;

import general.reuse.webmanipulation.Download;
import se.prv.rfsi.internal.Services;
import se.prv.rfsi.sync.rowimporter.ImportErrorCodes;
import se.prv.rfsi.sync.rowimporter.NonzipImporter;

public class VerifyDevelopUnZip {
	
	private static long maxHandledSize = 100000000l;
	private static String hashAlgo = "sha-256";

	public static void main(String[] args) throws IOException {
        ClassLoader cl = VerifyDevelopUnZip.class.getClassLoader();
        InputStream input = cl.getResourceAsStream("NP-020035.zip");
        Map<String, Object> lookup = new HashMap<String, Object>();
        importFromStream(lookup, input);
 	}
	
	public static void importFromStream(Map<String, Object> lookup, InputStream input) throws IOException {
		ZipInputStream zip = new ZipInputStream(input);
		ZipEntry entry = null;
		try {
			entry = zip.getNextEntry();
		} catch (IOException e) {
			System.out.println("Fel ImportErrorCodes.zipIoExceptionGetFirstEntry = "+ImportErrorCodes.zipIoExceptionGetFirstEntry);
			e.printStackTrace();
			return;
		}

		while(entry != null) {
			System.out.println("Method "+entry.getMethod());
			System.out.println("Crc "+entry.getCrc());
			String filename = entry.getName();
			System.out.println("---------------------------"+filename);
			long uncompressedSize = entry.getSize();
			if(entry.getMethod() != ZipEntry.DEFLATED && entry.getMethod() != ZipEntry.STORED) {
				// Kan göra något här. Filen kan inte zippas upp.
			}
			byte[] contents = null;
			if(uncompressedSize > maxHandledSize) {
				long length = -1;
				try {
					length = Services.passStream(zip);
				} 
				catch(IOException e) {
					System.out.println("Fel ImportErrorCodes.zipIoExceptionPassStream = "+ImportErrorCodes.zipIoExceptionPassStream);
					e.printStackTrace();
				}
				toLarge(length, filename);
			}
			else {
				try {
					contents = Services.getByteArrayFromStream(zip);
				} 
				catch(ZipException ze) {
					System.out.println("Fel ImportErrorCodes.zipExceptionReadStream = "+ImportErrorCodes.zipExceptionReadStream);
					ze.printStackTrace();					
				}
				catch(IOException e) {
					System.out.println("Fel ImportErrorCodes.zipIoExceptionReadStream = "+ImportErrorCodes.zipIoExceptionReadStream);
					e.printStackTrace();
				}
				catch(Exception e) {
					System.out.println("Fel ImportErrorCodes.zipExceptionUnexpected = "+ImportErrorCodes.zipExceptionUnexpected);
					e.printStackTrace();					
				}
				if(filename.toLowerCase().endsWith(".zip")) {
					importFromStream(lookup, new ByteArrayInputStream(contents));
				}
				
				MessageDigest md = null;
				try {
					md = MessageDigest.getInstance(hashAlgo);
				} 
				catch(NoSuchAlgorithmException e) {
					System.out.println("Fel ImportErrorCodes.zipMessageDigestNoAlgo = "+ImportErrorCodes.zipMessageDigestNoAlgo);
					e.printStackTrace();
				}
				catch(Exception e) {
					System.out.println("Fel ImportErrorCodes.zipMessageDigestUnexpected = "+ImportErrorCodes.zipMessageDigestUnexpected);
					e.printStackTrace();
				}
				md.update(contents);
				byte[] hash = md.digest();
				String checksum = Download.bytesToCharacterHexPresentation(hash);
				Object ixe = lookup.get(filename);
				if(ixe == null) {
					first(contents, checksum, filename);
				}
				else {
					notFirst(contents, checksum, filename);
				}
			}
			
			try {
				entry = zip.getNextEntry();
			} 
			catch(IOException e) {
				System.out.println("Fel ImportErrorCodes.zipIoExceptionGetNextEntry = "+ImportErrorCodes.zipIoExceptionGetNextEntry);
				e.printStackTrace();
			}
			catch(Exception e) {
				System.out.println("Fel ImportErrorCodes.zipExceptionGetNextEntry = "+ImportErrorCodes.zipExceptionGetNextEntry);
				e.printStackTrace();
			}
		}

		try {
			zip.closeEntry();
		} 
		catch(IOException e) {
			System.out.println("Fel ImportErrorCodes.zipIoExceptionCloseEntry = "+ImportErrorCodes.zipIoExceptionCloseEntry);
			e.printStackTrace();
		}
		catch(Exception e) {
			System.out.println("Fel ImportErrorCodes.zipExceptionCloseEntry = "+ImportErrorCodes.zipExceptionCloseEntry);
			e.printStackTrace();
		}
		try {
			zip.close();
		} 
		catch(IOException e) {
			System.out.println("Fel ImportErrorCodes.zipIoExceptionClose = "+ImportErrorCodes.zipIoExceptionClose);
			e.printStackTrace();
		}
		catch(Exception e) {
			System.out.println("Fel ImportErrorCodes.zipExceptionClose = "+ImportErrorCodes.zipExceptionClose);
			e.printStackTrace();
		}
	}

	private static void notFirst(byte[] contents, String checksum, String filename) {
		System.out.println("ZipImporter: NOT First file!, checksum "+checksum+", filename "+filename);
	}

	private static void first(byte[] contents, String checksum, String filename) {
		System.out.println("ZipImporter: First file!, checksum "+checksum+", filename "+filename);
	}

	private static void toLarge(long length, String filename) {
		System.out.println("ZipImporter: To large file!, length "+length+", filename "+filename);
	}

	
	
	/*
	private void readEnd(ZipEntry e) throws IOException {
		int n = inf.getRemaining();
		if (n > 0) {
			((PushbackInputStream) in).unread(buf, len - n, n);
		}
		if ((flag & 8) == 8) {
	    / * "Data Descriptor" present * /
			readFully(tmpbuf, 0, EXTHDR);
			long sig = get32(tmpbuf, 0);
			if (sig != EXTSIG) { // no EXTSIG present
				e.crc = sig;
				e.csize = get32(tmpbuf, EXTSIZ - EXTCRC);
				e.size = get32(tmpbuf, EXTLEN - EXTCRC);
				((PushbackInputStream) in).unread(tmpbuf, EXTHDR - EXTCRC - 1, EXTCRC);
			} else {
				e.crc = get32(tmpbuf, EXTCRC);
				e.csize = get32(tmpbuf, EXTSIZ);
				e.size = get32(tmpbuf, EXTLEN);
			}
		}
		if (e.size != inf.getBytesWritten()) {
			throw new ZipException("invalid entry size (expected " + e.size + " but got " + inf.getBytesWritten() + " bytes)");
		}
		if (e.csize != inf.getBytesRead()) {
			throw new ZipException("invalid entry compressed size (expected " + e.csize + " but got " + inf.getBytesRead() + " bytes)");
		}
		if (e.crc != crc.getValue()) {
			throw new ZipException("invalid entry CRC (expected 0x" + Long.toHexString(e.crc) + " but got 0x" + Long.toHexString(crc.getValue()) + ")");
		}
	}

	private void readFully(byte[] b, int off, int len) throws IOException {
		while (len > 0) {
			int n = in.read(b, off, len);
			if (n == -1) {
				throw new EOFException();
			}
			off += n;
			len -= n;
		}
	}
	*/
	
	/*
	 * Header signatures
	 */
	static long LOCSIG = 0x04034b50L;    // "PK\003\004"
	static long EXTSIG = 0x08074b50L;    // "PK\007\008"
	static long CENSIG = 0x02014b50L;    // "PK\001\002"
	static long ENDSIG = 0x06054b50L;    // "PK\005\006"

	/*
	 * Header sizes in bytes (including signatures)
	 */
	static final int LOCHDR = 30;    // LOC header size
	static final int EXTHDR = 16;    // EXT header size
	static final int CENHDR = 46;    // CEN header size
	static final int ENDHDR = 22;    // END header size

	/*
	 * Local file (LOC) header field offsets
	 */
	static final int LOCVER = 4;    // version needed to extract
	static final int LOCFLG = 6;    // general purpose bit flag
	static final int LOCHOW = 8;    // compression method
	static final int LOCTIM = 10;    // modification time
	static final int LOCCRC = 14;    // uncompressed file crc-32 value
	static final int LOCSIZ = 18;    // compressed size
	static final int LOCLEN = 22;    // uncompressed size
	static final int LOCNAM = 26;    // filename length
	static final int LOCEXT = 28;    // extra field length

	/*
	 * Extra local (EXT) header field offsets
	 */
	static final int EXTCRC = 4;    // uncompressed file crc-32 value
	static final int EXTSIZ = 8;    // compressed size
	static final int EXTLEN = 12;    // uncompressed size

	/*
	 * Central directory (CEN) header field offsets
	 */
	static final int CENVEM = 4;    // version made by
	static final int CENVER = 6;    // version needed to extract
	static final int CENFLG = 8;    // encrypt, decrypt flags
	static final int CENHOW = 10;    // compression method
	static final int CENTIM = 12;    // modification time
	static final int CENCRC = 16;    // uncompressed file crc-32 value
	static final int CENSIZ = 20;    // compressed size
	static final int CENLEN = 24;    // uncompressed size
	static final int CENNAM = 28;    // filename length
	static final int CENEXT = 30;    // extra field length
	static final int CENCOM = 32;    // comment length
	static final int CENDSK = 34;    // disk number start
	static final int CENATT = 36;    // internal file attributes
	static final int CENATX = 38;    // external file attributes
	static final int CENOFF = 42;    // LOC header offset

	/*
	 * End of central directory (END) header field offsets
	 */
	static final int ENDSUB = 8;    // number of entries on this disk
	static final int ENDTOT = 10;    // total number of entries
	static final int ENDSIZ = 12;    // central directory size in bytes
	static final int ENDOFF = 16;    // offset of first CEN header
	static final int ENDCOM = 20;    // zip file comment length
}
