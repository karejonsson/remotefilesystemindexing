
--prv.rfsi.jdbc.driverclass org.postgresql.Driver
--prv.rfsi.jdbc.domain localhost
--prv.rfsi.jdbc.port ${prv.rfsi.ssh.jdbc.tunnel_lport}
--prv.rfsi.jdbc.sid indexdb
--prv.rfsi.jdbc.url jdbc:postgresql://${prv.rfsi.jdbc.domain}:${prv.rfsi.jdbc.port}/${prv.rfsi.jdbc.sid}
--prv.rfsi.jdbc.username prvuser
--prv.rfsi.jdbc.password prvpwd

--prv.rfsi.ssh.jdbc.tunnel_lport 5445
--prv.rfsi.ssh.jdbc.tunnel_rhost localhost
--prv.rfsi.ssh.jdbc.tunnel_rport 5432
    
--prv.rfsi.ssh.jdbc.login_user index
--prv.rfsi.ssh.jdbc.login_domain 172.17.25.14
--prv.rfsi.ssh.jdbc.login_password pwd123

--prv.rfsi.ssh.es1.open true
--prv.rfsi.ssh.es1.tunnel_lport 9093
--prv.rfsi.ssh.es1.tunnel_rhost localhost
--prv.rfsi.ssh.es1.tunnel_rport 9300

--prv.rfsi.ssh.es2.open true
--prv.rfsi.ssh.es2.tunnel_lport 9090
--prv.rfsi.ssh.es2.tunnel_rhost localhost
--prv.rfsi.ssh.es2.tunnel_rport 9200
    
--prv.rfsi.ssh.es.login_user index
--prv.rfsi.ssh.es.login_domain 172.17.25.14
--prv.rfsi.ssh.es.login_password pwd123

--prv.rfsi.ssh.ssh_port 22

--es_clustername cluster3gpp
--es_publishHost localhost
--es_networkPort 9093

--es_indexName idxdoc3gpp
--es_documentType doc3gpp
--es_filename_analyser es_analysis.json
--es_filename_mapping es_mapping.json

-------------------------------------------------------------------

./start.sh se.prv.rfsi.idx.create.CreateIndexFromPremadeTextsInDatabase --prv.rfsi.jdbc.driverclass org.postgresql.Driver --prv.rfsi.jdbc.domain localhost --prv.rfsi.jdbc.port \${prv.rfsi.ssh.jdbc.tunnel_lport} --prv.rfsi.jdbc.sid indexdb --prv.rfsi.jdbc.url jdbc:postgresql://\${prv.rfsi.jdbc.domain}:\${prv.rfsi.jdbc.port}/\${prv.rfsi.jdbc.sid} --prv.rfsi.jdbc.username prvuser --prv.rfsi.jdbc.password prvpwd --prv.rfsi.ssh.jdbc.tunnel_lport 5445 --prv.rfsi.ssh.jdbc.tunnel_rhost localhost --prv.rfsi.ssh.jdbc.tunnel_rport 5432 --prv.rfsi.ssh.jdbc.login_user index --prv.rfsi.ssh.jdbc.login_domain 172.17.25.14 --prv.rfsi.ssh.jdbc.login_password pwd123 --prv.rfsi.ssh.es1.open true --prv.rfsi.ssh.es1.tunnel_lport 9093 --prv.rfsi.ssh.es1.tunnel_rhost localhost --prv.rfsi.ssh.es1.tunnel_rport 9300 --prv.rfsi.ssh.es2.open true --prv.rfsi.ssh.es2.tunnel_lport 9090 --prv.rfsi.ssh.es2.tunnel_rhost localhost --prv.rfsi.ssh.es2.tunnel_rport 9200 --prv.rfsi.ssh.es.login_user index --prv.rfsi.ssh.es.login_domain 172.17.25.14 --prv.rfsi.ssh.es.login_password pwd123 --prv.rfsi.ssh.ssh_port 22 --es_clustername cluster3gpp --es_publishHost localhost --es_networkPort 9093 --es_indexName idxdoc3gpp --es_documentType doc3gpp --es_filename_analyser es_analysis.json --es_filename_mapping es_mapping.json
