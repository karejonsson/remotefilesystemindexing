package se.prv.rfsi.idx.reuse;

import se.prv.rfsi.internal.InstallationProperties;

public class PropertyNames {

	public static final String jdcb_driver_class_property = InstallationProperties.jdbc_driver_class_property;
	public static final String jdcb_url_property = InstallationProperties.jdbc_url_property;
	public static final String jdcb_username_property = InstallationProperties.jdbc_username_property;
	public static final String jdcb_password_property = InstallationProperties.jdbc_password_property;

	public final static String jdcb_tunnel_lport = "prv.rfsi.ssh.jdbc.tunnel_lport";
	public final static String jdcb_tunnel_rhost = "prv.rfsi.ssh.jdbc.tunnel_rhost";
	public final static String jdcb_tunnel_rport = "prv.rfsi.ssh.jdbc.tunnel_rport";
    
	public final static String jdbc_open = "prv.rfsi.ssh.jdbc.open";
	public final static String jdcb_login_user = "prv.rfsi.ssh.jdbc.login_user";
	public final static String jdcb_login_domain = "prv.rfsi.ssh.jdbc.login_domain";
	public final static String jdcb_login_password = "prv.rfsi.ssh.jdbc.login_password";

	public final static String es1_open = "prv.rfsi.ssh.es1.open";
	public final static String es1_tunnel_lport = "prv.rfsi.ssh.es1.tunnel_lport";
	public final static String es1_tunnel_rhost = "prv.rfsi.ssh.es1.tunnel_rhost";
	public final static String es1_tunnel_rport = "prv.rfsi.ssh.es1.tunnel_rport";
    
	public final static String es2_open = "prv.rfsi.ssh.es2.open";
	public final static String es2_tunnel_lport = "prv.rfsi.ssh.es2.tunnel_lport";
	public final static String es2_tunnel_rhost = "prv.rfsi.ssh.es2.tunnel_rhost";
	public final static String es2_tunnel_rport = "prv.rfsi.ssh.es2.tunnel_rport";
    
	public final static String es_login_user = "prv.rfsi.ssh.es.login_user";
	public final static String es_login_domain = "prv.rfsi.ssh.es.login_domain";
	public final static String es_login_password = "prv.rfsi.ssh.es.login_password";

	public final static String ssh_port = "prv.rfsi.ssh.ssh_port";
	public final static String es_clustername = "es_clustername";
	public final static String es_publishHost = "es_publishHost";
	public final static String es_networkPort = "es_networkPort";
	
	public final static String es_indexName = "es_indexName";
	public final static String es_documentType = "es_documentType";
	public final static String es_filename_analyser = "es_filename_analyser";
	public final static String es_filename_mapping = "es_filename_mapping";

	public final static String progressbar = "progressbar";
	public final static String webprogress = "webprogress";
	public final static String webtitle = "webtitle";
	public final static String firstWait = "firstWait";
	public final static String reattempts = "reattempts";

}
