package se.prv.rfsi.idx.create;

import java.nio.charset.StandardCharsets;

import org.elasticsearch.client.Client;

import general.reuse.timeouthandling.task.TimeoutTask;
import se.prv.rfsi.domain.Document;
import se.prv.rfsi.idx.reuse.ElasticSearchOperations;
import se.prv.rfsi.idx.reuse.IndexationEntry;

public class IndexationTask implements TimeoutTask {
	
	private boolean isDone = false;
	private IndexationResourceManagement resources;
	private Document doc;
	private String indexName;
	private String documentType;
	
	public IndexationTask(IndexationResourceManagement resources, Document doc, String indexName, String documentType) {
		this.resources = resources;
		this.doc = doc;
		this.indexName = indexName;
		this.documentType = documentType;
	}

	@Override
	public void runTimeconsuming(int attemptNo) throws InterruptedException {
		isDone = doTheWorkForOneDoc(resources.getClient(), doc, indexName, documentType);
	}

	@Override
	public void onFinishedBeQuick(int attemptNo) {
		doc.setIndexed(true);
		doc.save();
	}

	@Override
	public void onTimeoutBeQuick(int attemptNo) {
	}

	@Override
	public void onTerminallyFailed() {
	}

	@Override
	public boolean isDone() {
		return isDone;
	}

	public static boolean doTheWorkForOneDoc(Client client, Document doc, String indexName, String documentType) {
		IndexationEntry entry = new IndexationEntry();
		entry.setBodytext(new String(doc.getData(), StandardCharsets.UTF_8));
		entry.setId(""+doc.getId());
		try {
			ElasticSearchOperations.entryIndexation(client, indexName, documentType, entry);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
