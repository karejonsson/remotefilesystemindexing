package se.prv.rfsi.idx.reuse;

public class IndexationEntry {
	private String id;
	private String bodytext;

	public IndexationEntry(String id, String breadtext) {
		this.id = id;
		this.bodytext = breadtext;
	}

	public IndexationEntry() {
	}

	public String toString() {
		return "Id: "+id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBodytext() {
		return bodytext;
	}

	public void setBodytext(String bodytext) {
		this.bodytext = bodytext;
	}

}
