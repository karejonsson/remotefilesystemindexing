package se.prv.rfsi.idx.create;

import java.util.ArrayList;
import java.util.List;

import org.elasticsearch.client.Client;

import general.reuse.properties.CommandLineProperties;
import general.reuse.properties.PropertiesAssemble;
import general.reuse.timeouthandling.monitortask.ProgressWebReporter;
import general.reuse.timeouthandling.monitortask.ProgressbarForJobs;
import general.reuse.timeouthandling.task.JobOfManyJobs;
import general.reuse.timeouthandling.task.TaskMonitorer;
import se.prv.rfsi.dbc.DocumentDB;
import se.prv.rfsi.domain.Document;
import se.prv.rfsi.idx.reuse.PropertyNames;

public class CreateIndexFromPremadeTextsInDatabase {

	public static void main(String args[]) {
		CommandLineProperties clp = null;
		try {
			clp = PropertiesAssemble.collectCLP(args);
			clp.printEvaluated();
		}
		catch(Exception e) {
			System.out.println("Fel vid inläsningen av kommandoradsparametrarna.");
			e.printStackTrace();
			System.exit(1);						
		}
		try {
			main(clp);
		}
		catch(Exception e) {
			System.out.println("Felaktig terminering.");
			e.printStackTrace();
			System.exit(1);			
		}
		System.exit(0);			
	}

	public static void main(CommandLineProperties clp) throws Exception {
		IndexationResourceManagement resources = new IndexationResourceManagement(clp);
		resources.start();
		
		doTheWorkPhase3(clp, resources.getClient(), resources);
		
	}
	
	public static final int limit = 10;
	
	public static void doTheWorkPhase3(CommandLineProperties clp, Client client, IndexationResourceManagement resources) throws Exception {
		String indexName = clp.getString(PropertyNames.es_indexName);
		String documentType = clp.getString(PropertyNames.es_documentType);

		List<Document> allDocs = DocumentDB.getAllDocuments();
		System.out.println("Hittade "+allDocs.size()+" dokument.");
		List<TaskMonitorer> jobs = new ArrayList<TaskMonitorer>();
		
		for(Document doc : allDocs) {
			if(!doc.isIndexed()) {
				jobs.add(new TaskMonitorer(new IndexationTask(resources, doc, indexName, documentType)));
			}
		}
		System.out.println("Hittade "+jobs.size()+" dokument att indexera.");
		
		long first = clp.getLong(PropertyNames.firstWait);
		Long[] reattempts = clp.getLongArray(PropertyNames.reattempts);	
		
		JobOfManyJobs worker = new JobOfManyJobs(jobs, first, reattempts, resources); 
		long before = System.currentTimeMillis();
		if(clp.getBoolean(PropertyNames.progressbar, false)) {
			ProgressbarForJobs.run("Hantera indexfilerna", worker, false);
		}
		else {
			Integer port = clp.getInt_Immutable(PropertyNames.webprogress, null);
			if(port != null) {
				ProgressWebReporter.run(worker, port, clp.getString(PropertyNames.webtitle, "Indexing"));
			}
			else {
				worker.run();
			}
		}
		long after = System.currentTimeMillis();
		System.out.println("Klar med "+jobs.size()+" indexfiler på "+(after-before)+" millisekunder");
		
		resources.stop();
	}
		
	/*
		long before = System.currentTimeMillis();
		int ctr = 0;
		int errorctr = 0;
		for(Document doc : docsToIndex) {
			ctr++;
			if(ctr % 100 == 0) {
				System.out.println("Indexering på varv "+ctr+" av "+docsToIndex.size()+". Tid "+(new Date())+", återstår "+(docsToIndex.size()-ctr));
			}
			boolean outcome = doTheWorkForOneDoc(client, doc, indexName, documentType);
			if(!outcome) {
				errorctr++;
			}
			if(errorctr > limit) {
				System.out.println("Avslutade vid "+limit+" felfall.");
				break;
			}
		}
		long after = System.currentTimeMillis();
		System.out.println("Klar efter "+(after - before)+" millisekunder.");
	}
	*/
	/*
	public static boolean doTheWorkForOneDoc(Client client, Document doc, String indexName, String documentType) {
		IndexationEntry entry = new IndexationEntry();
		entry.setBodytext(new String(doc.getData(), StandardCharsets.UTF_8));
		entry.setId(""+doc.getId());
		try {
			ElasticSearchOperations.entryIndexation(client, indexName, documentType, entry);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		doc.setIndexed(true);
		doc.save();
		return true;
	}
	*/

}
