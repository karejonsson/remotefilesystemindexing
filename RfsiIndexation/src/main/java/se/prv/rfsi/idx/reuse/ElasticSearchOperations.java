package se.prv.rfsi.idx.reuse;

import java.io.InputStreamReader;
import java.net.InetAddress;
import java.util.Map;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.IndicesAdminClient;
import org.elasticsearch.client.Requests;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.json.simple.parser.JSONParser;

public class ElasticSearchOperations {

    private final static String clusternameInternalPropertyName = "cluster.name";

    public static Client getClient(String clustername, String publishHost, int networkPort) throws Exception {
    	Settings settings = Settings.builder()
        .put(clusternameInternalPropertyName, clustername)
        .build();

    	TransportClient client = new PreBuiltTransportClient(settings)
    	        .addTransportAddress(new TransportAddress(InetAddress.getByName(publishHost), networkPort));
        return client;
    }

    public static void setupIndexByMapping(Client client, String indexName, String documentType, String filename_analyser, String filename_mapping) throws Exception {
    	IndicesAdminClient indices = client.admin().indices();
    	JSONParser parser = new JSONParser();
        Object mappingSettings = parser.parse(new InputStreamReader(ElasticSearchOperations.class.getClassLoader().getResourceAsStream(filename_mapping)));
        parser = new JSONParser();
        Object analyserSettings = parser.parse(new InputStreamReader(ElasticSearchOperations.class.getClassLoader().getResourceAsStream(filename_analyser)));
        indices.prepareCreate(indexName).setSettings((Map) analyserSettings).addMapping(documentType, (Map) mappingSettings).execute().actionGet();
    }

    public static final String bodytextFieldname = "bodytext";
    public static final String bodytextSynonymFieldname = "bodytextsynonym";
    public static final String idFieldname = "id";

	public static void entryIndexation(Client client, String indexName, String documentType, IndexationEntry entry) throws Exception {
    	//System.out.println(entry.toString());
    	XContentBuilder xcb = XContentFactory.jsonBuilder().startObject()
				.field(bodytextFieldname, entry.getBodytext())
				.field(bodytextSynonymFieldname, entry.getBodytext())
				.field(idFieldname, entry.getId())
				.endObject();
    	client.index(Requests.indexRequest(indexName).type(documentType).id(entry.getId()).source(xcb)).actionGet();
    }

}
