package se.prv.rfsi.idx.create;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.NoNodeAvailableException;

import general.reuse.database.pool.SimpleJDBCConnectionPool;
import general.reuse.properties.CommandLineProperties;
import general.reuse.timeouthandling.task.ResourceManagement;
import general.reuse.tunneling.TunnelForwardLEncapsulation;
import general.reuse.tunneling.TunnelForwardLParameters;
import se.prv.rfsi.dbcreuse.Commons;
import se.prv.rfsi.idx.reuse.ElasticSearchOperations;
import se.prv.rfsi.idx.reuse.PropertyNames;

public class IndexationResourceManagement implements ResourceManagement {

	private CommandLineProperties clp;
	private TunnelForwardLEncapsulation jdbc_tfle = null;
	private TunnelForwardLEncapsulation es2_tfle = null;
	private TunnelForwardLEncapsulation es1_tfle = null;
	private Client client = null;

	public IndexationResourceManagement(CommandLineProperties clp) {
		this.clp = clp;
	}
	
	public Client getClient() {
		return client;
	}

	@Override
	public boolean isActive() {
		if(clp.getBoolean(PropertyNames.jdbc_open, false)) {
			if(jdbc_tfle == null) {
				return false;
			}
		}
		if(clp.getBoolean(PropertyNames.es1_open, false)) {
			if(es1_tfle == null) {
				return false;
			}
		}
		if(clp.getBoolean(PropertyNames.es2_open, false)) {
			if(es2_tfle == null) {
				return false;
			}
		}
		if(client == null) {
			return false;
		}
		return true;
	}

	@Override
	public void stop() {
		try {
			Commons.destroy();
		}
		catch(NullPointerException npe) {
			System.out.println("JDBC-kopplingen var aldrig använd");
		}
		System.out.println("JDBC-uppkopplingen är stängd.");
		if(jdbc_tfle != null) {
			jdbc_tfle.closeTunnel();
			jdbc_tfle = null;
			System.out.println("SSH-tunneln för JDBC stängd.");
		}
		
		client.close();
		client = null;
		System.out.println("ES-client stängd.");
		if(es1_tfle != null) {
			es1_tfle.closeTunnel();
			es1_tfle = null;
			System.out.println("SSH-tunneln för ES1 stängd.");
		}
		if(es2_tfle != null) {
			es2_tfle.closeTunnel();
			es2_tfle = null;
			System.out.println("SSH-tunneln för ES2 stängd.");
		}
	}

	@Override
	public void start() {
		if(clp.getBoolean(PropertyNames.jdbc_open, false)) {

			TunnelForwardLParameters jdbc_tflp = new TunnelForwardLParameters();
			try {
				jdbc_tflp.setLogin_domain(clp.getString(PropertyNames.jdcb_login_domain));
				jdbc_tflp.setLogin_password(clp.getString(PropertyNames.jdcb_login_password));
				jdbc_tflp.setLogin_user(clp.getString(PropertyNames.jdcb_login_user));
				jdbc_tflp.setTunnel_lport(clp.getInt(PropertyNames.jdcb_tunnel_lport));
				jdbc_tflp.setTunnel_rport(clp.getInt(PropertyNames.jdcb_tunnel_rport));
				jdbc_tflp.setTunnel_rhost(clp.getString(PropertyNames.jdcb_tunnel_rhost));
	
				jdbc_tflp.setSsh_port(clp.getInt(PropertyNames.ssh_port, 22));
			}
			catch(Exception e) {
				System.out.println("Tillhandahållna parametrar räcker ej för tunneln för JDBC.\n"+jdbc_tflp);
				e.printStackTrace();
				return;
			}
			
			if(!jdbc_tflp.hasAll()) {
				System.out.println("Erforderliga parametrar saknas för tunneln för JDBC.\n"+jdbc_tflp);
				return;
			}
			
			try {
				jdbc_tfle = new TunnelForwardLEncapsulation(jdbc_tflp);
			} catch (Exception e1) {
				System.out.println("Parametrar saknas för att öppna tunneln för JDBC.\n"+jdbc_tflp);
				e1.printStackTrace();
				return;
			}
			
			if(!jdbc_tfle.openTunnel()) {
				System.out.println("Det gick inte att öppna SSH-tunneln för JDBC.\n"+jdbc_tflp);
				return;
			}
	
			System.out.println("SSH-tunneln för JDBC öppnad.");
		}
		
		try {
			Commons.setPool(new SimpleJDBCConnectionPool(
					clp.getString(PropertyNames.jdcb_driver_class_property),
					clp.getString(PropertyNames.jdcb_url_property),
					clp.getString(PropertyNames.jdcb_username_property),
					clp.getString(PropertyNames.jdcb_password_property)));
		}
		catch(Exception e) {
			System.out.println("Det gick inte att öppna mot databasen. JDBC-tunneln verkar OK.");
			e.printStackTrace();
			return;
		}
		
		if(clp.getBoolean(PropertyNames.jdbc_open, false)) {
			System.out.println("JDBC är öppnat genom SSH-tunneln.");			
		}
		else {
			System.out.println("JDBC är öppnat.");			
		}
		
		if(clp.getBoolean(PropertyNames.es1_open, false)) {
			TunnelForwardLParameters es1_tflp = new TunnelForwardLParameters();
			try {
				es1_tflp.setLogin_domain(clp.getString(PropertyNames.es_login_domain));
				es1_tflp.setLogin_password(clp.getString(PropertyNames.es_login_password));
				es1_tflp.setLogin_user(clp.getString(PropertyNames.es_login_user));
				es1_tflp.setTunnel_lport(clp.getInt(PropertyNames.es1_tunnel_lport));
				es1_tflp.setTunnel_rport(clp.getInt(PropertyNames.es1_tunnel_rport));
				es1_tflp.setTunnel_rhost(clp.getString(PropertyNames.es1_tunnel_rhost));
	
				es1_tflp.setSsh_port(clp.getInt(PropertyNames.ssh_port, 22));
			}
			catch(Exception e) {
				System.out.println("Tillhandahållna parametrar räcker ej för tunneln för ES1.\n"+es1_tflp);
				e.printStackTrace();
				return;
			}
			
			if(!es1_tflp.hasAll()) {
				System.out.println("Erforderliga parametrar saknas för tunneln för ES1.\n"+es1_tflp);
				return;
			}
			
			try {
				es1_tfle = new TunnelForwardLEncapsulation(es1_tflp);
			} catch (Exception e1) {
				System.out.println("Parametrar saknas för att öppna tunneln för ES1.\n"+es1_tflp);
				e1.printStackTrace();
				return;
			}
			
			if(!es1_tfle.openTunnel()) {
				System.out.println("Det gick inte att öppna SSH-tunneln för ES1.\n"+es1_tflp);
				return;
			}
	
			System.out.println("SSH-tunneln för ES1 öppnad.");
		}

		if(clp.getBoolean(PropertyNames.es2_open, false)) {
			TunnelForwardLParameters es2_tflp = new TunnelForwardLParameters();
			try {
				es2_tflp.setLogin_domain(clp.getString(PropertyNames.es_login_domain));
				es2_tflp.setLogin_password(clp.getString(PropertyNames.es_login_password));
				es2_tflp.setLogin_user(clp.getString(PropertyNames.es_login_user));
				es2_tflp.setTunnel_lport(clp.getInt(PropertyNames.es2_tunnel_lport));
				es2_tflp.setTunnel_rport(clp.getInt(PropertyNames.es2_tunnel_rport));
				es2_tflp.setTunnel_rhost(clp.getString(PropertyNames.es2_tunnel_rhost));
	
				es2_tflp.setSsh_port(clp.getInt(PropertyNames.ssh_port, 22));
			}
			catch(Exception e) {
				System.out.println("Tillhandahållna parametrar räcker ej för tunneln för ES2.\n"+es2_tflp);
				e.printStackTrace();
				return;
			}
			
			if(!es2_tflp.hasAll()) {
				System.out.println("Erforderliga parametrar saknas för tunneln för ES2.\n"+es2_tflp);
				return;
			}
			
			try {
				es2_tfle = new TunnelForwardLEncapsulation(es2_tflp);
			} catch (Exception e1) {
				System.out.println("Parametrar saknas för att öppna tunneln för ES2.\n"+es2_tflp);
				e1.printStackTrace();
				return;
			}
			
			if(!es2_tfle.openTunnel()) {
				System.out.println("Det gick inte att öppna SSH-tunneln för ES2.\n"+es2_tflp);
				return;
			}
	
			System.out.println("SSH-tunneln för ES2 öppnad.");
		}
		
		try {
			String clustername = clp.getString(PropertyNames.es_clustername);
			String publishHost = clp.getString(PropertyNames.es_publishHost);
			int networkPort = clp.getInt(PropertyNames.es_networkPort);
			
		    client = ElasticSearchOperations.getClient(clustername, publishHost, networkPort);
	
			System.out.println("ES-client är skapad.");
		}
		catch(Exception e) {
			e.printStackTrace();
			return;
		}
		
		try {			
			String indexName = clp.getString(PropertyNames.es_indexName);
			String documentType = clp.getString(PropertyNames.es_documentType);
			String filename_analyser = clp.getString(PropertyNames.es_filename_analyser);
			String filename_mapping = clp.getString(PropertyNames.es_filename_mapping);
			ElasticSearchOperations.setupIndexByMapping(client, indexName, documentType, filename_analyser, filename_mapping);
			System.out.println("ES-client har gjort mappning.");
		}
		catch(NoNodeAvailableException nnae) {
			System.out.println("ES-client hittar ingen tillgänglig nod. Avbryter.");
			nnae.printStackTrace();
			return;
		}
		catch(Exception e) {
			System.out.println("ES-client hade gjort mappningen tidigare. OK.");
			e.printStackTrace();
		}


	}

	@Override
	public boolean isProbablyOK() {
		return isActive();
	}

}
