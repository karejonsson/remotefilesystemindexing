echo "Full process ------ 1 $(date)"

cd /home/kjonsson/skelett
rm -rf *
wget --no-parent --recursive --tries 4 -A index.html -T 10 -m http://www.3gpp.org/ftp/Information/
echo "Full process ------ 1.1 $(date)"
wget --no-parent --recursive --tries 4 -A index.html -T 10 -m http://www.3gpp.org/ftp/Joint_Meetings/
echo "Full process ------ 1.2 $(date)"
wget --no-parent --recursive --tries 4 -A index.html -T 10 -m http://www.3gpp.org/ftp/Specs/latest/
echo "Full process ------ 1.3 $(date)"
wget --no-parent --recursive --tries 4 -A index.html -T 10 -m http://www.3gpp.org/ftp/tsg_cn/
echo "Full process ------ 1.4 $(date)"
wget --no-parent --recursive --tries 4 -A index.html -T 10 -m http://www.3gpp.org/ftp/tsg_ct/
echo "Full process ------ 1.5 $(date)"
wget --no-parent --recursive --tries 4 -A index.html -T 10 -m http://www.3gpp.org/ftp/tsg_geran/
echo "Full process ------ 1.6 $(date)"
wget --no-parent --recursive --tries 4 -A index.html -T 10 -m http://www.3gpp.org/ftp/tsg_ran/
echo "Full process ------ 1.7 $(date)"
wget --no-parent --recursive --tries 4 -A index.html -T 10 -m http://www.3gpp.org/ftp/tsg_sa/

echo "Full process ------ 2 $(date)"

cd /home/kjonsson/applications/RfsiSyncronize/app
./start.sh se.prv.rfsi.sync.skeleton.AbsorbSkeleton --folder /home/kjonsson/skelett/www.3gpp.org/ftp --delimiter /ftp/ --prv.rfsi.jdbc.driverclass org.postgresql.Driver --prv.rfsi.jdbc.domain localhost --prv.rfsi.jdbc.port 5432 --prv.rfsi.jdbc.sid indexdb --prv.rfsi.jdbc.url jdbc:postgresql://\${prv.rfsi.jdbc.domain}:\${prv.rfsi.jdbc.port}/\${prv.rfsi.jdbc.sid} --prv.rfsi.jdbc.username prvuser --prv.rfsi.jdbc.password prvpwd --prv.rfsi.ssh.ssh_port 22 --prv.rfsi.ssh.jdbc.open false --webprogress 8096 --firstWait 20000 --reattempts 20000 600000 1200000

echo "Full process ------ 3 $(date)"

./start.sh se.prv.rfsi.sync.total.ImportEverythingBatch --dateFormatActualized 'yyyy-MM-dd HH:mm:ss' --dateActualized '2018-02-15 08:24:00' --maxHandledSize 1000000000 --hashAlgo SHA-256 --delimiter /ftp/ --servedUrl http://www.3gpp.org\${delimiter} --prv.rfsi.jdbc.driverclass org.postgresql.Driver --prv.rfsi.jdbc.domain localhost --prv.rfsi.jdbc.port 5432 --prv.rfsi.jdbc.sid indexdb --prv.rfsi.jdbc.url jdbc:postgresql://\${prv.rfsi.jdbc.domain}:\${prv.rfsi.jdbc.port}/\${prv.rfsi.jdbc.sid} --prv.rfsi.jdbc.username prvuser --prv.rfsi.jdbc.password prvpwd --prv.rfsi.ssh.jdbc.open false --prv.rfsi.ssh.ssh_port 22 --webprogress 8096 --firstWait 30000 --reattempts 30000 600000 600000

echo "Full process ------ 4 $(date)"

cd /home/kjonsson/applications/RfsiIndexation/app
./start.sh se.prv.rfsi.idx.create.CreateIndexFromPremadeTextsInDatabase --prv.rfsi.jdbc.driverclass org.postgresql.Driver --prv.rfsi.jdbc.domain localhost --prv.rfsi.jdbc.port 5432 --prv.rfsi.jdbc.sid indexdb --prv.rfsi.jdbc.url jdbc:postgresql://\${prv.rfsi.jdbc.domain}:\${prv.rfsi.jdbc.port}/\${prv.rfsi.jdbc.sid} --prv.rfsi.jdbc.username prvuser --prv.rfsi.jdbc.password prvpwd --prv.rfsi.ssh.es1.tunnel_lport 9300 --prv.rfsi.ssh.ssh_port 22 --es_clustername cluster3gpp --es_publishHost localhost --es_networkPort 9300 --es_indexName idxdoc3gpp --es_documentType doc3gpp --es_filename_analyser es_analysis.json --es_filename_mapping es_mapping.json --webprogress 8086 --firstWait 20000 --reattempts 20000 20000 60000 60000 360000

echo "Full process ------ 5 $(date)"
