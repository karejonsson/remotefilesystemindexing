package se.prv.rfsi.internal;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class Services {

	public static long passStream(InputStream is) throws IOException {
		if (is == null) {
			return 0l;
		}
		byte[] data = new byte[16384];
		int nRead;
		long nReadTot = 0;
		while ((nRead = is.read(data, 0, data.length)) != -1) {
			nReadTot += nRead;
		}
		return nReadTot;
	}
	
	public static byte[] getByteArrayFromStream(InputStream is) throws IOException {
		if (is == null) {
			return new byte[0];
		}
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();

		int nRead;
		byte[] data = new byte[16384];

		while ((nRead = is.read(data, 0, data.length)) != -1) {
			buffer.write(data, 0, nRead);
		}

		buffer.flush();

		return buffer.toByteArray();
	}
	
	/*
	private byte[] readBytes(Entry entry) throws IOException {
	    byte[] header = getHeader(entry);
	    int csize = entry.compressedSize;
	    byte[] cbuf = new byte[csize];
	    zipRandomFile.skipBytes(get2ByteLittleEndian(header, 26) + get2ByteLittleEndian(header, 28));
	    zipRandomFile.readFully(cbuf, 0, csize);

	    // is this compressed - offset 8 in the ZipEntry header
	    if (get2ByteLittleEndian(header, 8) == 0)
	        return cbuf;

	    int size = entry.size;
	    byte[] buf = new byte[size];
	    if (inflate(cbuf, buf) != size)
	        throw new ZipException("corrupted zip file");

	    return buf;
	}
	
    private byte[] getHeader(Entry entry) throws IOException {
        zipRandomFile.seek(entry.offset);
        byte[] header = new byte[30];
        zipRandomFile.readFully(header);
        if (get4ByteLittleEndian(header, 0) != 0x04034b50)
            throw new ZipException("corrupted zip file");
        if ((get2ByteLittleEndian(header, 6) & 1) != 0)
            throw new ZipException("encrypted zip file"); // offset 6 in the header of the ZipFileEntry
        return header;
    }

	 
    private static int get2ByteLittleEndian(byte[] buf, int pos) {
        return (buf[pos] & 0xFF) + ((buf[pos+1] & 0xFF) << 8);
    }

    private static int get4ByteLittleEndian(byte[] buf, int pos) {
        return (buf[pos] & 0xFF) + ((buf[pos + 1] & 0xFF) << 8) +
                ((buf[pos + 2] & 0xFF) << 16) + ((buf[pos + 3] & 0xFF) << 24);
    }
	*/
	
	public static void putToFile(File f, byte bytesToPut[]) throws IOException {
		FileOutputStream fos = new FileOutputStream(f);
		fos.write(bytesToPut);
		fos.close();
	}

}
