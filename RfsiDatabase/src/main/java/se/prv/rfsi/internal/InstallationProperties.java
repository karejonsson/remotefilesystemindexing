package se.prv.rfsi.internal;

import general.reuse.properties.HarddriveProperties;

public class InstallationProperties {
	
	private static final String pathToConfigFile_property = "prv.rfsi.configfile";
	private static final String pathToConfigFile_default = "/etc/prvconfig/prvrfsi.properties";

	private static HarddriveProperties hp = new HarddriveProperties(pathToConfigFile_property, pathToConfigFile_default);
	 
	public static String getString(String propertyname) throws Exception {
		return hp.getString(propertyname);
	}

	public static String getString(String propertyname, final String defaultValue) {
		return hp.getString(propertyname, defaultValue);
	}
	
	public static Integer getInt(String propertyname) throws Exception {
		return hp.getCLPProperty(propertyname);
	}
	
	public static Integer getInt(String propertyname, final Integer defaultValue) {
		return hp.getCLPProperty(propertyname, defaultValue);
	}
	
	public static Boolean getBoolean(String propertyname, final Boolean defaultValue) {
		return hp.getBoolean(propertyname, defaultValue); 
	}
	
	public static void print() {
		try {
			hp.printProperties();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static final String jdbc_driver_class_property = "prv.rfsi.jdbc.driverclass";
	public static final String jdbc_url_property = "prv.rfsi.jdbc.url";
	public static final String jdbc_username_property = "prv.rfsi.jdbc.username";
	public static final String jdbc_password_property = "prv.rfsi.jdbc.password";
	
	public static void main(String args[]) throws Exception {
		System.out.println("Driver klass "+getString(jdbc_driver_class_property, "inte rätt"));
	}

}
