package se.prv.rfsi.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.rfsi.dbcreuse.Commons;
import se.prv.rfsi.domain.Indexfile;

public class IndexfileDB {

	/*
create table indexfile (
  id bigserial primary key,
  folder varchar(200) not null,
  postfixes varchar(250) not null,
  process bool default null,
  actualized timestamp not null
);
	 */

	public static List<Indexfile> getAllIndexfiles() {
		List<Indexfile> out = new ArrayList<Indexfile>();
		 
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select id, folder, postfixes, process, actualized from indexfile");
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	Indexfile ixf = new Indexfile();
		    	ixf.setId(rs.getLong(1));
		    	ixf.setFolder(rs.getString(2));
		    	ixf.setPostfixes(rs.getString(3));
		    	Object b = rs.getObject(4);
		    	if(b != null) {
			    	ixf.setProcess((Boolean) b);
		    	}
		    	ixf.setActualized(rs.getTimestamp(5));
		    	out.add(ixf);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}
	
	public static boolean deleteIndexfile(Indexfile pf) {
	 	boolean outcome = deleteIndexfile(pf.getId());
	 	return outcome;
	}

	public static boolean deleteIndexfile(long indexfileId) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("delete from indexfile where id = ?");
		    stmt.setLong(1, indexfileId);
		    stmt.executeUpdate();
		    conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

	public static boolean updateIndexfile(Indexfile ixf) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection(); // id, folder, postfixes, process, indexed, actualized from indexfile
		    stmt = conn.prepareStatement("update indexfile set folder = ?, postfixes = ?, process = ?, actualized = ? where id = ?");
		    stmt.setString(1, ixf.getFolder());
		    stmt.setString(2, ixf.getPostfixes());
		    Boolean b = ixf.getProcess();
		    if(b != null) {
			    stmt.setBoolean(3, b);
		    }
		    else {
		    	stmt.setNull(3, Types.BOOLEAN);
		    }
            Date actualized = ixf.getActualized();
            if(actualized == null) {
                actualized = new Date();
                ixf.setActualized(actualized);
            }
            stmt.setTimestamp(4, new java.sql.Timestamp(actualized.getTime()));
			stmt.setLong(5, ixf.getId());
		    stmt.executeUpdate();
		    conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

	public static boolean createIndexfile(Indexfile pf) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    boolean outcome = createIndexfile(pf, conn);
		    if(outcome) {
			    conn.commit();
		    }
		    else {
			    conn.rollback();
		    }
		    return outcome;
		} catch(Exception e) {
	    	//System.out.println("Fel 2");
			e.printStackTrace();
		    return false;
		} finally {
		    if(conn != null) { pool.releaseConnection(conn); }
		}
	}
	
	public static boolean createIndexfile(Indexfile ixf, Connection conn) {
		PreparedStatement stmt = null;
		try { // id, folder, postfixes, process, indexed, actualized from indexfile
		    stmt = conn.prepareStatement("insert into indexfile ( folder, postfixes, process, actualized ) values ( ?, ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);
		    stmt.setString(1, ixf.getFolder());
		    stmt.setString(2, ixf.getPostfixes());
		    Boolean b = ixf.getProcess();
		    if(b != null) {
			    stmt.setBoolean(3, b);
		    }
		    else {
		    	stmt.setNull(3, Types.BOOLEAN);
		    }

            Date actualized = ixf.getActualized();
            if(actualized == null) {
                actualized = new Date();
                ixf.setActualized(actualized);
            }
            stmt.setTimestamp(4, new java.sql.Timestamp(actualized.getTime()));

		    int affectedRows = stmt.executeUpdate();
		    try {
		    	ResultSet generatedKeys = stmt.getGeneratedKeys();
	            generatedKeys.next();
	            ixf.setId(generatedKeys.getLong(1));
	        }
		    catch(Exception e) {
		    	//System.out.println("Fel 1");
		    	e.printStackTrace();
		    }
		} catch(Exception e) {
	    	//System.out.println("Fel 2");
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		}
		return true;
	}
	
	public static Indexfile getIndexfileFromId(long id) {
		Indexfile ixf = null;
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection(); // id, folder, postfixes, process, indexed, actualized from indexfile
		    stmt = conn.prepareStatement("select folder, postfixes, process, actualized from indexfile where id = ?");
		    stmt.setLong(1, id);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	ixf = new Indexfile();
				ixf.setId(id);
				ixf.setFolder(rs.getString(1));
				ixf.setPostfixes(rs.getString(2));
		    	Object b = rs.getObject(3);
		    	if(b != null) {
			    	ixf.setProcess((Boolean) b);
		    	}
				ixf.setActualized(rs.getTimestamp(4));
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return ixf;
	}
	
	public static Indexfile getIndexfileFromFolder(String folder) {
		Indexfile ixf = null;
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection(); // id, folder, postfixes, process, indexed, actualized from indexfile
		    stmt = conn.prepareStatement("select id, postfixes, process, actualized from indexfile where folder = ?");
		    stmt.setString(1, folder);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	ixf = new Indexfile();
				ixf.setId(rs.getLong(1));
				ixf.setPostfixes(rs.getString(2));
		    	Object b = rs.getObject(3);
		    	if(b instanceof Boolean) {
			    	ixf.setProcess((Boolean) b);
		    	}
				ixf.setActualized(rs.getTimestamp(4));
				ixf.setFolder(folder);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return ixf;
	}
	
}

