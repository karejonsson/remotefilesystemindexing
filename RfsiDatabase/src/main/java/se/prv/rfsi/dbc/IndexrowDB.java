package se.prv.rfsi.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.rfsi.dbcreuse.Commons;
import se.prv.rfsi.domain.Indexrow;

public class IndexrowDB {

	/*
create table indexrow (
  id bigserial primary key,
  indexfileId bigint not null,
  name varchar(200) not null,
  href varchar(400) not null,
  skeletonsize bigint not null,
  importsize bigint default null,
  skeletondate timestamp not null,
  actualized timestamp not null,
  failcode integer default null,
  failmsg varchar(300) default null,
  foreign key ( indexfileId ) references indexfile (id)
);
	 */

	public static List<Indexrow> getAllIndexrows() {
		List<Indexrow> out = new ArrayList<Indexrow>();
		 
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select id, indexfileId, name, href, skeletonsize, importsize, skeletondate, actualized, failcode, failmsg from indexrow");
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	Indexrow ixr = new Indexrow();
		    	ixr.setId(rs.getLong(1));
		    	ixr.setIndexfileId(rs.getLong(2));
		    	ixr.setName(rs.getString(3));
		    	ixr.setHref(rs.getString(4));
		    	ixr.setSkeletonsize(rs.getLong(5));
		    	Object is = rs.getObject(6);
		    	if(is != null) {
		    		if(is instanceof Long) {
		    			ixr.setImportsize((Long) is);
		    		}
		    		if(is instanceof Integer) {
		    			ixr.setImportsize(new Long((Integer) is));
		    		}
		    	}
		    	ixr.setActualized(rs.getTimestamp(7));
		    	ixr.setSkeletondate(rs.getTimestamp(8));
		    	Object fc = rs.getObject(9);
		    	if(fc != null) {
		    		if(fc instanceof Long) {
		    			ixr.setFailcode(((Long) fc).intValue());
		    		}
		    		if(fc instanceof Integer) {
		    			ixr.setFailcode((Integer) fc);
		    		}
		    	}
		    	ixr.setFailmsg(rs.getString(10));
		    	out.add(ixr);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}
	
	public static List<Indexrow> getAllIndexrowsOfIndexfile(long indexfileId) {
		List<Indexrow> out = new ArrayList<Indexrow>();
		 
		JDBCConnectionPool pool = null;
		
		Connection conn = null;
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection(); // id, indexfileId, name, href, size, actualized from indexrow
		    stmt = conn.prepareStatement("select id, name, href, skeletonsize, importsize, skeletondate, actualized, failcode, failmsg from indexrow where indexfileId = ?");
		    stmt.setLong(1, indexfileId);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	Indexrow ixr = new Indexrow();
		    	ixr.setId(rs.getLong(1));
		    	ixr.setName(rs.getString(2));
		    	ixr.setHref(rs.getString(3));
		    	ixr.setSkeletonsize(rs.getLong(4));
		    	Object is = rs.getObject(5);
		    	if(is != null) {
		    		if(is instanceof Long) {
		    			ixr.setImportsize((Long) is);
		    		}
		    		if(is instanceof Integer) {
		    			ixr.setImportsize(new Long((Integer) is));
		    		}
		    	}
		    	ixr.setSkeletondate(rs.getTimestamp(6));
		    	ixr.setActualized(rs.getTimestamp(7));
		    	Object fc = rs.getObject(8);
		    	if(fc != null) {
		    		if(fc instanceof Long) {
		    			ixr.setFailcode(((Long) fc).intValue());
		    		}
		    		if(fc instanceof Integer) {
		    			ixr.setFailcode((Integer) fc);
		    		}
		    	}	    	
		    	ixr.setFailmsg(rs.getString(9));
		    	ixr.setIndexfileId(indexfileId);
		    	out.add(ixr);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}
	
	public static Map<String, Indexrow> getAllIndexrowsOfindexfileMapped(long indexfileId) {
		List<Indexrow> list = getAllIndexrowsOfIndexfile(indexfileId);
		Map<String, Indexrow> out = new HashMap<String, Indexrow>();
		for(Indexrow ir : list) {
			out.put(ir.getName(), ir);
		}
		return out;
	}

	public static boolean deleteIndexrow(Indexrow ixr) {
	 	boolean outcome = deleteIndexrow(ixr.getId());
	 	return outcome;
	}

	public static boolean deleteIndexrow(long IndexrowId) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("delete from indexrow where id = ?");
		    stmt.setLong(1, IndexrowId);
		    stmt.executeUpdate();
		    conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

	public static boolean updateIndexrow(Indexrow ixr) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection(); // id, indexfileId, name, href, skeletonsize, importsize, actualized from indexrow
		    stmt = conn.prepareStatement("update indexrow set indexfileId = ?, name = ?, href = ?, skeletonsize = ?, importsize = ?, skeletondate = ?, actualized = ?, failcode = ?, failmsg = ? where id = ?");
		    stmt.setLong(1, ixr.getIndexfileId());
		    stmt.setString(2, ixr.getName());
		    stmt.setString(3, ixr.getHref());
		    stmt.setLong(4, ixr.getSkeletonsize());
		    Long importsize = ixr.getImportsize();
		    if(importsize != null) {
			    stmt.setLong(5, importsize);
		    }
		    else {
		    	stmt.setNull(5, Types.INTEGER);
		    }		    
            Date skeletondate = ixr.getSkeletondate();
            if(skeletondate == null) {
                stmt.setTimestamp(6, null);
            }
            else {
                stmt.setTimestamp(6, new java.sql.Timestamp(skeletondate.getTime()));
            }
            Date actualized = ixr.getActualized();
            if(actualized == null) {
                actualized = new Date();
                ixr.setActualized(actualized);
            }
            stmt.setTimestamp(7, new java.sql.Timestamp(actualized.getTime()));
            Integer fc = ixr.getFailcode();
            if(fc != null) {
            	stmt.setInt(8,  fc);
            }
            else {
            	stmt.setNull(8, Types.INTEGER);
            }
            stmt.setString(9, ixr.getFailmsg());
		    stmt.setLong(10, ixr.getId());
		    stmt.executeUpdate();
		    conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

	public static boolean createIndexrow(Indexrow pf) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    boolean outcome = createIndexrow(pf, conn);
		    if(outcome) {
			    conn.commit();
		    }
		    else {
			    conn.rollback();
		    }
		    return outcome;
		} catch(Exception e) {
	    	//System.out.println("Fel 2");
			e.printStackTrace();
		    return false;
		} finally {
		    if(conn != null) { pool.releaseConnection(conn); }
		}
	}
	
	public static boolean createIndexrow(Indexrow ixr, Connection conn) {
		PreparedStatement stmt = null;
		try { // id, indexfileId, name, href, skeletonsize, importsize, actualized from indexrow
		    stmt = conn.prepareStatement("insert into indexrow ( indexfileId, name, href, skeletonsize, importsize, skeletondate, actualized, failcode, failmsg ) values ( ?, ?, ?, ?, ?, ?, ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);
		    stmt.setLong(1, ixr.getIndexfileId());
		    stmt.setString(2, ixr.getName());
		    stmt.setString(3, ixr.getHref());
		    stmt.setLong(4, ixr.getSkeletonsize());
		    Long importsize = ixr.getImportsize();
		    if(importsize != null) {
			    stmt.setLong(5, importsize);
		    }
		    else {
		    	stmt.setNull(5, Types.INTEGER);
		    }
            Date skeletondate = ixr.getSkeletondate();
            if(skeletondate == null) {
                stmt.setTimestamp(6, null);
            }
            else {
                stmt.setTimestamp(6, new java.sql.Timestamp(skeletondate.getTime()));
            }
            Date actualized = ixr.getActualized();
            if(actualized == null) {
                actualized = new Date();
                ixr.setActualized(actualized);
            }
            stmt.setTimestamp(7, new java.sql.Timestamp(actualized.getTime()));
            Integer fc = ixr.getFailcode();
            if(fc != null) {
            	stmt.setInt(8,  fc);
            }
            else {
            	stmt.setNull(8, Types.INTEGER);
            }
            stmt.setString(9, ixr.getFailmsg());
		    int affectedRows = stmt.executeUpdate();
		    try {
		    	ResultSet generatedKeys = stmt.getGeneratedKeys();
	            generatedKeys.next();
	            ixr.setId(generatedKeys.getLong(1));
	        }
		    catch(Exception e) {
		    	//System.out.println("Fel 1");
		    	e.printStackTrace();
		    }
		} catch(Exception e) {
	    	//System.out.println("Fel 2");
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		}
		return true;
	}
	
	public static Indexrow getIndexrowFromId(long id) {
		Indexrow ixr = null;
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection(); // id, indexfileId, name, href, skeletonsize, importsize, actualized from indexrow
		    stmt = conn.prepareStatement("select indexfileId, name, href, skeletonsize, importsize, skeletondate, actualized, failcode, failmsg from indexrow where id = ?");
		    stmt.setLong(1, id);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	ixr = new Indexrow();
		    	ixr.setId(id);
		    	ixr.setIndexfileId(rs.getLong(1));
		    	ixr.setName(rs.getString(2));
		    	ixr.setHref(rs.getString(3));
		    	ixr.setSkeletonsize(rs.getLong(4));
		    	Object is = rs.getObject(5);
		    	if(is != null) {
		    		if(is instanceof Long) {
		    			ixr.setImportsize((Long) is);
		    		}
		    		if(is instanceof Integer) {
		    			ixr.setImportsize(new Long((Integer) is));
		    		}
		    	}
		    	ixr.setSkeletondate(rs.getTimestamp(6));
		    	ixr.setActualized(rs.getTimestamp(7));
		    	Object fc = rs.getObject(8);
		    	if(fc != null) {
		    		if(fc instanceof Long) {
		    			ixr.setFailcode(((Long) fc).intValue());
		    		}
		    		if(fc instanceof Integer) {
		    			ixr.setFailcode((Integer) fc);
		    		}
		    	}	    	
		    	ixr.setFailmsg(rs.getString(9));
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return ixr;
	}
	
	public static Indexrow getIndexrowFromIdAndName(long id, String name) {
		Indexrow ixr = null;
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection(); // id, indexfileId, name, href, skeletonsize, importsize, actualized from indexrow
		    stmt = conn.prepareStatement("select indexfileId, href, skeletonsize, importsize, skeletondate, actualized, failcode, failmsg from indexrow where id = ? and name = ?");
		    stmt.setLong(1, id);
		    stmt.setString(2,  name);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	ixr = new Indexrow();
				ixr.setId(id);
				ixr.setName(name);
				ixr.setIndexfileId(rs.getLong(1));
				ixr.setHref(rs.getString(2));
		    	ixr.setSkeletonsize(rs.getLong(3));
		    	Object is = rs.getObject(4);
		    	if(is != null) {
		    		if(is instanceof Long) {
		    			ixr.setImportsize((Long) is);
		    		}
		    		if(is instanceof Integer) {
		    			ixr.setImportsize(new Long((Integer) is));
		    		}
		    	}
				ixr.setSkeletondate(rs.getTimestamp(5));
				ixr.setActualized(rs.getTimestamp(6));
		    	Object fc = rs.getObject(7);
		    	if(fc != null) {
		    		if(fc instanceof Long) {
		    			ixr.setFailcode(((Long) fc).intValue());
		    		}
		    		if(fc instanceof Integer) {
		    			ixr.setFailcode((Integer) fc);
		    		}
		    	}	    	
		    	ixr.setFailmsg(rs.getString(8));
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return ixr;
	}
	
}

