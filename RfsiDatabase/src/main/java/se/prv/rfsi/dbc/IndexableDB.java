package se.prv.rfsi.dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.rfsi.dbcreuse.Commons;
import se.prv.rfsi.domain.Indexable;

public class IndexableDB {

	/*
create table indexable (
  id bigserial primary key,
  indexrowId bigint not null,
  documentId bigint default null,
  name varchar(200) not null,
  size bigint not null,
  actualized timestamp not null,
  failcode integer default null,
  failmsg varchar(300) default null,
  foreign key ( indexrowId ) references indexrow (id),
  foreign key ( documentId ) references document (id)
);
	 */

	public static List<Indexable> getAllIndexables() {
		List<Indexable> out = new ArrayList<Indexable>();
		 
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select id, indexrowId, documentId, name, size, actualized, failcode, failmsg from indexable");
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	Indexable ixe = new Indexable();
		    	ixe.setId(rs.getLong(1));
		    	ixe.setIndexrowId(rs.getLong(2));
		    	Object di = rs.getObject(3);
		    	if(di != null) {
		    		if(di instanceof Long) {
		    			ixe.setDocumentId((Long) di);
		    		}
		    		if(di instanceof Integer) {
		    			ixe.setDocumentId(new Long((Integer) di));
		    		}
		    	}
		    	ixe.setName(rs.getString(4));
		    	ixe.setSize(rs.getLong(5));
		    	ixe.setActualized(rs.getTimestamp(6));
		    	Object is = rs.getObject(7);
		    	if(is != null) {
		    		if(is instanceof Long) {
		    			ixe.setFailcode(((Long)is).intValue());
		    		}
		    		if(is instanceof Integer) {
		    			ixe.setFailcode((Integer) is);
		    		}
		    	}
		    	ixe.setFailmsg(rs.getString(8));
		    	
		    	out.add(ixe);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}
	
	public static List<Indexable> getAllIndexablesOfIndexrow(long indexrowId) {
		List<Indexable> out = new ArrayList<Indexable>();
		 
		JDBCConnectionPool pool = null;
		
		Connection conn = null;
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection(); // indexrowId, documentId, name, size, actualized from indexable
		    stmt = conn.prepareStatement("select id, documentId, name, size, actualized, failcode, failmsg from indexable where indexrowId = ?");
		    stmt.setLong(1, indexrowId);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	Indexable ixe = new Indexable();
		    	ixe.setId(rs.getLong(1));
		    	Object di = rs.getObject(3);
		    	if(di != null) {
		    		if(di instanceof Long) {
		    			ixe.setDocumentId((Long) di);
		    		}
		    		if(di instanceof Integer) {
		    			ixe.setDocumentId(new Long((Integer) di));
		    		}
		    	}
		    	ixe.setName(rs.getString(3));
		    	ixe.setSize(rs.getLong(4));
		    	ixe.setActualized(rs.getTimestamp(5));
		    	Object fc = rs.getObject(6);
		    	if(fc != null) {
		    		if(fc instanceof Long) {
		    			ixe.setFailcode(((Long) fc).intValue());
		    		}
		    		if(fc instanceof Integer) {
		    			ixe.setFailcode((Integer) fc);
		    		}
		    	}	    	
		    	ixe.setFailmsg(rs.getString(7));
		    	ixe.setIndexrowId(indexrowId);
		    	out.add(ixe);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}
	
	public static List<Indexable> getAllIndexablesOfDocument(long documentId) {
		List<Indexable> out = new ArrayList<Indexable>();
		 
		JDBCConnectionPool pool = null;
		
		Connection conn = null;
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection(); // indexrowId, documentId, name, size, actualized from indexable
		    stmt = conn.prepareStatement("select id, indexrowId, name, size, actualized, failcode, failmsg from indexable where documentId = ?");
		    stmt.setLong(1, documentId);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	Indexable ixe = new Indexable();
		    	ixe.setId(rs.getLong(1));
		    	ixe.setIndexrowId(rs.getLong(2));
		    	ixe.setName(rs.getString(3));
		    	ixe.setSize(rs.getLong(4));
		    	ixe.setActualized(rs.getTimestamp(5));
		    	Object fc = rs.getObject(6);
		    	if(fc != null) {
		    		if(fc instanceof Long) {
		    			ixe.setFailcode(((Long) fc).intValue());
		    		}
		    		if(fc instanceof Integer) {
		    			ixe.setFailcode((Integer) fc);
		    		}
		    	}	    	
		    	ixe.setFailmsg(rs.getString(7));
		    	ixe.setDocumentId(documentId);
		    	out.add(ixe);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}
	
	public static boolean deleteIndexable(Indexable ixr) {
	 	boolean outcome = deleteIndexable(ixr.getId());
	 	return outcome;
	}

	public static boolean deleteIndexable(long IndexrowId) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("delete from indexable where id = ?");
		    stmt.setLong(1, IndexrowId);
		    stmt.executeUpdate();
		    conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

	public static boolean updateIndexable(Indexable ixe) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection(); // indexrowId, documentId, name, size, actualized from indexable
		    stmt = conn.prepareStatement("update indexable set indexrowId = ?, documentId = ?, name = ?, size = ?, actualized = ?, failcode = ?, failmsg = ? where id = ?");
		    stmt.setLong(1, ixe.getIndexrowId());
		    Long documentId = ixe.getDocumentId();
		    if(documentId != null) {
			    stmt.setLong(2, ixe.getDocumentId());
		    }
		    else {
		    	stmt.setNull(2, Types.INTEGER);
		    }
		    stmt.setString(3, ixe.getName());
		    stmt.setLong(4, ixe.getSize());
		    
            Date actualized = ixe.getActualized();
            if(actualized == null) {
                actualized = new Date();
                ixe.setActualized(actualized);
            }
            stmt.setTimestamp(5, new java.sql.Timestamp(actualized.getTime()));
            Integer fc = ixe.getFailcode();
            if(fc != null) {
            	stmt.setInt(6,  fc);
            }
            else {
            	stmt.setNull(6, Types.INTEGER);
            }
            stmt.setString(7, ixe.getFailmsg());
            
		    stmt.setLong(8, ixe.getId());
		    stmt.executeUpdate();
		    conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

	public static boolean createIndexable(Indexable ixe) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    boolean outcome = createIndexable(ixe, conn);
		    if(outcome) {
			    conn.commit();
		    }
		    else {
			    conn.rollback();
		    }
		    return outcome;
		} catch(Exception e) {
	    	//System.out.println("Fel 2");
			e.printStackTrace();
		    return false;
		} finally {
		    if(conn != null) { pool.releaseConnection(conn); }
		}
	}
	
	public static boolean createIndexable(Indexable ixe, Connection conn) {
		PreparedStatement stmt = null;
		try { // indexrowId, documentId, name, size, actualized from indexable, failcode, failmsg = ?
		    stmt = conn.prepareStatement("insert into indexable ( indexrowId, documentId, name, size, actualized, failcode, failmsg ) values ( ?, ?, ?, ?, ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);
		    stmt.setLong(1, ixe.getIndexrowId());
		    Long documentId = ixe.getDocumentId();
		    if(documentId != null) {
			    stmt.setLong(2, ixe.getDocumentId());
		    }
		    else {
		    	stmt.setNull(2, Types.INTEGER);
		    }
		    stmt.setString(3, ixe.getName());
		    stmt.setLong(4, ixe.getSize());
            Date actualized = ixe.getActualized();
            if(actualized == null) {
                actualized = new Date();
                ixe.setActualized(actualized);
            }
            stmt.setTimestamp(5, new java.sql.Timestamp(actualized.getTime()));
            Integer fc = ixe.getFailcode();
            if(fc != null) {
            	stmt.setInt(6,  fc);
            }
            else {
            	stmt.setNull(6, Types.INTEGER);
            }
            stmt.setString(7, ixe.getFailmsg());
		    int affectedRows = stmt.executeUpdate();
		    try {
		    	ResultSet generatedKeys = stmt.getGeneratedKeys();
	            generatedKeys.next();
	            ixe.setId(generatedKeys.getLong(1));
	        }
		    catch(Exception e) {
		    	//System.out.println("Fel 1");
		    	e.printStackTrace();
		    }
		} catch(Exception e) {
	    	//System.out.println("Fel 2");
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		}
		return true;
	}
	
	public static Indexable getIndexableFromId(long id) {
		Indexable ixe = null;
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection(); // indexrowId, documentId, name, size, actualized from indexable
		    stmt = conn.prepareStatement("select indexrowId, documentId, name, size, actualized, failcode, failmsg from indexable where id = ?");
		    stmt.setLong(1, id);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	ixe = new Indexable();
				ixe.setId(id);
				ixe.setIndexrowId(rs.getLong(1));
		    	Object is = rs.getObject(3);
		    	if(is != null) {
		    		if(is instanceof Long) {
		    			ixe.setDocumentId((Long) is);
		    		}
		    		if(is instanceof Integer) {
		    			ixe.setDocumentId(new Long((Integer) is));
		    		}
		    	}
				ixe.setName(rs.getString(3));
				ixe.setSize(rs.getLong(4));
				ixe.setActualized(rs.getTimestamp(5));
		    	Object fc = rs.getObject(6);
		    	if(fc != null) {
		    		if(fc instanceof Long) {
		    			ixe.setFailcode(((Long) fc).intValue());
		    		}
		    		if(fc instanceof Integer) {
		    			ixe.setFailcode((Integer) fc);
		    		}
		    	}	    	
		    	ixe.setFailmsg(rs.getString(7));
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return ixe;
	}
	
}

