package se.prv.rfsi.dbc;

import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.rfsi.dbcreuse.Commons;
import se.prv.rfsi.domain.Document;
import se.prv.rfsi.internal.Services;

public class DocumentDB {

	/*
create table document (
  id bigserial primary key,
  checksum varchar(70) not null,
  mimetype varchar(200) not null,
  indexed bool default null,
  importer integer default null;
  data bytea,
  unique(checksum)
);
	 */
	
	public static List<Document> getAllDocuments() {
		List<Document> out = new ArrayList<Document>();
		 
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection(); // id, checksum, mimetype
		    stmt = conn.prepareStatement("select id, checksum, mimetype, indexed, importer from document");
		    rs = stmt.executeQuery();
		    while(rs.next()) {
				Document df = new Document();
		    	df.setId(rs.getLong(1));
		    	df.setChecksum(rs.getString(2));
		    	df.setMimetype(rs.getString(3));
		    	Object b = rs.getObject(4);
		    	if(b instanceof Boolean) {
			    	df.setIndexed((Boolean) b);
		    	}
		    	Object i = rs.getObject(5);
		    	if(i instanceof Integer) {
			    	df.setImporter((Integer) i);
		    	}
		    	out.add(df);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return out;
	}
	
	public static boolean deleteDocument(Document df) {
	 	return deleteDocument(df.getId());
	}

	public static boolean deleteDocument(long documentId) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("delete from document where id = ?");
		    stmt.setLong(1, documentId);
		    stmt.executeUpdate();
		    conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

	public static boolean updateDocument(Document pf) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection(); // id, checksum, mimetype : document
		    stmt = conn.prepareStatement("update document set checksum = ?, mimetype = ?, indexed = ?, importer = ? where id = ?");
		    stmt.setString(1, pf.getChecksum());
		    stmt.setString(2, pf.getMimetype());
		    Boolean b = pf.getIndexed();
		    if(b != null) {
			    stmt.setBoolean(3, b);
		    }
		    else {
		    	stmt.setNull(3, Types.BOOLEAN);
		    }
		    Integer i = pf.getImporter();
		    if(i != null) {
			    stmt.setInt(4, i);
		    }
		    else {
		    	stmt.setNull(4, Types.INTEGER);
		    }
		    stmt.setLong(5, pf.getId());
		    stmt.executeUpdate();
		    conn.commit();
		} catch(Exception e) {
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return true;
	}

	public static boolean createDocument(Document pf) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    boolean outcome = createDocument(pf, conn);
		    if(outcome) {
			    conn.commit();
		    }
		    else {
			    conn.rollback();
		    }
		    return outcome;
		} catch(Exception e) {
	    	//System.out.println("Fel 2");
			e.printStackTrace();
		    return false;
		} finally {
		    if(conn != null) { pool.releaseConnection(conn); }
		}
	}
	
	public static boolean createDocument(Document pf, byte data[]) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		try {
			pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    if(!createDocument(pf, conn)) {
			    conn.rollback();
			    return false;
		    }
		    if(!setData(pf.getId(), data, conn)) {
			    conn.rollback();
			    return false;
		    }
		    conn.commit();
		    return true;
		} 
		catch(Exception e) {
			e.printStackTrace();
		    if(conn != null) { 
		    	try {
		    		conn.rollback();
		    	} 
		    	catch (SQLException e1) {
		    		e1.printStackTrace();
		    	} 
		    }
		    return false;
		} 
		finally {
		    if(conn != null) { pool.releaseConnection(conn); }
		}
	}
	
	public static boolean createDocument(Document pf, Connection conn) {
		PreparedStatement stmt = null;
		try { // id, checksum, mimetype : document
		    stmt = conn.prepareStatement("insert into document (checksum, mimetype, indexed, importer) values ( ?, ?, ?, ? )", Statement.RETURN_GENERATED_KEYS);
		    stmt.setString(1, pf.getChecksum());
		    stmt.setString(2, pf.getMimetype());
		    Boolean b = pf.getIndexed();
		    if(b != null) {
			    stmt.setBoolean(3, b);
		    }
		    else {
		    	stmt.setNull(3, Types.BOOLEAN);
		    }
		    Integer i = pf.getImporter();
		    if(i != null) {
			    stmt.setInt(4, i);
		    }
		    else {
		    	stmt.setNull(4, Types.INTEGER);
		    }
		    int affectedRows = stmt.executeUpdate();
		    try {
		    	ResultSet generatedKeys = stmt.getGeneratedKeys();
	            generatedKeys.next();
	            pf.setId(generatedKeys.getLong(1));
	        }
		    catch(Exception e) {
		    	//System.out.println("Fel 1");
		    	e.printStackTrace();
		    }
		} catch(Exception e) {
	    	//System.out.println("Fel 2");
			e.printStackTrace();
		    return false;
		} finally {
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		}
		return true;
	}
	
	public static Document getDocumentFromId(long id) {
		Document df = null;
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection(); // id, checksum, mimetype : document
		    stmt = conn.prepareStatement("select checksum, mimetype, indexed, importer from document where id = ?");
		    stmt.setLong(1, id);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	df = new Document();
				df.setId(id);
				df.setChecksum(rs.getString(1));
				df.setMimetype(rs.getString(2));
		    	Object b = rs.getObject(3);
		    	if(b instanceof Boolean) {
			    	df.setIndexed((Boolean) b);
		    	}
		    	Object i = rs.getObject(4);
		    	if(i instanceof Integer) {
			    	df.setImporter((Integer) i);
		    	}
		    	if(i instanceof Long) {
			    	df.setImporter(((Long) i).intValue());
		    	}
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return df;
	}

	public static Document getDocumentFromChecksum(String checksum) {
		Document df = null;
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection(); // id, checksum, mimetype : document
		    stmt = conn.prepareStatement("select id, mimetype, indexed, importer from document where checksum = ?");
		    stmt.setString(1, checksum);
		    rs = stmt.executeQuery();
		    while(rs.next()) {
		    	df = new Document();
				df.setId(rs.getLong(1));
				df.setMimetype(rs.getString(2));
		    	Object b = rs.getObject(3);
		    	if(b != null) {
			    	df.setIndexed((Boolean) b);
		    	}
		    	Object i = rs.getObject(4);
		    	if(i instanceof Integer) {
			    	df.setImporter((Integer) i);
		    	}
		    	if(i instanceof Long) {
			    	df.setImporter(((Long) i).intValue());
		    	}
				df.setChecksum(checksum);
		    }
		} catch(Exception e) {
		    e.printStackTrace();
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
		return df;
	}

	public static boolean setData(long id, byte[] bytes) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    if(!setData(id, bytes, conn)) {
		    	conn.rollback();
		    	return false;
		    }
		    conn.commit();
		    return true;
		} catch(Exception e) {
		    e.printStackTrace();
		    return false;
		} finally {
		    if(conn != null) { pool.releaseConnection(conn); }
		}
	}

	public static boolean setData(long id, byte[] bytes, Connection conn) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    stmt = conn.prepareStatement("update document set data = ? where id = ?");
		    stmt.setBinaryStream(1, new ByteArrayInputStream(bytes != null ? bytes : new byte[0]), (bytes != null ? bytes.length : 0));
		    stmt.setLong(2, id);
		    int count = stmt.executeUpdate();
		    return true;
		} catch(Exception e) {
		    e.printStackTrace();
		    return false;
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		}
	}

	public static byte[] getData(long id) {
		JDBCConnectionPool pool = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
		    pool = Commons.createPool();
		    conn = pool.reserveConnection();
		    stmt = conn.prepareStatement("select data from document where id = ?");
		    stmt.setLong(1, id);
		    rs = stmt.executeQuery();
		    rs.next();
		    return Services.getByteArrayFromStream(rs.getBinaryStream(1));
		} catch(Exception e) {
		    e.printStackTrace();
		    return null;
		} finally {
		    try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmt != null) stmt.close(); } catch (Exception e) {};
		    if(conn != null) { pool.releaseConnection(conn); }
		}
	}
		
}

