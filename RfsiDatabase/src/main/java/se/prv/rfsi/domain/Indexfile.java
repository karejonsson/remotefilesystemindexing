package se.prv.rfsi.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import se.prv.rfsi.dbc.IndexfileDB;
import se.prv.rfsi.dbc.IndexrowDB;

public class Indexfile {

	/*
create table indexfile (
  id bigserial primary key,
  folder varchar(200) not null,
  postfixes varchar(250) not null,
  process bool default null,
  actualized timestamp not null
);
	 */

	private Long id;
	private String folder;
	private String postfixes;
	private Boolean process;
	private Date actualized;
	public String toString() {
		return "Indexfile{ id "+id+", folder "+folder+", postfixes "+postfixes+
				", process "+process+", actualized "+actualized+" }";
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFolder() {
		return folder;
	}
	public void setFolder(String folder) {
		this.folder = folder;
	}
	public String getPostfixes() {
		return postfixes;
	}
	public void setPostfixes(String postfixes) {
		this.postfixes = postfixes;
	}
	public Boolean getProcess() {
		return process;
	}
	public boolean isProcessed() {
		if(process == null) {
			return false;
		}
		return process;
	}
	public void setProcess(Boolean process) {
		this.process = process;
	}
	public Date getActualized() {
		return actualized;
	}
	public void setActualized(Date actualized) {
		this.actualized = actualized;
	}
	
	public List<Indexrow> getReferringIndexrows() {
		return IndexrowDB.getAllIndexrowsOfIndexfile(id);
	}
	
	public Map<String, Indexrow> getIndexrowsMapped() {
		return IndexrowDB.getAllIndexrowsOfindexfileMapped(id);
	}
	
	public void save() {
		if(id == null) {
			IndexfileDB.createIndexfile(this);
		}
		else {
			IndexfileDB.updateIndexfile(this);
		}
	}

	public static String makeStringForPostfixField(List<String> l) {
		if(l == null) {
			return null;
		}
		if(l.size() == 0) {
			return "[]";
		}
		if(l.size() == 1) {
			return "[ "+l.get(0)+" ]";
		}
		StringBuffer sb = new StringBuffer();
		sb.append("[ "+l.get(0).toString());
		for(int i = 1 ; i < l.size() ; i++) {
			sb.append(", "+l.get(i).toString());
		}
		return sb.toString()+" ]";
	}

	public void setPostfixes(List<String> postfixes) {
		this.postfixes = makeStringForPostfixField(postfixes);
		updatePostfixesList();
	}
	
	private List<String> postfixesList = null;
	
	public List<String> getPostfixesList() {
		updatePostfixesList();
		return postfixesList;
	}
	
	private void updatePostfixesList() {
		if(postfixes == null) {
			postfixesList = null;
			return;
		}
		String x = postfixes.replace("[", " ");
		x = x.replaceAll("]", " ");
		x = x.replaceAll(",", " ");
		String[] partsS = x.split(" ");
		List<String> parts = new ArrayList<String>();
		for(String part : partsS) {
			if(part.trim().length() > 0) {
				parts.add(part.trim());
			}
		}
		postfixesList = parts;
	}

	public boolean hasSamePostfixes(List<String> otherPostfixes) {
		updatePostfixesList();
		for(String otherPostfix : otherPostfixes) {
			if(!postfixesList.contains(otherPostfix)) {
				return false;
			}
		}
		return true;
	}
}
