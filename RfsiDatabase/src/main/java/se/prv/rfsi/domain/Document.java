package se.prv.rfsi.domain;

import java.util.List;

import se.prv.rfsi.dbc.DocumentDB;
import se.prv.rfsi.dbc.IndexableDB;
import se.prv.rfsi.dbc.IndexrowDB;

public class Document {

	/*
create table document (
  id bigserial primary key,
  checksum varchar(70) not null,
  mimetype varchar(200) not null,
  indexed bool default null,
  importer integer default null;
  data bytea,
  unique(checksum)
);
	 */
	
	private Long id;
	private String checksum;
	private String mimetype;
	private Boolean indexed;
	private Integer importer;
	public String toString() {
		return "Document{ id "+id+", checksum "+checksum+", mimetype "+mimetype+
				", indexed "+indexed+" }";
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getChecksum() {
		return checksum;
	}
	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}
	public String getMimetype() {
		return mimetype;
	}
	public void setMimetype(String mimetype) {
		this.mimetype = mimetype;
	}
	public Boolean getIndexed() {
		return indexed;
	}
	public boolean isIndexed() {
		if(indexed == null) {
			return false;
		}
		return indexed;
	}
	public void setIndexed(Boolean indexed) {
		this.indexed = indexed;
	}
	
	public Integer getImporter() {
		return importer;
	}
	public void setImporter(Integer importer) {
		this.importer = importer;
	}
	public List<Indexable> getReferringIndexables() {
		return IndexableDB.getAllIndexablesOfDocument(id);
	}
	
	public void save() {
		if(id == null) {
			DocumentDB.createDocument(this);
		}
		else {
			DocumentDB.updateDocument(this);
		}
	}

	public void setData(byte[] bytes) {
		DocumentDB.setData(id, bytes);
	}
	
	public byte[] getData() {
		return DocumentDB.getData(id);
	}
}
