package se.prv.rfsi.domain;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;

import se.prv.rfsi.dbc.DocumentDB;
import se.prv.rfsi.dbc.IndexableDB;
import se.prv.rfsi.dbc.IndexrowDB;

public class Indexable {

	/*
create table indexable (
  id bigserial primary key,
  indexrowId bigint not null,
  documentId bigint default null,
  name varchar(200) not null,
  size bigint not null,
  actualized timestamp not null,
  failcode integer default null,
  failmsg varchar(300) default null,
  foreign key ( indexrowId ) references indexrow (id),
  foreign key ( documentId ) references document (id)
);
	*/
	
	private Long id;
	private Long indexrowId;
	private Long documentId;
	private String name;
	private Long size;
	private Date actualized;
	private Integer failcode;
	private String failmsg;
	
	public String toString() {
		return "Indexable{ id "+id+", indexrowId "+indexrowId+", documentId "+documentId+
				", name "+name+", size "+size+", actualized "+actualized+", failcode "+failcode+", failmsg "+failmsg+" }";
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIndexrowId() {
		return indexrowId;
	}
	public void setIndexrowId(Long indexrowId) {
		this.indexrowId = indexrowId;
		indexrow = null;
	}
	public Long getDocumentId() {
		return documentId;
	}
	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getSize() {
		return size;
	}
	public void setSize(Long size) {
		this.size = size;
	}
	public Date getActualized() {
		return actualized;
	}
	public void setActualized(Date actualized) {
		this.actualized = actualized;
	}	
	public Integer getFailcode() {
		return failcode;
	}
	public void setFailcode(Integer failcode) {
		this.failcode = failcode;
	}
	public String getFailmsg() {
		return failmsg;
	}
	public static final int mailmsglength = 300;
	public void setFailmsg(String failmsg) {
		if(failmsg == null) {
			this.failmsg = null;
			return;
		}
		if(failmsg.length() > mailmsglength) {
			failmsg = failmsg.substring(0, mailmsglength);
		}
		this.failmsg = failmsg;
	}
	public void setFailmsg(Exception e) {
		if(e == null) {
			this.failmsg = null;
			return;
		}
		StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		String exceptionAsString = sw.toString();
		setFailmsg(exceptionAsString);
	}

	private Indexrow indexrow = null;
	
	public Indexrow getIndexrow() {
		if(indexrow != null) {
			return indexrow;
		}
		indexrow = IndexrowDB.getIndexrowFromId(indexrowId);
		return indexrow;
	}
	
	public void setIndexrow(Indexrow indexrow) {
		indexrowId = indexrow.getId();
		this.indexrow = indexrow;
	}
	
	private Document document = null;
	
	public Document getDocument() {
		if(document != null) {
			return document;
		}
		if(documentId == null) {
			return null;
		}
		document = DocumentDB.getDocumentFromId(documentId);
		return document;
	}
	
	public void setDocument(Document document) {
		documentId = document == null ? null : document.getId();
		this.document = document;
	}
	
	public void save() {
		if(id == null) {
			IndexableDB.createIndexable(this);
		}
		else {
			IndexableDB.updateIndexable(this);
		}
	}


}
