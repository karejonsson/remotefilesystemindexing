package se.prv.rfsi.domain;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import se.prv.rfsi.dbc.IndexableDB;
import se.prv.rfsi.dbc.IndexfileDB;
import se.prv.rfsi.dbc.IndexrowDB;

public class Indexrow {

	/*
create table indexrow (
  id bigserial primary key,
  indexfileId bigint not null,
  name varchar(200) not null,
  href varchar(400) not null,
  skeletonsize bigint not null,
  importsize bigint default null,
  skeletondate timestamp not null,
  actualized timestamp not null,
  failcode integer default null,
  failmsg varchar(300) default null,
  foreign key ( indexfileId ) references indexfile (id)
);
	 */
	
	private Long id;
	private Long indexfileId;
	private String name;
	private String href;
	private Long skeletonsize;
	private Long importsize;
	private Date skeletondate;
	private Date actualized;
	private Integer failcode;
	private String failmsg;
	public String toString() {
		return "Indexrow{ id "+id+", indexfileId "+indexfileId+", name "+name+", href "+href+
				", skeletonsize "+skeletonsize+", importsize "+importsize+
				", skeletondate "+skeletondate+
				", actualized "+actualized
				+", failcode "+failcode+", failmsg "+failmsg+" }";
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIndexfileId() {
		return indexfileId;
	}
	public void setIndexfileId(Long indexfileId) {
		this.indexfileId = indexfileId;
		indexfile = null;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public Long getSkeletonsize() {
		return skeletonsize;
	}
	public void setSkeletonsize(Long skeletonsize) {
		this.skeletonsize = skeletonsize;
	}
	public Long getImportsize() {
		return importsize;
	}
	public void setImportsize(Long importsize) {
		this.importsize = importsize;
	}
	public Date getSkeletondate() {
		return skeletondate;
	}
	public void setSkeletondate(Date skeletondate) {
		this.skeletondate = skeletondate;
	}
	public Date getActualized() {
		return actualized;
	}
	public void setActualized(Date actualized) {
		this.actualized = actualized;
	}
	public Integer getFailcode() {
		return failcode;
	}
	public void setFailcode(Integer failcode) {
		this.failcode = failcode;
	}
	public String getFailmsg() {
		return failmsg;
	}
	public static final int mailmsglength = 300;
	public void setFailmsg(String failmsg) {
		if(failmsg == null) {
			this.failmsg = null;
			return;
		}
		if(failmsg.length() > mailmsglength) {
			failmsg = failmsg.substring(0, mailmsglength);
		}
		this.failmsg = failmsg;
	}
	public void setFailmsg(Exception e) {
		if(e == null) {
			this.failmsg = null;
			return;
		}
		StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		String exceptionAsString = sw.toString();
		setFailmsg(exceptionAsString);
	}
	
	public List<Indexable> getReferringIndexables() {
		return IndexableDB.getAllIndexablesOfIndexrow(id);
	}
	
	public Map<String, Indexable> getIndexablesMapped() {
		List<Indexable> list = getReferringIndexables();
		Map<String, Indexable> out = new HashMap<String, Indexable>();
		for(Indexable ir : list) {
			out.put(ir.getName(), ir);
			ir.setIndexrow(this);
		}
		return out;
	}
	
	private Indexfile indexfile = null;
	
	public Indexfile getIndexfile() {
		if(indexfile != null) {
			return indexfile;
		}
		indexfile = IndexfileDB.getIndexfileFromId(indexfileId);
		return indexfile;
	}
	
	public void setIndexfile(Indexfile indexfile) {
		indexfileId = indexfile.getId();
		this.indexfile = indexfile;
	}
	
	public void save() {
		if(id == null) {
			IndexrowDB.createIndexrow(this);
		}
		else {
			IndexrowDB.updateIndexrow(this);
		}
	}

}
