package se.prv.rfsi.dbcreuse;

import general.reuse.database.pool.JDBCConnectionPool;
import general.reuse.database.pool.SimpleJDBCConnectionPool;
import se.prv.rfsi.internal.InstallationProperties;

public class Commons {

	private static JDBCConnectionPool pool = null;

	public static void setPool(JDBCConnectionPool pool) {
		Commons.pool = pool;
	}
	
	public static void destroy() {
		if(pool != null) {
			pool.destroy();
		}
	}

	public static JDBCConnectionPool createPool() throws Exception {
		if(pool != null) {
			return pool;
		}
		SimpleJDBCConnectionPool pool = new SimpleJDBCConnectionPool(
				InstallationProperties.getString(InstallationProperties.jdbc_driver_class_property, "org.postgresql.Driver"),
				InstallationProperties.getString(InstallationProperties.jdbc_url_property, "jdbc:postgresql://127.0.0.1:5432/mbblog"),
				InstallationProperties.getString(InstallationProperties.jdbc_username_property, "tunnelsup"),
				InstallationProperties.getString(InstallationProperties.jdbc_password_property, "abc123"));
		setPool(pool);
		System.out.println("Created pool "+pool);
		return pool;
	}

}