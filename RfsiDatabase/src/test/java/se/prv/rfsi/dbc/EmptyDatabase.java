package se.prv.rfsi.dbc;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.rfsi.test.TestUtilities;

public class EmptyDatabase {
	
    private JDBCConnectionPool pool = null;

    @Before
    public void setup() throws IOException, SQLException {
        pool = TestUtilities.setupPool();
    }

    @After
    public void tearDown() throws Exception {
        TestUtilities.destroyPool(pool);
    }
    
    @Test
    public void lengthZero() throws Exception {
    	Assert.assertTrue(DocumentDB.getAllDocuments().size() == 0);
    	Assert.assertTrue(IndexfileDB.getAllIndexfiles().size() == 0);
    	Assert.assertTrue(IndexrowDB.getAllIndexrows().size() == 0);
    	Assert.assertTrue(IndexableDB.getAllIndexables().size() == 0);
    }

}
