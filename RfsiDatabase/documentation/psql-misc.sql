alter table indexrow alter column href type varchar(400);
alter table document alter column checksum type varchar(70);
( export PGHOST=localhost ; psql -U prvuser -W indexdb )

sudo -u postgres dropdb indexdb
alter table indexable alter column name type varchar(200);

sudo -u postgres dropdb indexdb
sudo -u postgres createdb -O prvuser -Eutf8 indexdb
psql -U prvuser -d indexdb -a -f psql-schema.sql

alter table document add column importer integer;

alter table indexable add column failcode integer default null;
alter table indexable add column failmsg varchar(300) default null;
alter table indexrow add column failcode integer default null;
alter table indexrow add column failmsg varchar(300) default null;

