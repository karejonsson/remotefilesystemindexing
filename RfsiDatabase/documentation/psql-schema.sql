create table indexfile (
  id bigserial primary key,
  folder varchar(200) not null,
  postfixes varchar(250) not null,
  process bool default null,
  actualized timestamp not null
);

create table indexrow (
  id bigserial primary key,
  indexfileId bigint not null,
  name varchar(200) not null,
  href varchar(400) not null,
  skeletonsize bigint not null,
  importsize bigint default null,
  skeletondate timestamp not null,
  actualized timestamp not null,
  failcode integer default null,
  failmsg varchar(300) default null,
  foreign key ( indexfileId ) references indexfile (id)
);

create table document (
  id bigserial primary key,
  checksum varchar(70) not null,
  mimetype varchar(200) not null,
  indexed bool default null,
  importer integer default null,
  data bytea,
  unique(checksum)
);

create table indexable (
  id bigserial primary key,
  indexrowId bigint not null,
  documentId bigint default null,
  name varchar(200) not null,
  size bigint not null,
  actualized timestamp not null,
  failcode integer default null,
  failmsg varchar(300) default null,
  foreign key ( indexrowId ) references indexrow (id),
  foreign key ( documentId ) references document (id)
);
